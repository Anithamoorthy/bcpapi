﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class InvestorDeclarationBO
    {
        public int DeclarationID { get; set; }
        public string Dec_Label { get; set; }
        public bool Dec_Individual { get; set; }
        public bool Dec_Joint { get; set; }
        public bool Dec_NonIndividual { get; set; }
        public bool Dec_Marketing { get; set; }
        public byte Sort_Order { get; set; }
        public bool BCP_ARF_Only { get; set; }
        public bool BCP_AMRF_Only { get; set; }
        public bool BCP_PRB_Only { get; set; }
    }
}
