﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface ISmsServiceRepository
    {
        void SendSms(string to, string content);
    }
}
