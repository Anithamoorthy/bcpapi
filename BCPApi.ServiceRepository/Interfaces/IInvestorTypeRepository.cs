﻿using BCPApi.Entity.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IInvestorTypeRepository
    {
        IList<InvestorTypeBO> GetByClientType(string clientType);
    }
}
