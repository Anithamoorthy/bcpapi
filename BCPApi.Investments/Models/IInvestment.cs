﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Investments.Models
{
    public interface IInvestment
    {
        List<InvestmentBO> GetClientInvestments(int clientNumber);
        List<AssetClassList> GetAssetClasses(int stockId);
    }
}
