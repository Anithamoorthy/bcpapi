﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class ClientTransactionBO
    {
        public int Client_ID_Number { get; set; }
        public int StockID { get; set; }
        public string Fund_Name { get; set; }
        public int CLTransactionID { get; set; }
        public string Transaction_Description { get; set; }
        public DateTime Transaction_Date { get; set; }
        public double Number_Of_Units { get; set; }
        public decimal Transaction_Value { get; set; }
        public double UnitPrice { get; set; }
    }
}
