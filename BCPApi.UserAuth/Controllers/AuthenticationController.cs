﻿using BCPApi.Entities;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BCPApi.UserAuth.Controllers
{
    public class AuthenticationController : ApiController
    {
        // GET: api/Authentication
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Authentication/5
        public string Get(int id)
        {
            return "value";
        }

        public AuthenticationController()
        {
        }

        IUserAuthRepository userservice;

        public AuthenticationController(IUserAuthRepository userAuth)
        {
            userservice = userAuth;
        }

        // POST: api/Authentication
        //[EnableCors(origins: "http://localhost:10612/", headers: "*", methods: "*")]
        [Route("userauth/login")]
        public IHttpActionResult Login([FromBody]AuthRequest login)
        {
            if (login == null || !ModelState.IsValid)
            {
                return BadRequest("User authenticatio invalid!");
            }
            return Ok(userservice.CheckUserAuth(login.username, login.password).Select(i => new AuthResponse()
            {
                ClientIDNumber = i.ClientIDNumber,
                ClientAccountNumber = i.ClientAccountNumber,
                FirstName = i.FirstName,
                LastName = i.LastName,
                City = i.City,
                Country=i.Country
            }));
        }

        // PUT: api/Authentication/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Authentication/5
        public void Delete(int id)
        {
        }
       
    }
}
