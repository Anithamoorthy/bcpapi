﻿using BCPApi.Entities.Data;
using BCPApi.ServiceRepository.Interfaces;
using com.esendex.sdk.messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class SmsServiceRepository : ISmsServiceRepository
    {
        public void SendSms(string to, string content)
        {
            if (!string.IsNullOrWhiteSpace(ApplicationSettings.Instance.TestSmsTo))
                to = ApplicationSettings.Instance.TestSmsTo;
            MessagingService messagingService = new MessagingService(ApplicationSettings.Instance.SmsUsername, ApplicationSettings.Instance.SmsPassword);
            SmsMessage smsMessage = new SmsMessage(to, content, ApplicationSettings.Instance.SmsAccountReference);
            smsMessage.Originator = ApplicationSettings.Instance.SmsFrom;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            messagingService.SendMessage(smsMessage);
        }
    }
}
