﻿using BCPApi.Entities.BO.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IIndividualInvestorRepository
    {
        RegistrationIndividualFieldsBO GetRegistrationFields();
        RegistrationIndividualBO GetRegistration(int webClientIDNumber);
        RegistrationIndividualBO SaveRegistrationAndSendNotification(RegistrationIndividualBO individual);
        RegistrationIndividualBO ContinueRegistration(string notificationCode);
        void SendRegistration2FactorCode(int webClientIDNumber, string phone);
        void SendVerificationEmail(RegistrationIndividualBO investor);
    }
}
