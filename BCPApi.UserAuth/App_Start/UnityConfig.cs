using BCPApi.ServiceRepository;
using BCPApi.ServiceRepository.Interfaces;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Mvc5;

namespace BCPApi.UserAuth
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            //container.RegisterType<UserAuthRepository>(new InjectionConstructor(new BCPApi.Database.BaseDbContext()));
            //container.RegisterType<IUserAuthRepository, UserAuthRepository>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}