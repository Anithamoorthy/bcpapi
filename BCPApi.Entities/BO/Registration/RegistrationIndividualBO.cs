﻿using System;

namespace BCPApi.Entities.BO.Registration
{
    public class RegistrationIndividualBO : BaseRegistrationBO
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string DateOfBirthString { get; set; }
        public string Person1CountryOfBirth { get; set; }
        public string Person1PlaceOfBirth { get; set; }
        public string PPS_Number { get; set; }
        public string SourceOfFunds { get; set; }
        public string SourcePolicy { get; set; }
        public bool? ARF_Has_AMRF { get; set; }
        public string QFM_Name { get; set; }
        public string AMRF_Ref { get; set; }
        public decimal? AMRF_Original_Investment { get; set; }
        public bool AMRF_No_Over75 { get; set; }
        public bool AMRF_No_Pension { get; set; }
        public bool? AMRF_No_Annuity { get; set; }
        public string AMRF_No_InsuranceCo { get; set; }
        public string AMRF_No_RefNum { get; set; }
        public decimal? AMRF_No_Premium { get; set; }
        public DateTime? AMRF_No_AnnuityStart { get; set; }
        public string AMRF_No_AnnuityStartString { get; set; }
    }
}
