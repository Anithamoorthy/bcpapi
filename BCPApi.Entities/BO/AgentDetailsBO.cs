﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO
{
    public class AgentDetailsBO
    {
        [JsonIgnore]
        public int AdvisorID { get; set; }
        public string Username { get; set; }
        public string AdvisorTitle { get; set; }
        public string AdvisorForename { get; set; }
        public string AdvisorSurname { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string AgentCode { get; set; }
        public string TradingName { get; set; }
        public string BCPPartner { get; set; }

        [ScriptIgnore]
        public string DisplayName
        {
            get
            {
                return Join(" ", new string[] { AdvisorForename, AdvisorSurname });
            }
        }

        [ScriptIgnore]
        public string DisplayFormalName
        {
            get
            {
                return Join(" ", new string[] { AdvisorTitle, AdvisorSurname });
            }
        }

        private string Join(string separator, string[] values)
        {
            return string.Join(separator, values.Where(w => !string.IsNullOrWhiteSpace(w))).Trim();
        }

    }
}
