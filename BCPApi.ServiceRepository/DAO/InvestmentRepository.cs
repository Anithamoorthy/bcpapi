﻿using BCPApi.Database;
using BCPApi.Entities.BO;
using BCPApi.Entities.BO.Registration;
using BCPApi.Entities.Data;
using BCPApi.Entities.Domain;
using BCPApi.Entities.Exceptions;
using BCPApi.ServiceRepository.Interfaces;
using BCPApi.ServiceRepository.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace BCPApi.ServiceRepository.DAO
{
    public class InvestmentRepository : CommonService, IInvestmentRepository
    {
        protected IBaseDbContext Db { get; set; }

        private Lazy<IUserAuthRepository> userService;
        public IUserAuthRepository UserService { get { return userService.Value; } }

        private Lazy<ITokenServiceRepository> tokenService;
        public ITokenServiceRepository TokenService { get { return tokenService.Value; } }

        private Lazy<IEmailServiceRepository> emailService;
        public IEmailServiceRepository EmailService { get { return emailService.Value; } }

        private Lazy<ISmsServiceRepository> smsService;
        public ISmsServiceRepository SmsService { get { return smsService.Value; } }

        private Lazy<IDocumentRepository> documentService;
        public IDocumentRepository DocumentService { get { return documentService.Value; } }

        private Lazy<IInvestorTypeRepository> investorTypeService;
        public IInvestorTypeRepository InvestorTypeService { get { return investorTypeService.Value; } }

        public InvestmentRepository(IBaseDbContext db, Lazy<IUserAuthRepository> userService, Lazy<ITokenServiceRepository> tokenService, Lazy<IEmailServiceRepository> emailService, Lazy<ISmsServiceRepository> smsService, //Lazy<IDocumentService> documentService,
            Lazy<IInvestorTypeRepository> investorTypeService, Lazy<IDocumentRepository> documentService)
        {
            Db = db;
            this.userService = userService;
            this.tokenService = tokenService;
            this.emailService = emailService;
            this.smsService = smsService;            
            this.investorTypeService = investorTypeService;
            this.documentService = documentService;
        }

        public IList<InvestmentBO> GetClientInvestments(int clientNumber, DateTime? startDate, DateTime? endDate)
        {
            SqlParameter ClientIDNumber = CreateSqlParameter("ClientIDNumber", clientNumber, SqlDbType.Int);
            return Db.DataBase.SqlQuery<InvestmentBO>("exec CRM_Get_Client_Holdings @ClientIDNumber", ClientIDNumber)
                .Where(i => !startDate.HasValue || i.MaturityDate >= startDate.Value)
                .Where(i => !endDate.HasValue || i.MaturityDate <= endDate.Value)
                .Select(i => { i.AssetClasses = GetAssetClasses(i.StockID); return i; })
                .ToList();
        }

        public IList<InvestmentBO> GetClientInvestmentById(int clientNumber, int ClientHoldingId, DateTime? startDate, DateTime? endDate)
        {
            SqlParameter ClientIDNumber = CreateSqlParameter("ClientIDNumber", clientNumber, SqlDbType.Int), HodlingId = CreateSqlParameter("ClientHoldingId", ClientHoldingId, SqlDbType.Int);
            return Db.DataBase.SqlQuery<InvestmentBO>("exec CRM_Get_Client_Holdings_By_Holding_Id @ClientIDNumber, @ClientHoldingId", ClientIDNumber, HodlingId)
                .Where(i => !startDate.HasValue || i.MaturityDate >= startDate.Value)
                .Where(i => !endDate.HasValue || i.MaturityDate <= endDate.Value)
                .Select(i => { i.AssetClasses = GetAssetClasses(i.StockID); return i; })
                .ToList();
        }

        #region  Documents
        public IList<InvestmentDocumentBO> GetInvestmentDocuments(int investmentId)
        {
            IList<InvestmentDocumentBO> allDocuments = GetAllInvestmentDocuments(investmentId);
            IList<InvestmentDocumentBO> documentRecords = GetInvestmentDocumentRecords(investmentId);
            IList<InvestmentDocumentBO> documents = new List<InvestmentDocumentBO>();

            foreach (InvestmentDocumentBO doc in allDocuments)
            {
                InvestmentDocumentBO foundDoc = documentRecords
                    .Where(d => string.Equals(d.Document_Link, doc.Document_Link) && string.Equals(d.Document_Description, doc.Document_Description))
                    .FirstOrDefault();
                if (foundDoc != null)
                    documents.Add(foundDoc);
                else
                    documents.Add(doc);
            }

            return documents;
        }

        public IList<InvestmentDocumentBO> SaveInvestmentDocuments(IList<InvestmentDocumentBO> investmentDocuments, int clientId, string clientName, UserBasicBO user)
        {
            for (int i = 0; i < investmentDocuments.Count; i++)
            {
                investmentDocuments[i] = SaveInvestmentDocument(investmentDocuments[i], clientId, clientName, user);
            }
            return investmentDocuments;
        }

        #endregion

        #region Transaction
        public IList<ClientTransactionBO> GetClientTransactions(int clientNumber, int? stockId, DateTime? startDate, DateTime? endDate)
        {
            SqlParameter ClientIDNumber = CreateSqlParameter("ClientIDNumber", clientNumber, SqlDbType.Int);
            return Db.DataBase.SqlQuery<ClientTransactionBO>("exec CRM_Get_Client_Transactions @ClientIDNumber", ClientIDNumber)
                .Where(t => !stockId.HasValue || t.StockID == stockId.Value)
                .Where(t => !startDate.HasValue || t.Transaction_Date >= startDate.Value)
                .Where(t => !endDate.HasValue || t.Transaction_Date <= endDate.Value)
                .ToList();
        }
        #endregion

        #region "Registration"

        public string CompleteInvestorRegistration(int webClientIDNumber, string password, string twoFactorCode, short investorNotificationTypeId, string completeRegistrationToken)
        {
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();

            TokenBO smsTokenDetails = TokenService.GetTokenDetails(twoFactorCode, NotificationTokenType.CompleteRegistrationSmsVerification.Id);
            smsTokenDetails.Assert("Code is invalid.", t => t != null && t.WebClientIDNumber == webClientIDNumber);
            RegistrationAccountBO account = this.UpdateInvestorToComplete(webClientIDNumber);
            SetInvestorRegistrationPassword(account.ClientAccountNumber, password);

            // retrieve user files and add them to final folder
            //IList<RegistrationDocumentBO> registrationDocuments = GetRegistrationDocumentsByUser(webClientIDNumber);
            //foreach (RegistrationDocumentBO registrationDocument in registrationDocuments)
            //{
            //    DocumentBO doc = DocumentService.UploadS3Document(account.ClientIDNumber, account.ClientAccountNumber, "Registration Document", registrationDocument.Document_Description, registrationDocument.FileName, registrationDocument.Document_Link, null);
            //    DocumentService.MarkDocumentReadByClient(doc.ID_Number);
            //    //registrationDocument.Document_Link = doc.WebDocPath;
            //    //UpdateRegistrationDocument(registrationDocument);
            //}
            try
            {
                TokenBO emailTokenDetails = TokenService.GetTokenDetails(completeRegistrationToken, investorNotificationTypeId);
                emailTokenDetails.Token_Used_Date = DateTime.Now;
                TokenService.UpdateToken(emailTokenDetails);

                smsTokenDetails.Token_Used_Date = DateTime.Now;
                TokenService.UpdateToken(smsTokenDetails);

                UserBasicBO user = UserService.GetUserBasicByAccountNumber(account.ClientAccountNumber);

                Uri url = HttpContext.Current.Request.Url;
                string loginUrl = string.Join("/", url.AbsoluteUri.Substring(0, url.AbsoluteUri.IndexOf(url.AbsolutePath)), "client-login");

                StringBuilder content = new StringBuilder();

                string name = string.Empty;
                if (user != null && user.IsClient)
                {
                    ClientDetailsBO clientDetails = UserService.GetClientDetailsByAccountNumber(account.ClientAccountNumber);
                    if (clientDetails.IsIndividualClient)
                    {
                        name = string.Join(" ", clientDetails.Title, clientDetails.LastName);
                    }
                    else if (clientDetails.IsJointClient)
                    {
                        name = string.Join(" & ", string.Join(" ", clientDetails.Title, clientDetails.LastName), string.Join(" ", clientDetails.Title2, clientDetails.LastName2));
                    }
                    else if (clientDetails.IsNonIndividualClient)
                    {
                        name = string.Join(" ", clientDetails.ContactTitle, clientDetails.ContactLastName);
                    }
                    else
                    {
                        name = clientDetails.DefaultRegistration;
                    }
                }
                //else if (user != null && user.IsAgent)
                //{
                //    AgentDetailsVO agentDetails = GetAgentDetailsByAccountNumber(account.ClientAccountNumber);
                //    name = agentDetails.DisplayName;
                //}
                content.AppendLine(string.Format("Hello {0},<br><br>", name));
                content.AppendLine("You have now completed your registration.<br>");
                content.AppendLine(string.Format("<strong>Important Note:</strong> Your BCP Account Number/Username is as follows: {0}.<br><br>", account.ClientAccountNumber));
                content.AppendLine(string.Format("Please <a href='{0}'>click here</a> to login to your account.<br><br>", loginUrl));
                content.AppendLine("Need to contact us? Send us an e-mail @ <a href='mailto:salessupport@bcp.ie'>salessupport@bcp.ie</a> or call us on <a href='tel:016684688'>01 6684688</a>.<br><br>");
                content.AppendLine("We look forward to hearing from you!<br>");
                //content.AppendLine("BCP Asset Management<br>");
                content.AppendLine("<strong>PS: Please don't respond to this automatically generated email.</strong>");

                EmailService.SendEmail(new EmailBO()
                {
                    From = ApplicationSettings.Instance.MailFrom,
                    To = user.Email,
                    Subject = "Welcome to BCP Vespro - Registration Complete",
                    Content = content.ToString()
                });
                //transaction.Commit();
            }
            catch (Exception ex)
            {
                //transaction.Rollback();
                throw ex;
            }
            if (account != null)
                return account.ClientAccountNumber;
            else
                return null;
        }
              
        public IList<RegistrationDeclarationBO> GetRegistrationDeclarations(int webClientIDNumber, short clientType, string clientSubType)
        {
            IList<InvestorDeclarationBO> investorDeclarations = GetRegistrationDeclarationsByType(clientType, clientSubType);

            IList<RegistrationDeclarationBO> registrationDeclarations = GetRegistrationDeclarationsByUser(webClientIDNumber);

            return investorDeclarations.Select(id => new RegistrationDeclarationBO()
            {
                DeclarationID = registrationDeclarations.Where(rd => rd.Dec_Label == id.Dec_Label).Select(rd => rd.DeclarationID).FirstOrDefault(),
                Dec_Label = id.Dec_Label,
                Dec_Marketing = id.Dec_Marketing,
                WebClientIDNumber = webClientIDNumber,
                Dec_Signed = registrationDeclarations.Where(rd => rd.Dec_Label == id.Dec_Label).Select(rd => rd.Dec_Signed).FirstOrDefault(),
                Sort_Order = id.Sort_Order
            }).OrderBy(id => id.Sort_Order).ToList();
        }

        public IList<RegistrationDeclarationBO> AddRegistrationDeclarations(IList<RegistrationDeclarationBO> registrationDeclarations)
        {
            if (registrationDeclarations == null) throw new ValidationException("At least 1 declaration is required.");
            registrationDeclarations.Assert("Investor is required.", r => r.Any(rd => rd.WebClientIDNumber > 0));
            registrationDeclarations.Assert("One or more declaration statements are required.", r => r.Any(rd => !string.IsNullOrWhiteSpace(rd.Dec_Label)));
            registrationDeclarations.Assert("One or more declaration agreement are required.", r => r.Any(rd => rd.Dec_Marketing || rd.Dec_Signed.HasValue));
            for (int i = 0; i < registrationDeclarations.Count; i++)
            {
                if (registrationDeclarations[i].DeclarationID == 0)
                {
                    registrationDeclarations[i] = AddRegistrationDeclaration(registrationDeclarations[i]);
                }
                else
                {
                    UpdateRegistrationDeclaration(registrationDeclarations[i]);
                }
            }
            return registrationDeclarations;
        }

        public IList<RegistrationDocumentBO> GetRegistrationDocuments(int webClientIDNumber, string clientSubType)
        {
            IList<InvestorDocumentBO> investorDocuments = GetRegistrationDocumentsByType(clientSubType);
            IList<RegistrationDocumentBO> registrationDocuments = GetRegistrationDocumentsByUser(webClientIDNumber);

            return investorDocuments.Select(id => new RegistrationDocumentBO()
            {
                WebDocID = id.WebDocID,
                Document_Description = id.Document_Description,
                Document_Link = id.Document_Link,
                Sort_Order = id.Sort_Order,
                WebClientIDNumber = webClientIDNumber,
                Receipt_Confirmed = registrationDocuments.Where(rd => rd.Document_Description == id.Document_Description && rd.Document_Link == rd.Document_Link).Select(rd => rd.Receipt_Confirmed).FirstOrDefault(),
                Viewed = registrationDocuments.Where(rd => rd.Document_Description == id.Document_Description && rd.Document_Link == rd.Document_Link).Select(rd => rd.Viewed).FirstOrDefault(),
                Viewed_Date = registrationDocuments.Where(rd => rd.Document_Description == id.Document_Description && rd.Document_Link == rd.Document_Link).Select(rd => rd.Viewed_Date).FirstOrDefault(),
                WebRegDocID = registrationDocuments.Where(rd => rd.Document_Description == id.Document_Description && rd.Document_Link == rd.Document_Link).Select(rd => rd.WebRegDocID).FirstOrDefault()
            }).OrderBy(id => id.Sort_Order).ToList();
        }

        public IList<RegistrationDocumentBO> AddRegistrationDocuments(IList<RegistrationDocumentBO> registrationDocuments)
        {
            if (registrationDocuments == null) throw new ValidationException("At least 1 document is required.");
            registrationDocuments.Assert("Investor is required.", r => r.Any(rd => rd.WebClientIDNumber > 0));
            registrationDocuments.Assert("Document review is required.", r => r.Any(rd => rd.Viewed.HasValue));
            registrationDocuments.Assert("Document review is required.", r => r.Any(rd => rd.Viewed_Date.HasValue));
            registrationDocuments.Assert("Document receipt is required.", r => r.Any(rd => rd.Receipt_Confirmed.HasValue));
            for (int i = 0; i < registrationDocuments.Count; i++)
            {
                if (registrationDocuments[i].WebRegDocID == 0)
                {
                    registrationDocuments[i] = AddRegistrationDocument(registrationDocuments[i]);
                }
                else
                {
                    UpdateRegistrationDocument(registrationDocuments[i]);
                }
            }
            return registrationDocuments;
        }


        #endregion

        #region internal methods
        private IList<InvestorDocumentBO> GetRegistrationDocumentsByType(string clientSubType)
        {
            bool arf = "ARF".Equals(clientSubType, StringComparison.OrdinalIgnoreCase),
                amrf = "AMRF".Equals(clientSubType, StringComparison.OrdinalIgnoreCase),
                prb = "PRB".Equals(clientSubType, StringComparison.OrdinalIgnoreCase),
                cu = "CU".Equals(clientSubType, StringComparison.OrdinalIgnoreCase);
            IList<InvestorDocumentBO> investorDocuments = Db.DataBase.SqlQuery<InvestorDocumentBO>("exec CRM_Get_Web_Registration_Documents")
                .Where(id => id.Incl_All || (id.Incl_ARF && arf) || (id.Incl_AMRF && amrf) || (id.Incl_PRB && prb) || (id.Incl_CU && cu))
                .OrderBy(id => id.Sort_Order)
                .ToList();

            return investorDocuments;
        }

        private IList<RegistrationDocumentBO> GetRegistrationDocumentsByUser(int webClientIDNumber)
        {
            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);

            IList<RegistrationDocumentBO> registrationDocuments = Db.DataBase.SqlQuery<RegistrationDocumentBO>("exec CRM_Get_Registration_Documents_All @WebClientIDNumber", WebClientIDNumber)
                .ToList();

            return registrationDocuments;
        }

        private RegistrationDocumentBO AddRegistrationDocument(RegistrationDocumentBO registrationDocument)
        {
            registrationDocument.Assert("Investor is required.", rd => rd.WebClientIDNumber > 0);
            registrationDocument.Assert("Document review is required.", rd => rd.Viewed.HasValue);
            registrationDocument.Assert("Document review is required.", rd => rd.Viewed_Date.HasValue);
            registrationDocument.Assert("Document receipt is required.", rd => rd.Receipt_Confirmed.HasValue);

            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", registrationDocument.WebClientIDNumber, SqlDbType.Int);
            SqlParameter Document_Description = CreateSqlParameter("Document_Description", registrationDocument.Document_Description, SqlDbType.Text);
            SqlParameter Document_Link = CreateSqlParameter("Document_Link", registrationDocument.Document_Link, SqlDbType.Text);
            SqlParameter Viewed = CreateSqlParameter("Viewed", registrationDocument.Viewed, SqlDbType.Bit);
            SqlParameter Viewed_Date = CreateSqlParameter("Viewed_Date", registrationDocument.Viewed_Date, SqlDbType.DateTime2);
            SqlParameter Receipt_Confirmed = CreateSqlParameter("Receipt_Confirmed", registrationDocument.Receipt_Confirmed, SqlDbType.Bit);

            registrationDocument.WebRegDocID = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Registration_Document @WebClientIDNumber, @Document_Description, @Document_Link, @Viewed, @Viewed_Date, @Receipt_Confirmed",
                WebClientIDNumber, Document_Description, Document_Link, Viewed, Viewed_Date, Receipt_Confirmed).FirstOrDefault();
            return registrationDocument;
        }

        private RegistrationDocumentBO UpdateRegistrationDocument(RegistrationDocumentBO registrationDocument)
        {
            registrationDocument.Assert("Document is required.", rd => rd.WebRegDocID > 0);
            registrationDocument.Assert("Document review is required.", rd => rd.Viewed.HasValue);
            registrationDocument.Assert("Document review is required.", rd => rd.Viewed_Date.HasValue);
            registrationDocument.Assert("Document receipt is required.", rd => rd.Receipt_Confirmed.HasValue);

            SqlParameter WebRegDocID = CreateSqlParameter("WebRegDocID", registrationDocument.WebRegDocID, SqlDbType.Int);
            SqlParameter Document_Description = CreateSqlParameter("Document_Description", registrationDocument.Document_Description, SqlDbType.Text);
            SqlParameter Document_Link = CreateSqlParameter("Document_Link", registrationDocument.Document_Link, SqlDbType.Text);
            SqlParameter Viewed = CreateSqlParameter("Viewed", registrationDocument.Viewed, SqlDbType.Bit);
            SqlParameter Viewed_Date = CreateSqlParameter("Viewed_Date", registrationDocument.Viewed_Date, SqlDbType.DateTime2);
            SqlParameter Receipt_Confirmed = CreateSqlParameter("Receipt_Confirmed", registrationDocument.Receipt_Confirmed, SqlDbType.Bit);

            string resultCode = Db.DataBase.SqlQuery<string>("exec CRM_Update_Registration_Document @WebRegDocID, @Document_Description, @Document_Link, @Viewed, @Viewed_Date, @Receipt_Confirmed",
                WebRegDocID, Document_Description, Document_Link, Viewed, Viewed_Date, Receipt_Confirmed).FirstOrDefault();
            "OK".Assert("Declaration could not be updated.", r => r.Equals(resultCode, StringComparison.OrdinalIgnoreCase));
            return registrationDocument;
        }

        private IList<InvestorDeclarationBO> GetRegistrationDeclarationsByType(short clientType, string clientSubType)
        {
            IList<InvestorDeclarationBO> InvestorDeclarations = Db.DataBase.SqlQuery<InvestorDeclarationBO>("exec CRM_Get_Web_Registration_Declarations").ToList();
            bool arf = (clientSubType ?? string.Empty).Contains("ARF"),
                 amrf = (clientSubType ?? string.Empty).Contains("AMRF"),
                 prb = (clientSubType ?? string.Empty).Contains("PRB");

            if (InvestorRegistrationType.Individual.Id == clientType)
            {
                InvestorDeclarations = InvestorDeclarations.Where(id => id.Dec_Individual || (id.BCP_ARF_Only && arf) || (id.BCP_AMRF_Only && amrf) || (id.BCP_PRB_Only && prb)).ToList();
            }
            else if (InvestorRegistrationType.Joint.Id == clientType)
            {
                InvestorDeclarations = InvestorDeclarations.Where(id => id.Dec_Joint || (id.BCP_ARF_Only && arf) || (id.BCP_AMRF_Only && amrf) || (id.BCP_PRB_Only && prb)).ToList();
            }
            else if (InvestorRegistrationType.NonIndividual.Id == clientType)
            {
                if (arf || amrf || prb)
                    InvestorDeclarations = InvestorDeclarations.Where(id => id.Dec_Individual || (id.BCP_ARF_Only && arf) || (id.BCP_AMRF_Only && amrf) || (id.BCP_PRB_Only && prb)).ToList();
                else
                    InvestorDeclarations = InvestorDeclarations.Where(id => id.Dec_NonIndividual).ToList();
            }
            return InvestorDeclarations.OrderBy(id => id.Sort_Order).ToList();
        }

        private IList<RegistrationDeclarationBO> GetRegistrationDeclarationsByUser(int webClientIDNumber)
        {
            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);

            IList<RegistrationDeclarationBO> registrationDeclarations = Db.DataBase.SqlQuery<RegistrationDeclarationBO>("exec CRM_Get_Registration_Declarations_All @WebClientIDNumber", WebClientIDNumber)
                .ToList();

            return registrationDeclarations;
        }

        private RegistrationDeclarationBO AddRegistrationDeclaration(RegistrationDeclarationBO registrationDeclaration)
        {
            registrationDeclaration.Assert("Investor is required.", rd => rd.WebClientIDNumber > 0);
            registrationDeclaration.Assert("Declaration statement is required.", rd => !string.IsNullOrWhiteSpace(rd.Dec_Label));
            registrationDeclaration.Assert("Declaration agreement is required.", rd => rd.Dec_Marketing || rd.Dec_Signed.HasValue);

            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", registrationDeclaration.WebClientIDNumber, SqlDbType.Int);
            SqlParameter Dec_Label = CreateSqlParameter("Dec_Label", registrationDeclaration.Dec_Label, SqlDbType.Text);
            SqlParameter Dec_Signed = CreateSqlParameter("Dec_Signed", (registrationDeclaration.Dec_Signed.HasValue && registrationDeclaration.Dec_Signed.Value), SqlDbType.Bit);
            SqlParameter Dec_Marketing = CreateSqlParameter("Dec_Marketing", registrationDeclaration.Dec_Marketing, SqlDbType.Bit);
            SqlParameter SortOrder = CreateSqlParameter("SortOrder", registrationDeclaration.Sort_Order, SqlDbType.SmallInt);

            registrationDeclaration.WebRegDecID = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Registration_Declaration @WebClientIDNumber, @Dec_Label, @Dec_Signed, @Dec_Marketing, @SortOrder",
                WebClientIDNumber, Dec_Label, Dec_Signed, Dec_Marketing, SortOrder).FirstOrDefault();
            return registrationDeclaration;
        }

        private RegistrationDeclarationBO UpdateRegistrationDeclaration(RegistrationDeclarationBO registrationDeclaration)
        {
            registrationDeclaration.Assert("Declaration is required.", rd => rd.DeclarationID > 0);
            registrationDeclaration.Assert("Declaration statement is required.", rd => !string.IsNullOrWhiteSpace(rd.Dec_Label));
            registrationDeclaration.Assert("Declaration agreement is required.", rd => rd.Dec_Marketing || rd.Dec_Signed.HasValue);

            SqlParameter DeclarationID = CreateSqlParameter("DeclarationID", registrationDeclaration.DeclarationID, SqlDbType.Int);
            SqlParameter Dec_Label = CreateSqlParameter("Dec_Label", registrationDeclaration.Dec_Label, SqlDbType.Text);
            SqlParameter Dec_Signed = CreateSqlParameter("Dec_Signed", (registrationDeclaration.Dec_Signed.HasValue && registrationDeclaration.Dec_Signed.Value), SqlDbType.Bit);
            SqlParameter Dec_Marketing = CreateSqlParameter("Dec_Marketing", registrationDeclaration.Dec_Marketing, SqlDbType.Bit);
            SqlParameter SortOrder = CreateSqlParameter("SortOrder", registrationDeclaration.Sort_Order, SqlDbType.SmallInt);

            string resultCode = Db.DataBase.SqlQuery<string>("exec CRM_Update_Registration_Declaration @DeclarationID, @Dec_Label, @Dec_Signed, @Dec_Marketing, @SortOrder",
                DeclarationID, Dec_Label, Dec_Signed, Dec_Marketing, SortOrder).FirstOrDefault();
            "OK".Assert("Declaration could not be updated.", r => r.Equals(resultCode, StringComparison.OrdinalIgnoreCase));
            return registrationDeclaration;
        }

        private void SetInvestorRegistrationPassword(string accountNumber, string password)
        {
            ValidatePasswordMinRequirements(password);
            string passwordSaltHash = UserService.GetPasswordSaltHash(accountNumber), passwordHash = UserService.GeneratePasswordHashSha256(password, passwordSaltHash);

            SqlParameter acNumber = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
            SqlParameter pwHash = CreateSqlParameter("PW_Hash", passwordHash, SqlDbType.Text);
            SqlParameter pwSalt = CreateSqlParameter("PW_Salt", passwordSaltHash, SqlDbType.Text);

            string response = Db.DataBase.SqlQuery<string>("exec CRM_Create_CRM_User_Record @AC_Num, @PW_Hash, @PW_Salt", acNumber, pwHash, pwSalt).FirstOrDefault();

            response.Assert(response, r => "Success".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        private void ValidatePasswordMinRequirements(string password)
        {
            Regex rNumber = new Regex("[0-9]"),
                rLowerCase = new Regex("[a-z]"),
                rUpperCase = new Regex("[A-Z]"),
                rSpecialCharacter = new Regex("[£!@#$^&*()\\-_=+\\[\\]{}|;:,.<>?]");
            string[] /*dictionary = new string[] { "najaf" },*/
                    sequenceDictionary = new string[]{ "0123", "1234", "2345", "3456", "4567", "5678", "6789", "7890", "0987", "9876", "8765", "7654", "6543", "5432", "4321", "3210",
                        "abcd", "bcde", "cdef", "defg", "efgh", "fghi", "ghij", "hijk", "ijkl", "jklm", "klmn", "lmno", "mnop", "nopq", "opqr", "pqrs", "qrst", "rstu", "stuv", "tuvw", "uvwx", "vwxy", "wxyz",
                        "zyxw", "yxwv", "xwvu", "wvut", "vuts", "utsr", "tsrq", "srqp", "rqpo", "qpon", "ponm", "onml", "nmlk", "mlkj", "lkji", "kjih", "jihg", "ihgf", "hgfe", "gfed", "fedc", "edcb", "dcba"};


            // password must have 8 characters or more
            password.Assert("Password must have 8 characters or more.", p => !string.IsNullOrWhiteSpace(p) && p.Length >= 8);
            // password must have a number
            password.Assert("Password must have a number.", p => rNumber.Match(p).Success);
            // password must have a lower case letter
            password.Assert("Password must have a lower case letter.", p => rLowerCase.Match(p).Success);
            // password must have an upper case letter
            password.Assert("Password must have an upper case letter.", p => rUpperCase.Match(p).Success);
            // password must have at least 1 of these characters: ! @ # $ ^ & * ( ) - _ =+ [ ]{ } | ; : , . < > ?
            password.Assert("Password must have at least 1 of these characters: ! @ # $ ^ & * ( ) - _ =+ [ ]{ } | ; : , . < > ?", p => rSpecialCharacter.Match(p).Success);
            //// password may NOT have a word in the dictionary
            //password.Assert("Password may not have a word in the dictionary.", p => !dictionary.Any(w => p.ToUpper().Contains(w.ToUpper())));
            // password may NOT have a sequence of letters or numbers such as "abcd" or "1234"
            password.Assert("Password may not have a sequence of letters or numbers such as \"abcd\" or \"1234\".", p => !sequenceDictionary.Any(seq => p.ToUpper().Contains(seq.ToUpper())));
        }

        private string GetInvestorPasswordSalt(string accountNumber)
        {
            accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));

            SqlParameter AcNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);

            string passwordSalt = Db.DataBase.SqlQuery<string>("exec CRM_Get_User_Salt @AC_Num", AcNum).FirstOrDefault();

            return passwordSalt;
        }

        private RegistrationAccountBO UpdateInvestorToComplete(int webClientIDNumber)
        {
            webClientIDNumber.Assert("Investor not found.", id => id > 0);

            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);

            RegistrationAccountBO response = Db.DataBase.SqlQuery<RegistrationAccountBO>("exec CRM_Update_Registration_To_Complete @WebClientIDNumber ", WebClientIDNumber).FirstOrDefault();
            response.Assert("Registration couldn't be completed.", r => !string.IsNullOrWhiteSpace(r.ClientAccountNumber) && r.ClientIDNumber > 0);

            return response;
        }

        private string GeneratePasswordSaltHash(string accountNumber)
        {
            accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));

            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);

            PasswordHashBO response = Db.DataBase.SqlQuery<PasswordHashBO>("exec CRM_Create_User_Salt @AC_Num ", acNum).FirstOrDefault();
            UserService.UpdatePasswordSaltHash(accountNumber, response.PW_Salt);
            return response.PW_Salt;
        }

        private string GetPasswordSaltHash(string accountNumber)
        {
            accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));

            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);

            string response = Db.DataBase.SqlQuery<string>("exec CRM_Get_User_Salt @AC_Num ", acNum).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(response))
            {
                response = GeneratePasswordSaltHash(accountNumber);
            }

            return response;
        }

        private string GeneratePasswordHashSha1(string password, string passwordSalt)
        {
            return Encrypter.EncryptSha1(passwordSalt, password);
        }

        private string GeneratePasswordHashSha256(string password, string passwordSalt)
        {
            return Encrypter.EncryptSha256(passwordSalt, password);
        }

        //private IList<T> GetClients<T>(string accountNumber)
        //{
        //    SqlParameter ACNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
        //    SqlParameter User_IP = CreateSqlParameter("User_IP", NetworkUtils.GetRequestIpAddress(), SqlDbType.Text);
        //    return Db.DataBase.SqlQuery<T>("exec CRM_ClientListForUser @AC_Num, @User_IP", ACNum, User_IP).ToList();
        //}
            
        private RegistrationIndividualBO GetIndividualUser(int webClientIDNumber)
        {
            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);
            RegistrationIndividualBO investor = Db.DataBase.SqlQuery<RegistrationIndividualBO>("exec CRM_Get_Permanent_Registration_Individual @WebClientIDNumber", WebClientIDNumber).FirstOrDefault();
            investor.Assert("Investor not found.", i => investor != null);
            investor.DateOfBirthString = (investor.DateOfBirth.HasValue ? investor.DateOfBirth.Value.ToString("dd/MM/yyyy") : null);
            return investor;
        }

        private RegistrationJointBO GetJointUser(int webClientIDNumber)
        {
            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);
            RegistrationJointBO investor = Db.DataBase.SqlQuery<RegistrationJointBO>("exec CRM_Get_Permanent_Registration_Joint @WebClientIDNumber", WebClientIDNumber).FirstOrDefault();
            investor.Assert("Investor not found.", i => investor != null);
            investor.DateOfBirthString = (investor.DateOfBirth.HasValue ? investor.DateOfBirth.Value.ToString("dd/MM/yyyy") : null);
            investor.DateOfBirth2String = (investor.DateOfBirth2.HasValue ? investor.DateOfBirth2.Value.ToString("dd/MM/yyyy") : null);
            return investor;
        }

        private RegistrationNonIndividualBO GetNonIndividualUser(int webClientIDNumber)
        {
            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);
            return Db.DataBase.SqlQuery<RegistrationNonIndividualBO>("exec CRM_Get_Permanent_Registration_NonInd @WebClientIDNumber", WebClientIDNumber).FirstOrDefault();
        }

        //private InvestorRecordStatsBO GetInvestorRecordStats(string username)
        //{
        //    SqlParameter AC_Num = CreateSqlParameter("AC_Num", username, SqlDbType.Text);
        //    InvestorRecordStatsVO investorRecordStats = Db.DataBase.SqlQuery<InvestorRecordStatsVO>("exec CRM_Get_CRM_User_Record @AC_Num", AC_Num).FirstOrDefault();
        //    return investorRecordStats;
        //}
       
        private UserBasicBO GetUserBasicByEmail(string email)
        {
            email.Assert("Email is required.", e => !string.IsNullOrWhiteSpace(e));

            SqlParameter AcNum = CreateSqlParameter("AC_Num", DBNull.Value, SqlDbType.Text);
            SqlParameter ACEmail = CreateSqlParameter("AC_Email", email, SqlDbType.Text);
            SqlParameter isUseEmail = CreateSqlParameter("Check_Type", 1, SqlDbType.SmallInt);
            //SqlParameter User_IP = CreateSqlParameter("User_IP", NetworkUtils.GetRequestIpAddress(), SqlDbType.Text);
            return Db.DataBase.SqlQuery<UserBasicBO>("exec CRM_Get_User_Basics @AC_Num, @AC_Email, @Check_Type", AcNum, ACEmail, isUseEmail).FirstOrDefault();
        }

        //private UserBasicBO CheckUser(UserBasicBO user)
        //{
        //    SqlParameter AC_Num = CreateSqlParameter("AC_Num", user.UserName, SqlDbType.Text);
        //    SqlParameter User_IP = CreateSqlParameter("User_IP", NetworkUtils.GetRequestIpAddress(), SqlDbType.Text);
        //    LoginCheckVO loginCheck = Db.DataBase.SqlQuery<LoginCheckVO>("exec CRM_User_Check @AC_Num, @User_IP", AC_Num, User_IP).FirstOrDefault();
        //    loginCheck.Assert(loginCheck != null && !string.IsNullOrWhiteSpace(loginCheck.Reason) ? loginCheck.Reason : "Invalid login, please check your BCP Account Number\\Username and Password and try again.", lc => lc != null && lc.Result == 1);

        //    user.LoginCheck = loginCheck;

        //    return user;
        //}
                
        //private IList<AssetClassBO> GetAssetClasses(int stockId)
        //{
        //    SqlParameter Stock_ID = CreateSqlParameter("Stock_ID", stockId, SqlDbType.Int);
        //    return Db.DataBase.SqlQuery<AssetClassBO>("exec CRM_Get_Stock_AssetClasses @Stock_ID", Stock_ID).ToList();
        //}

        //private void ActivateOnlineUser(string accountNumber)
        //{
        //    SqlParameter AC_Num = CreateSqlParameter("AC_Num", accountNumber.ToUpper(), SqlDbType.Text);
        //    ResultVO result = Db.DataBase.SqlQuery<ResultVO>("exec CRM_Update_user_crm_activated @AC_Num", AC_Num).FirstOrDefault();
        //    result.Assert("User could not be activated.", r => r != null && "OK".Equals(r.Reason, StringComparison.OrdinalIgnoreCase));
        //}

        private void UpdateClientAddress(string accountNumber, string address1, string address2, string town, string county, string postcode, string username)
        {
            SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter addr1Param = CreateSqlParameter("addr1", address1, SqlDbType.Text);
            SqlParameter addr2Param = CreateSqlParameter("addr2", address2, SqlDbType.Text);
            SqlParameter townParam = CreateSqlParameter("town", town, SqlDbType.Text);
            SqlParameter countyParam = CreateSqlParameter("county", county, SqlDbType.Text);
            SqlParameter postcodeParam = CreateSqlParameter("postcode", postcode, SqlDbType.Text);
            SqlParameter usernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Client_Address @acNum, @addr1, @addr2, @town, @county, @postcode, @username",
                acNumParam, addr1Param, addr2Param, townParam, countyParam, postcodeParam, usernameParam).FirstOrDefault();
            result.Assert("Address could not be updated.", r => r != null && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        //private void UpdateClientMobile(string accountNumber, string mobile, string user)
        //{
        //    SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
        //    SqlParameter mobileParam = CreateSqlParameter("mobile", mobile, SqlDbType.Text);
        //    SqlParameter userParam = CreateSqlParameter("user", user, SqlDbType.Text);
        //    ResultVO result = Db.DataBase.SqlQuery<ResultVO>("exec CRM_Update_User_Mobile @acNum, @mobile, @user", acNumParam, mobileParam, userParam).FirstOrDefault();
        //    result.Assert("Mobile could not be updated.", r => r != null && "OK".Equals(r.Reason, StringComparison.OrdinalIgnoreCase));
        //}

        //private void UpdateClientEmail(string accountNumber, string email, string user)
        //{
        //    SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
        //    SqlParameter emailParam = CreateSqlParameter("email", email, SqlDbType.Text);
        //    SqlParameter userParam = CreateSqlParameter("user", user, SqlDbType.Text);
        //    ResultVO result = Db.DataBase.SqlQuery<ResultVO>("exec CRM_Update_User_email @acNum, @email, @user", acNumParam, emailParam, userParam).FirstOrDefault();
        //    result.Assert("Email could not be updated.", r => r != null && "OK".Equals(r.Reason, StringComparison.OrdinalIgnoreCase));
        //}

        private void UpdateClientTaxDetails(string accountNumber, string taxResUs, string taxResIr, string taxResOther, string taxOtherCountry, string taxOtherIdNumber, string taxOtherCountry1, string taxOtherIdNumber1, string taxOtherCountry2, string taxOtherIdNumber2, string username)
        {
            SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter taxResUsParam = CreateSqlParameter("taxResUs", taxResUs, SqlDbType.Text);
            SqlParameter taxResIrParam = CreateSqlParameter("taxResIr", taxResIr, SqlDbType.Text);
            SqlParameter taxResOtherParam = CreateSqlParameter("taxResOther", taxResOther, SqlDbType.Text);
            SqlParameter taxOtherCountryParam = CreateSqlParameter("taxOtherCountry", taxOtherCountry, SqlDbType.Text);
            SqlParameter taxOtherIdNumberParam = CreateSqlParameter("taxOtherIdNumber", taxOtherIdNumber, SqlDbType.Text);
            SqlParameter taxOtherCountry1Param = CreateSqlParameter("taxOtherCountry1", taxOtherCountry1, SqlDbType.Text);
            SqlParameter taxOtherIdNumber1Param = CreateSqlParameter("taxOtherIdNumber1", taxOtherIdNumber1, SqlDbType.Text);
            SqlParameter taxOtherCountry2Param = CreateSqlParameter("taxOtherCountry2", taxOtherCountry2, SqlDbType.Text);
            SqlParameter taxOtherIdNumber2Param = CreateSqlParameter("taxOtherIdNumber2", taxOtherIdNumber2, SqlDbType.Text);
            SqlParameter usernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Client1_Tax_Details @acNum, @taxResUs, @taxResIr, @taxResOther, @taxOtherCountry, @taxOtherIdNumber, @taxOtherCountry1, @taxOtherIdNumber1, @taxOtherCountry2, @taxOtherIdNumber2, @username",
                acNumParam, taxResUsParam, taxResIrParam, taxResOtherParam, taxOtherCountryParam, taxOtherIdNumberParam, taxOtherCountry1Param, taxOtherIdNumber1Param, taxOtherCountry2Param, taxOtherIdNumber2Param, usernameParam).FirstOrDefault();
            result.Assert("Tax details could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        private void UpdateClient2TaxDetails(string accountNumber, string taxResUs, string taxResIr, string taxResOther, string taxOtherCountry, string taxOtherIdNumber, string taxOtherCountry1, string taxOtherIdNumber1, string taxOtherCountry2, string taxOtherIdNumber2, string username)
        {
            SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter taxResUsParam = CreateSqlParameter("taxResUs", taxResUs, SqlDbType.Text);
            SqlParameter taxResIrParam = CreateSqlParameter("taxResIr", taxResIr, SqlDbType.Text);
            SqlParameter taxResOtherParam = CreateSqlParameter("taxResOther", taxResOther, SqlDbType.Text);
            SqlParameter taxOtherCountryParam = CreateSqlParameter("taxOtherCountry", taxOtherCountry, SqlDbType.Text);
            SqlParameter taxOtherIdNumberParam = CreateSqlParameter("taxOtherIdNumber", taxOtherIdNumber, SqlDbType.Text);
            SqlParameter taxOtherCountry1Param = CreateSqlParameter("taxOtherCountry1", taxOtherCountry1, SqlDbType.Text);
            SqlParameter taxOtherIdNumber1Param = CreateSqlParameter("taxOtherIdNumber1", taxOtherIdNumber1, SqlDbType.Text);
            SqlParameter taxOtherCountry2Param = CreateSqlParameter("taxOtherCountry2", taxOtherCountry2, SqlDbType.Text);
            SqlParameter taxOtherIdNumber2Param = CreateSqlParameter("taxOtherIdNumber2", taxOtherIdNumber2, SqlDbType.Text);
            SqlParameter usernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Client2_Tax_Details @acNum, @taxResUs, @taxResIr, @taxResOther, @taxOtherCountry, @taxOtherIdNumber, @taxOtherCountry1, @taxOtherIdNumber1, @taxOtherCountry2, @taxOtherIdNumber2, @username",
                acNumParam, taxResUsParam, taxResIrParam, taxResOtherParam, taxOtherCountryParam, taxOtherIdNumberParam, taxOtherCountry1Param, taxOtherIdNumber1Param, taxOtherCountry2Param, taxOtherIdNumber2Param, usernameParam).FirstOrDefault();
            result.Assert("Tax details could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        private void UpdateClientBirthDetails(string accountNumber, string city, string country, string username)
        {
            SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter cityParam = CreateSqlParameter("city", city, SqlDbType.Text);
            SqlParameter countryParam = CreateSqlParameter("country", country, SqlDbType.Text);
            SqlParameter usernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Client1_Birth_Details @acNum, @city, @country, @username", acNumParam, cityParam, countryParam, usernameParam).FirstOrDefault();
            result.Assert("Birth place details could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        private void UpdateClient2BirthDetails(string accountNumber, string city, string country, string username)
        {
            SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter cityParam = CreateSqlParameter("city", city, SqlDbType.Text);
            SqlParameter countryParam = CreateSqlParameter("country", country, SqlDbType.Text);
            SqlParameter usernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Client2_Birth_Details @acNum, @city, @country, @username", acNumParam, cityParam, countryParam, usernameParam).FirstOrDefault();
            result.Assert("Birth place details could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        private void UpdateClientPhoneDetails(string accountNumber, string phone, string username)
        {
            SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter phoneParam = CreateSqlParameter("phone", phone, SqlDbType.Text);
            SqlParameter usernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Client_Phone @acNum, @phone, @username", acNumParam, phoneParam, usernameParam).FirstOrDefault();
            result.Assert("Phone details could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        private void UpdateClientPpsNumber(string accountNumber, string ppsNumber, string username)
        {
            SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter ppsNumberParam = CreateSqlParameter("ppsNumber", ppsNumber, SqlDbType.Text);
            SqlParameter usernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Client1_PPSN @acNum, @ppsNumber, @username", acNumParam, ppsNumberParam, usernameParam).FirstOrDefault();
            result.Assert("PPS number could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        private void UpdateClient2PpsNumber(string accountNumber, string ppsNumber, string username)
        {
            SqlParameter acNumParam = CreateSqlParameter("acNum", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter ppsNumberParam = CreateSqlParameter("ppsNumber", ppsNumber, SqlDbType.Text);
            SqlParameter usernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Client2_PPSN @acNum, @ppsNumber, @username", acNumParam, ppsNumberParam, usernameParam).FirstOrDefault();
            result.Assert("PPS number details could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }
        
        private IList<InvestmentDocumentBO> GetAllInvestmentDocuments(int investmentId)
        {
            SqlParameter investmentIdParam = CreateSqlParameter("investmentId", investmentId, SqlDbType.Int);

            return Db.DataBase.SqlQuery<InvestmentDocumentBO>("exec CRM_Get_Investment_Documents @investmentId", investmentIdParam).ToList();
        }

        private IList<InvestmentDocumentBO> GetInvestmentDocumentRecords(int investmentId)
        {
            SqlParameter investmentIdParam = CreateSqlParameter("investmentId", investmentId, SqlDbType.Int);

            return Db.DataBase.SqlQuery<InvestmentDocumentBO>("exec CRM_Get_Investment_Document_Records @investmentId", investmentIdParam).ToList();
        }

        private InvestmentDocumentBO SaveInvestmentDocument(InvestmentDocumentBO investmentDocument, int clientId, string clientName, UserBasicBO user)
        {
            string username = user.UserName;
            if (user.IsClient || (user.IsMasterClient && user.ClientDetails != null))
                username = user.ClientDetails.ClientAccountNumber;
            DocumentBO doc = DocumentService.UploadS3Document(clientId, username, "Investment Document", investmentDocument.Document_Description, investmentDocument.FileName, investmentDocument.Document_Link, clientName);
            if (user.IsClient || user.IsMasterClient)
                DocumentService.MarkDocumentReadByClient(doc.ID_Number);
            else if (user.IsAgent)
                DocumentService.MarkDocumentReadByAgent(doc.ID_Number, user.AgentDetails.AdvisorID);
            //investmentDocument.Document_Link = doc.WebDocPath;
            if (investmentDocument.WIDocID > 0)
                investmentDocument = UpdateInvestmentDocument(investmentDocument);
            else
                investmentDocument = CreateInvestmentDocument(investmentDocument);

            return investmentDocument;
        }

        private InvestmentDocumentBO CreateInvestmentDocument(InvestmentDocumentBO investmentDocument)
        {
            SqlParameter WinvestmentIDParam = CreateSqlParameter("WinvestmentID", investmentDocument.WInvestmentID, SqlDbType.Int);
            SqlParameter Document_DescriptionParam = CreateSqlParameter("Document_Description", investmentDocument.Document_Description, SqlDbType.Text);
            SqlParameter Document_LinkParam = CreateSqlParameter("Document_Link", investmentDocument.Document_Link, SqlDbType.Text);
            SqlParameter ViewedParam = CreateSqlParameter("Viewed", investmentDocument.Viewed, SqlDbType.Bit);
            SqlParameter ViewedDateParam = CreateSqlParameter("ViewedDate", investmentDocument.Viewed_Date, SqlDbType.DateTime2);
            SqlParameter ReceiptConfirmedParam = CreateSqlParameter("ReceiptConfirmed", investmentDocument.Receipt_Confirmed, SqlDbType.Bit);

            investmentDocument.WIDocID = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Investment_Document_Record @WinvestmentID, @Document_Description, @Document_Link, @Viewed, " +
                "@ViewedDate, @ReceiptConfirmed", WinvestmentIDParam, Document_DescriptionParam, Document_LinkParam, ViewedParam, ViewedDateParam, ReceiptConfirmedParam).FirstOrDefault();
            return investmentDocument;
        }

        private InvestmentDocumentBO UpdateInvestmentDocument(InvestmentDocumentBO investmentDocument)
        {
            SqlParameter WIDocIDParam = CreateSqlParameter("WIDocID", investmentDocument.WIDocID, SqlDbType.Int);
            SqlParameter Document_DescriptionParam = CreateSqlParameter("Document_Description", investmentDocument.Document_Description, SqlDbType.Text);
            SqlParameter Document_LinkParam = CreateSqlParameter("Document_Link", investmentDocument.Document_Link, SqlDbType.Text);
            SqlParameter ViewedParam = CreateSqlParameter("Viewed", investmentDocument.Viewed, SqlDbType.Bit);
            SqlParameter ViewedDateParam = CreateSqlParameter("ViewedDate", investmentDocument.Viewed_Date, SqlDbType.DateTime2);
            SqlParameter Receipt_ConfirmedParam = CreateSqlParameter("Receipt_Confirmed", investmentDocument.Receipt_Confirmed, SqlDbType.Bit);

            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Investment_Document_Record @WIDocID, @Document_Description, @Document_Link, @Viewed, @ViewedDate, @Receipt_Confirmed",
                WIDocIDParam, Document_DescriptionParam, Document_LinkParam, ViewedParam, ViewedDateParam, Receipt_ConfirmedParam).FirstOrDefault();
            result.Assert("Investment document could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
            return investmentDocument;
        }

        private IList<InvestmentDeclarationBO> GetAllInvestmentDeclarations(int investmentId)
        {
            SqlParameter investmentIdParam = CreateSqlParameter("investmentId", investmentId, SqlDbType.Int);

            return Db.DataBase.SqlQuery<InvestmentDeclarationBO>("exec CRM_Get_Investment_Declarations @investmentId", investmentIdParam).ToList();
        }

        private IList<InvestmentDeclarationBO> GetInvestmentDeclarationRecords(int investmentId)
        {
            SqlParameter investmentIdParam = CreateSqlParameter("investmentId", investmentId, SqlDbType.Int);

            return Db.DataBase.SqlQuery<InvestmentDeclarationBO>("exec CRM_Get_Investment_Declaration_Records @investmentId", investmentIdParam).ToList();
        }

        private InvestmentDeclarationBO SaveInvestmentDeclaration(InvestmentDeclarationBO investmentDeclaration)
        {
            if (investmentDeclaration.WIDecID > 0)
                return UpdateInvestmentDeclaration(investmentDeclaration);
            else
                return CreateInvestmentDeclaration(investmentDeclaration);
        }

        private InvestmentDeclarationBO CreateInvestmentDeclaration(InvestmentDeclarationBO investmentDeclaration)
        {
            SqlParameter WInvestmentIDParam = CreateSqlParameter("WInvestmentID", investmentDeclaration.WInvestmentID, SqlDbType.Int);
            SqlParameter Dec_LabelParam = CreateSqlParameter("Dec_Label", investmentDeclaration.Dec_Label, SqlDbType.Text);
            SqlParameter Dec_SignedParam = CreateSqlParameter("Dec_Signed", investmentDeclaration.DecSigned, SqlDbType.Bit);

            investmentDeclaration.WIDecID = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Investment_Declaration_Record @WInvestmentID, @Dec_Label, @Dec_Signed",
                WInvestmentIDParam, Dec_LabelParam, Dec_SignedParam).FirstOrDefault();
            return investmentDeclaration;
        }

        private InvestmentDeclarationBO UpdateInvestmentDeclaration(InvestmentDeclarationBO investmentDeclaration)
        {
            SqlParameter WIDecIDParam = CreateSqlParameter("WIDecID", investmentDeclaration.WIDecID, SqlDbType.Int);
            SqlParameter Dec_LabelParam = CreateSqlParameter("Dec_Label", investmentDeclaration.Dec_Label, SqlDbType.Text);
            SqlParameter Dec_SignedParam = CreateSqlParameter("Dec_Signed", investmentDeclaration.DecSigned, SqlDbType.Bit);

            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Investment_Declaration_Record @WIDecID, @Dec_Label, @Dec_Signed",
                WIDecIDParam, Dec_LabelParam, Dec_SignedParam).FirstOrDefault();
            result.Assert("Investment declaration could not be updated.", r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
            return investmentDeclaration;
        }

        #endregion
    }
}
