﻿using BCPApi.ServiceRepository;
using BCPApi.Database;
using BCPApi.Entities.BO;
using BCPApi.Entities.Domain;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BCPApi.Entities.Data;
using System.Text.RegularExpressions;
using BCPApi.Entities.Exceptions;

namespace BCPApi.ServiceRepository.DAO
{
    public class ForgotpwdRepository : CommonService, IForgotpwdRepository
    {
        protected IBaseDbContext Db { get; set; }// = new BaseDbContext();
        public ForgotpwdRepository(IBaseDbContext db)
        {
            Db = db;
        }

        private TokenServiceRepository TokenService = new TokenServiceRepository(new BaseDbContext());
        private UserAuthRepository UserService = new UserAuthRepository(new BaseDbContext());
        private EmailServiceRepository EmailService = new EmailServiceRepository();
        private SmsServiceRepository SmsService = new SmsServiceRepository();

        //private Lazy<ITokenServiceRepository> tokenService;
        //public ITokenServiceRepository TokenService { get { return tokenService.Value; } }

        //private readonly Lazy<IUserAuthRepository> userService;
        //public IUserAuthRepository UserService { get { return userService.Value; } }

        //private Lazy<IEmailServiceRepository> emailService;
        //public IEmailServiceRepository EmailService { get { return emailService.Value; } }

        //private Lazy<ISmsServiceRepository> smsService;
        //public ISmsServiceRepository SmsService { get { return smsService.Value; } }
                
        //public ForgotpwdRepository(IBaseDbContext db, Lazy<ITokenServiceRepository> tokenService, Lazy<IUserAuthRepository> userService, Lazy<IEmailServiceRepository> emailService, Lazy<ISmsServiceRepository> smsService)
        //{
        //    Db = db;
        //    this.tokenService = tokenService;
        //    this.userService = userService;
        //    this.emailService = emailService;
        //    this.smsService = smsService;
        //}

        public void SendResetPasswordEmail(string username)
        {
            string action = "Reset password", notes = "Send reset password email";
            try
            {
                UserBasicBO user = UserService.GetUserBasicByAccountNumber(username);
             
                DateTime now = DateTime.Now;
                TokenBO investorNotification = new TokenBO()
                {
                    Token_Code = TokenBO.GenerateEmailHash(),
                    AC_Num = user.UserName,
                    InvestorNotificationType_Id = NotificationTokenType.InvestorForgotPasswordEmail.Id,
                    Token_Created_Date = now,
                    Token_Expiry_Date = now.AddHours(1)
                };
                TokenService.AddToken(investorNotification);
                Uri url = HttpContext.Current.Request.Url;
                string host = url.AbsoluteUri.Substring(0, url.AbsoluteUri.IndexOf(url.AbsolutePath)),
                    resetPasswordUrl = string.Join("/", new[] { host, "reset-password", investorNotification.Token_Code });
                                
                StringBuilder content = new StringBuilder();

                string name = string.Empty;
                if (user != null)
                {
                    if (user.IsClient)
                    {
                        ClientDetailsBO clientDetails = UserService.GetClientDetailsByAccountNumber(username);
                        name = clientDetails.DefaultRegistration;
                    }
                    //else if (user.IsAgent)
                    //{
                    //    AgentDetailsBO agentDetails = GetAgentDetailsByAccountNumber(username);
                    //    name = agentDetails.DisplayName;
                    //}
                    else
                    {
                        name = user.DisplayName;
                    }
                }
                content.AppendLine(string.Format("Hello {0},<br><br>", name));
                content.AppendLine("Please click on the below link to reset your password.<br>");
                content.AppendLine(string.Format("<a href='{0}'>Reset Password</a><br><br>", resetPasswordUrl));
                content.AppendLine("Please note: This link will expire in 1 hour.<br>");
                content.AppendLine("Please disconsider this email if you didn't requested to reset your password.<br><br>");
                content.AppendLine("Kind regards,<br>");
                content.AppendLine("BCP Vespro");

                EmailService.SendEmail(new EmailBO()
                {
                    From = ApplicationSettings.Instance.MailFrom,
                    To = user.Email,
                    Subject = "Reset password",
                    Content = content.ToString()
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserBasicBO ValidateResetPasswordToken(string token)
        {
            string username = null;
            try
            {
                TokenBO tokenDetails = TokenService.GetTokenDetails(token, NotificationTokenType.InvestorForgotPasswordEmail.Id);                
                username = tokenDetails.AC_Num;
                UserBasicBO user = UserService.GetUserBasicByAccountNumber(tokenDetails.AC_Num);
                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SendResetPassword2FactorCode(string token)
        {
            string code = TokenBO.GenerateTextMessageHash();
            string action = "Reset password", notes = "Send two factor authentication code \"" + code + "\"", username = null;
            try
            {
                TokenBO tokenDetails = TokenService.GetTokenDetails(token, NotificationTokenType.InvestorForgotPasswordEmail.Id);
                username = tokenDetails.AC_Num;
                UserBasicBO user = UserService.GetUserBasicByAccountNumber(tokenDetails.AC_Num);

                DateTime now = DateTime.Now;
                string sentAuthCodeMessage = string.Empty;

                TokenBO investorNotification = new TokenBO()
                {
                    Token_Code = code,
                    AC_Num = user.UserName,
                    InvestorNotificationType_Id = NotificationTokenType.InvestorForgotPasswordSms.Id,
                    Token_Created_Date = now,
                    Token_Expiry_Date = now.AddHours(1)
                };
                TokenService.AddToken(investorNotification);
                ClientContactPreferenceBO ccp = GetClientContactPreference(username);
                if (!ApplicationSettings.Instance.SendResetPassword2FactorCodeByEmail && !string.IsNullOrWhiteSpace(user.Mobile) && (ccp == null || !ccp.SendVesproCodesToEmail))
                {
                    notes = "Send two factor authentication code \"" + code + "\" by sms to: " + user.Mobile;
                    //send sms
                    SmsService.SendSms(user.Mobile, string.Format("Your BCP Vespro reset code is {0}. Please note: This code will expire in 1 hour.", investorNotification.Token_Code));
                    sentAuthCodeMessage = "The authentication code has been sent to your registered mobile number.";
                    //AuditTrailService.AddAuditTrailRecord(username, action, notes, NetworkUtils.GetRequestIpAddress(), null, null, username, NetworkUtils.GetRequestUserAgent());
                }
                else
                {
                    notes = "Send two factor authentication code \"" + code + "\" by email to: " + user.Email;
                    //EmailService.SendEmail(new EmailBO()
                    //{
                    //    From = ApplicationSettings.Instance.MailFrom,
                    //    To = user.Email,
                    //    Subject = "Your BCP Vespro reset code",
                    //    Content = string.Format("Your BCP Vespro reset code is {0}. Please note: This code will expire in 1 hour.", investorNotification.Token_Code)
                    //});
                    sentAuthCodeMessage = "The authentication code has been sent to your registered email.";
                    
                }
                //}
                return sentAuthCodeMessage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientContactPreferenceBO GetClientContactPreference(string clientAccountNumber)
        {
            SqlParameter AC_Num = CreateSqlParameter("AC_Num", clientAccountNumber, SqlDbType.Text);
            return Db.DataBase.SqlQuery<ClientContactPreferenceBO>("exec CRM_Get_Client_Contact_Preferences @AC_Num", AC_Num).FirstOrDefault();
        }

        public void ResetPassword(string resetCode, string password, string confirmCode)
        {
            string action = "Reset password", notes = "Save new password", username = null;
            try
            {
                TokenBO tokenResetDetails = TokenService.GetTokenDetails(resetCode, NotificationTokenType.InvestorForgotPasswordEmail.Id);
                username = tokenResetDetails.AC_Num;
                //AuditTrailService.AddAuditTrailRecord(username, action, notes, NetworkUtils.GetRequestIpAddress(), null, null, username, NetworkUtils.GetRequestUserAgent());
                TokenBO tokenDetails = TokenService.GetTokenDetails(confirmCode, NotificationTokenType.InvestorForgotPasswordSms.Id);
                tokenDetails.Assert("Invalid code.", i => i != null);

                ValidatePasswordMinRequirements(password);
                string passwordSaltHash = UserService.GetPasswordSaltHash(tokenDetails.AC_Num), passwordHash = UserService.GeneratePasswordHashSha256(password, passwordSaltHash);

                UserService.UpdatePasswordHash(tokenDetails.AC_Num, passwordHash);
                UserService.UpdateInvestorRecordStats(tokenDetails.AC_Num, 0);

                tokenResetDetails.Token_Used_Date = DateTime.Now;
                TokenService.UpdateToken(tokenResetDetails);
                tokenDetails.Token_Used_Date = DateTime.Now;
                TokenService.UpdateToken(tokenDetails);
            }
            catch (Exception ex)
            {
                //AuditTrailService.AddAuditTrailRecord(username, action, notes, NetworkUtils.GetRequestIpAddress(), null, null, username, NetworkUtils.GetRequestUserAgent());
                throw ex;
            }
        }

        private void ValidatePasswordMinRequirements(string password)
        {
            Regex rNumber = new Regex("[0-9]"),
                rLowerCase = new Regex("[a-z]"),
                rUpperCase = new Regex("[A-Z]"),
                rSpecialCharacter = new Regex("[£!@#$^&*()\\-_=+\\[\\]{}|;:,.<>?]");
            string[] /*dictionary = new string[] { "najaf" },*/
                    sequenceDictionary = new string[]{ "0123", "1234", "2345", "3456", "4567", "5678", "6789", "7890", "0987", "9876", "8765", "7654", "6543", "5432", "4321", "3210",
                        "abcd", "bcde", "cdef", "defg", "efgh", "fghi", "ghij", "hijk", "ijkl", "jklm", "klmn", "lmno", "mnop", "nopq", "opqr", "pqrs", "qrst", "rstu", "stuv", "tuvw", "uvwx", "vwxy", "wxyz",
                        "zyxw", "yxwv", "xwvu", "wvut", "vuts", "utsr", "tsrq", "srqp", "rqpo", "qpon", "ponm", "onml", "nmlk", "mlkj", "lkji", "kjih", "jihg", "ihgf", "hgfe", "gfed", "fedc", "edcb", "dcba"};


            // password must have 8 characters or more
            password.Assert("Password must have 8 characters or more.", p => !string.IsNullOrWhiteSpace(p) && p.Length >= 8);
            // password must have a number
            password.Assert("Password must have a number.", p => rNumber.Match(p).Success);
            // password must have a lower case letter
            password.Assert("Password must have a lower case letter.", p => rLowerCase.Match(p).Success);
            // password must have an upper case letter
            password.Assert("Password must have an upper case letter.", p => rUpperCase.Match(p).Success);
            // password must have at least 1 of these characters: ! @ # $ ^ & * ( ) - _ =+ [ ]{ } | ; : , . < > ?
            password.Assert("Password must have at least 1 of these characters: ! @ # $ ^ & * ( ) - _ =+ [ ]{ } | ; : , . < > ?", p => rSpecialCharacter.Match(p).Success);
            //// password may NOT have a word in the dictionary
            //password.Assert("Password may not have a word in the dictionary.", p => !dictionary.Any(w => p.ToUpper().Contains(w.ToUpper())));
            // password may NOT have a sequence of letters or numbers such as "abcd" or "1234"
            password.Assert("Password may not have a sequence of letters or numbers such as \"abcd\" or \"1234\".", p => !sequenceDictionary.Any(seq => p.ToUpper().Contains(seq.ToUpper())));
        }

        private string GetInvestorPasswordSalt(string accountNumber)
        {
            SqlParameter AcNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);

            string passwordSalt = Db.DataBase.SqlQuery<string>("exec CRM_Get_User_Salt @AC_Num", AcNum).FirstOrDefault();

            return passwordSalt;
        }
    }
}
