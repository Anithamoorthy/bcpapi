﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.Exceptions
{
    public static class Extensions
    {
        public static void Assert<T>(this T model, string message, Func<T, bool> isValid)
        {
            if (!isValid(model))
            {
                throw new ValidationException(message);
            }
        }

        public static bool IsNullOrWhiteSpace(this String str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static string GetFullMessage(this Exception ex)
        {
            return GetFullExceptionMessage(ex).ToString();
        }

        private static StringBuilder GetFullExceptionMessage(Exception exception)
        {
            StringBuilder exceptionMessage = new StringBuilder();
            if (exception.InnerException != null)
                exceptionMessage = GetFullExceptionMessage(exception.InnerException);
            exceptionMessage.AppendLine(exception.Message);
            return exceptionMessage;
        }

        public static string GetFullStackTrace(this Exception ex)
        {
            if (!string.IsNullOrWhiteSpace(ex.Data["StackTrace"] as string))
                return string.Join(Environment.NewLine, ex.Data["StackTrace"], ex.StackTrace);
            return ex.StackTrace;
        }
    }
}
