﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BCPApi.Service.Security
{
    public class Encrypter
    {
        public static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);
        }

        public static string Encrypt(string value1, string value2, string value3)
        {
            return GetSHA1HashData(string.Format("{0}{1}{2}",
                    value3,
                    value1,
                    value2));
        }

        public static string EncryptSha1(string value1, string value2)
        {
            return GetSHA1HashData(string.Format("{0}{1}",
                    value2,
                    value1));
        }

        public static string EncryptSha256(string value1, string value2)
        {
            return GetSHA256HashData(string.Format("{0}{1}",
                    value2,
                    value1));
        }

        private static string GetSHA1HashData(string data)
        {
            ////create new instance of md5
            //MD5 md5 = MD5.Create();

            ////convert the input text to array of bytes
            //byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(data));

            ////create new instance of StringBuilder to save hashed data
            //StringBuilder returnValue = new StringBuilder();

            ////loop for each byte and add it to StringBuilder
            //for (int i = 0; i < hashData.Length; i++)
            //{
            //    returnValue.Append(string.Format("{0:X}", hashData[i]));
            //}

            //// return hexadecimal string
            //return returnValue.ToString();

            ////create new instance of sha1
            SHA1 sha = new SHA1CryptoServiceProvider();

            //convert the input text to array of bytes
            byte[] hashData = sha.ComputeHash(Encoding.Default.GetBytes(data));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(string.Format("{0:x2}", hashData[i]));
            }

            // return hexadecimal string
            return returnValue.ToString();
        }

        private static string GetSHA256HashData(string data)
        {
            ////create new instance of sha256
            SHA256 sha = new SHA256CryptoServiceProvider();

            //convert the input text to array of bytes
            byte[] hashData = sha.ComputeHash(Encoding.Default.GetBytes(data));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(string.Format("{0:x2}", hashData[i]));
            }

            // return hexadecimal string
            return returnValue.ToString();
        }
    }
}
