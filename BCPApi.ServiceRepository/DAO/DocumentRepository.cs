﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using BCPApi.Database;
using BCPApi.Entities.BO;
using BCPApi.ServiceRepository.Interfaces;
//using BCP.Util.Data;
//using BCP.Util.Exceptions;
//using BCP.Util.Files;
//using BCP.Util.Logging;
//using BCP.Util.Network;
//using BCP.Util.Unity.Lifetime;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BCPApi.ServiceRepository.DAO;
using BCPApi.Entities.Exceptions;
using BCPApi.Entities.Data;
using System.ComponentModel.DataAnnotations;

namespace BCPApi.ServiceRepository.DAO
{
    //[UnityIoCPerRequestLifetime]
    public class DocumentRepository : CommonService, IDocumentRepository
    {
        private static string FILE_NOT_FOUND_ERROR_MESSAGE = "File not found.";

        protected IBaseDbContext Db { get; set; }
        //private Lazy<IInvestmentRepository> investorService;
        //public IInvestmentRepository InvestorService { get { return investorService.Value; } }

        public DocumentRepository(IBaseDbContext db)//, Lazy<IInvestmentRepository> investorService)
        {
            Db = db;
            //this.investorService = investorService;
        }

        //public IList<DocumentBO> GetAgentDocuments(string accountNumber, string clientAccountNumber, string docType, bool? readStatus)
        //{
        //    ClientDetailsBO client = InvestorService.GetClientDetailsByAccountNumber(clientAccountNumber);
        //    //if (!string.IsNullOrWhiteSpace(clientAccountNumber))
        //    //{
        //    //    return GetClientDocuments((client ?? new ClientDetailsVO()).Client_ID_Number, docType, readStatus);
        //    //}
        //    //else
        //    //{
        //    SqlParameter ac_num = CreateSqlParameter("ac_num", accountNumber, SqlDbType.Text);
        //    SqlParameter User_IP = CreateSqlParameter("User_IP", NetworkUtils.GetRequestIpAddress(), SqlDbType.Text);
        //    IList<DocumentBO> documents = Db.DataBase.SqlQuery<DocumentBO>("exec CRM_Get_Documents_For_Advisor @ac_num, @User_IP", ac_num, User_IP).ToList();

        //    if (!string.IsNullOrWhiteSpace(clientAccountNumber))
        //        documents = documents.Where(d => d.Client_ID_Number == (client ?? new ClientDetailsBO()).Client_ID_Number).ToList();
        //    if (docType != null)
        //        documents = documents.Where(d => d.DocumentTypeLong == docType).ToList();
        //    if (readStatus.HasValue)
        //        documents = documents.Where(d => (readStatus.Value && d.ReadByAdvisor.HasValue && d.ReadByAdvisor > 0)
        //        || (!readStatus.Value && (!d.ReadByAdvisor.HasValue || d.ReadByAdvisor.Value == 0))).ToList();
        //    return documents;
        //    //}
        //}

        public IList<DocumentBO> GetClientDocuments(int clientId, string docType, bool? readStatus)
        {
            SqlParameter client_id = CreateSqlParameter("client_id", clientId, SqlDbType.Int);
            IList<DocumentBO> documents = Db.DataBase.SqlQuery<DocumentBO>("exec CRM_Get_Documents_For_Client @client_id", client_id).ToList();

            if (docType != null)
                documents = documents.Where(d => d.DocumentTypeLong == docType).ToList();
            if (readStatus.HasValue)
                documents = documents.Where(d => (readStatus.Value && d.ReadByClient.HasValue && d.ReadByClient.Value)
                || (!readStatus.Value && (!d.ReadByClient.HasValue || !d.ReadByClient.Value))).ToList();
            return documents;
        }

        public void MarkDocumentReadByAgent(int documentId, int agentId)
        {
            SqlParameter id_number = CreateSqlParameter("id_number", documentId, SqlDbType.Int);
            SqlParameter advisor_id = CreateSqlParameter("advisor_id", agentId, SqlDbType.Int);
            string response = Db.DataBase.SqlQuery<string>("exec CRM_Mark_Document_Read_By_Advisor @id_number, @advisor_id", id_number, advisor_id).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        public void MarkDocumentReadByClient(int documentId)
        {
            SqlParameter id_number = CreateSqlParameter("id_number", documentId, SqlDbType.Int);
            string response = Db.DataBase.SqlQuery<string>("exec CRM_Mark_Document_Read_By_Client @id_number", id_number).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        public byte[] DownloadDocument(DocumentBO file, bool isClient, int? agentId)
        {
            byte[] fileContent = DownloadS3Document(file.WebDocPath);
            if (isClient && !file.DateReadByClient.HasValue)
                MarkDocumentReadByClient(file.ID_Number);
            else if (!isClient && !!agentId.HasValue && !file.DateReadByAdvisor.HasValue)
                MarkDocumentReadByAgent(file.ID_Number, agentId.Value);
            return fileContent;
        }

        public void UploadDocument(int clientId, string username, string docType, string details, HttpPostedFileWrapper file)
        {
            ValidateDocument(file);
            DocumentBO doc = DocumentBO.CreateNew(clientId, string.Format("{0}_{1}.{2}", file.FileName.Substring(0, file.FileName.LastIndexOf('.')), DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), file.FileName.Substring(file.FileName.LastIndexOf('.') + 1)), username, docType, details, null);
            IAmazonS3 client;
            using (client = new AmazonS3Client(RegionEndpoint.EUWest1))
            {
                TransferUtility fileTransferUtility = new TransferUtility(client);

                //FileStream fs = new FileStream(FileUtil.GetFullDirectoryFromRelative(docPath), FileMode.Open);
                string realFilePath = string.Join("/", doc.WebDocPath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries));

                fileTransferUtility.Upload(file.InputStream, ApplicationSettings.Instance.AWSBucketName, realFilePath);
                IList<DocumentBO> clientDocuments = GetClientDocuments(clientId, null, null);
                DocumentBO existingDoc = clientDocuments.Where(d => doc.WebDocPath.Equals(d.WebDocPath, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (existingDoc != null)
                {
                    DeleteDocument(existingDoc.ID_Number);
                }
                InsertDocument(doc);
            }
        }

        public void UploadClientDocument(int? clientId, string username, string docType, string details, HttpPostedFileWrapper file, string clientName, string agentCode)
        {
            ValidateDocument(file);
            DocumentBO doc = DocumentBO.CreateNew(clientId, string.Format("{0}_{1}.{2}", file.FileName.Substring(0, file.FileName.LastIndexOf('.')), DateTime.Now.ToString("yyyyMMddHHmmssfffffff"), file.FileName.Substring(file.FileName.LastIndexOf('.') + 1)), username, docType, details, clientName, agentCode);
            IAmazonS3 client;
            using (client = new AmazonS3Client(RegionEndpoint.EUWest1))
            {
                TransferUtility fileTransferUtility = new TransferUtility(client);

                //FileStream fs = new FileStream(FileUtil.GetFullDirectoryFromRelative(docPath), FileMode.Open);
                string realFilePath = string.Join("/", doc.WebDocPath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries));

                fileTransferUtility.Upload(file.InputStream, ApplicationSettings.Instance.AWSBucketName, realFilePath);
                IList<DocumentBO> existingDocuments = new List<DocumentBO>();
                if (clientId.HasValue)
                    existingDocuments = GetClientDocuments(clientId.Value, null, null);
                //else
                //    existingDocuments = GetAgentDocuments(username, null, null, null);
                DocumentBO existingDoc = existingDocuments.Where(d => doc.WebDocPath.Equals(d.WebDocPath, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (existingDoc != null)
                {
                    DeleteDocument(existingDoc.ID_Number);
                }
                InsertClientDocument(doc);
            }
        }

        public DocumentBO UploadS3Document(int clientId, string username, string docType, string details, string docName, string docPath, string clientName)
        {
            DocumentBO doc = DocumentBO.CreateNew(clientId, string.Format("{0}_{1}.{2}", docName.Substring(0, docName.LastIndexOf('.')), DateTime.Now.ToString("yyyyMMddHHmmss.fffffff"), docName.Substring(docName.LastIndexOf('.') + 1)), username, docType, details, clientName);
            IAmazonS3 client;
            using (client = new AmazonS3Client(RegionEndpoint.EUWest1))
            {
                TransferUtility fileTransferUtility = new TransferUtility(client);

                WebClient webClient;
                using (webClient = new WebClient())
                {
                    Stream stream;
                    string url = (docPath ?? string.Empty).Split(new string[] { "#" }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                    using (stream = webClient.OpenRead(url))
                    {
                        string realFilePath = string.Join("/", doc.WebDocPath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries));
                        MemoryStream memoryStream = new MemoryStream();

                        byte[] buffer = new byte[2048];
                        int bytesRead;
                        while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            memoryStream.Write(buffer, 0, bytesRead);
                        }
                        memoryStream.Position = 0;
                        fileTransferUtility.Upload(memoryStream, ApplicationSettings.Instance.AWSBucketName, realFilePath);
                    }
                }
                IList<DocumentBO> clientDocuments = GetClientDocuments(clientId, null, null);
                DocumentBO existingDoc = clientDocuments.Where(d => doc.WebDocPath.Equals(d.WebDocPath, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (existingDoc != null)
                {
                    DeleteDocument(existingDoc.ID_Number);
                }
                if (!string.IsNullOrWhiteSpace(clientName))
                    InsertDocument(doc);
                else
                    InsertClientDocument(doc);
                return doc;
            }
        }

        public void ValidateFileBeforeUpload(HttpPostedFileWrapper file, int? clientId, string docName, string username, string agentCode)
        {
            file.Assert("File is required", f => f != null);
            file.Assert("File is too big, maximum size is 70MB", f => f.ContentLength < 73400320);
            file.Assert("Invalid type selected, Please select one of the following file types: pdf, jpg, jpeg, png", f => new string[] { "pdf", "jpg", "jpeg", "png" }.Any(t => t.Equals(file.FileName.Split('.').LastOrDefault(), StringComparison.OrdinalIgnoreCase)));
            DocumentBO doc = DocumentBO.CreateNew(clientId, docName, null, null, null, null, agentCode);
            try
            {
                byte[] s3File = DownloadS3Document(doc.WebDocPath);
                s3File.Assert("There is already a file with same name.", f => f == null);
            }
            catch (ValidationException ex)
            {
                if (ex.Message != FILE_NOT_FOUND_ERROR_MESSAGE) throw ex;
            }
            IList<DocumentBO> existingDocuments = new List<DocumentBO>();
            if (clientId.HasValue)
                existingDocuments = GetClientDocuments(clientId.Value, null, null);
            //else
            //    existingDocuments = GetAgentDocuments(username, null, null, null);
            existingDocuments.Assert("There is already a file with same name.", cd => cd == null || !cd.Any(d => doc.WebDocPath.Equals(d.WebDocPath, StringComparison.OrdinalIgnoreCase)));
        }

        public byte[] DownloadS3Document(string path)
        {
            string realFilePath = string.Join("/", path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries));
            byte[] fileContent = null;
            try
            {
                GetObjectRequest request = new GetObjectRequest()
                {
                    BucketName = ApplicationSettings.Instance.AWSBucketName,
                    Key = realFilePath
                };

                IAmazonS3 client;
                using (client = new AmazonS3Client(RegionEndpoint.EUWest1))
                {
                    using (GetObjectResponse response = client.GetObject(request))
                    {

                        using (Stream rs = response.ResponseStream)
                        {
                            using (var stream = new MemoryStream())
                            {
                                byte[] buffer = new byte[2048]; // read in chunks of 2KB
                                int bytesRead;
                                while ((bytesRead = rs.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    stream.Write(buffer, 0, bytesRead);
                                }
                                fileContent = stream.ToArray();
                            }
                        }
                    }
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    //LoggerFile.AppendLogSafe("Please check the provided AWS Credentials. If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3");
                }
                throw new ValidationException(FILE_NOT_FOUND_ERROR_MESSAGE);
            }
            return fileContent;
        }

        private void ValidateDocument(HttpPostedFileWrapper file)
        {
            file.Assert("File not selected", f => f != null);
            file.Assert("Invalid type selected, Please select one of the following file types: pdf,jpg,jpeg,png", f => !"application/octetstream".Equals(f.ContentType, StringComparison.OrdinalIgnoreCase));
            file.Assert("Invalid type selected, Please select one of the following file types: pdf,jpg,jpeg,png", f => "pdf,jpg,jpeg,png".Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Contains(f.FileName.ToLower().Substring(f.FileName.LastIndexOf(".")).Replace(".", string.Empty)));
        }

        private DocumentBO InsertDocument(DocumentBO document)
        {
            SqlParameter client_id = CreateSqlParameter("client_id", document.Client_ID_Number, SqlDbType.Int);
            SqlParameter agent_code = CreateSqlParameter("agent_code", document.AgentCode, SqlDbType.Text);
            SqlParameter item_date = CreateSqlParameter("item_date", document.ItemDate, SqlDbType.DateTime);
            SqlParameter document_type_long = CreateSqlParameter("document_type_long", document.DocumentTypeLong, SqlDbType.Text);
            SqlParameter details = CreateSqlParameter("details", document.Details, SqlDbType.Text);
            SqlParameter pdf_document_name = CreateSqlParameter("pdf_document_name", document.PDFDocumentName, SqlDbType.Text);
            SqlParameter upload_path = CreateSqlParameter("upload_path", document.Uploadpath, SqlDbType.Text);
            SqlParameter full_doc_path = CreateSqlParameter("full_doc_path", document.FullDocPath, SqlDbType.Text);
            SqlParameter web_doc_path = CreateSqlParameter("web_doc_path", document.WebDocPath, SqlDbType.Text);
            SqlParameter uploaded_by = CreateSqlParameter("uploaded_by", document.UploadedBy, SqlDbType.Text);
            document.ID_Number = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Insert_Document @client_id, @agent_code, @item_date, @document_type_long, @details, @pdf_document_name," +
                " @upload_path, @full_doc_path, @web_doc_path, @uploaded_by", client_id, agent_code, item_date, document_type_long,
                details, pdf_document_name, upload_path, full_doc_path, web_doc_path, uploaded_by).FirstOrDefault();
            return document;
        }

        private DocumentBO InsertClientDocument(DocumentBO document)
        {
            SqlParameter Client_ID_NumberParam = CreateSqlParameter("Client_ID_Number", document.Client_ID_Number, SqlDbType.Int);
            SqlParameter AgentCodeParam = CreateSqlParameter("AgentCode", document.AgentCode, SqlDbType.Text);
            SqlParameter ItemDateParam = CreateSqlParameter("ItemDate", document.ItemDate, SqlDbType.DateTime);
            SqlParameter DocumentTypeLongParam = CreateSqlParameter("DocumentTypeLong", document.DocumentTypeLong, SqlDbType.Text);
            SqlParameter DetailsParam = CreateSqlParameter("Details", document.Details, SqlDbType.Text);
            SqlParameter PDFDocumentNameParam = CreateSqlParameter("PDFDocumentName", document.PDFDocumentName, SqlDbType.Text);
            SqlParameter UploadPathParam = CreateSqlParameter("UploadPath", document.Uploadpath, SqlDbType.Text);
            SqlParameter FullDocPathParam = CreateSqlParameter("FullDocPath", document.FullDocPath, SqlDbType.Text);
            SqlParameter WebDocPathParam = CreateSqlParameter("WebDocPath", document.WebDocPath, SqlDbType.Text);
            SqlParameter UploadedByParam = CreateSqlParameter("UploadedBy", document.UploadedBy, SqlDbType.Text);
            SqlParameter ClientNameParam = CreateSqlParameter("ClientName", document.ClientName, SqlDbType.Text);
            SqlParameter CertifiedParam = CreateSqlParameter("Certified", true, SqlDbType.Bit);
            document.ID_Number = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Insert_Document_By_Agent @Client_ID_Number, @AgentCode, @ItemDate, @DocumentTypeLong, @Details, @PDFDocumentName," +
                " @UploadPath, @FullDocPath, @WebDocPath, @UploadedBy, @ClientName, @Certified", Client_ID_NumberParam, AgentCodeParam, ItemDateParam, DocumentTypeLongParam, DetailsParam,
                PDFDocumentNameParam, UploadPathParam, FullDocPathParam, WebDocPathParam, UploadedByParam, ClientNameParam, CertifiedParam).FirstOrDefault();
            return document;
        }

        private void DeleteDocument(int id)
        {
            SqlParameter id_number = CreateSqlParameter("id_number", id, SqlDbType.Int);
            string response = Db.DataBase.SqlQuery<string>("exec CRM_Delete_Document @id_number", id_number).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }

        private void UpdateDocument(DocumentBO document)
        {
            SqlParameter id_number = CreateSqlParameter("id_number", document.ID_Number, SqlDbType.Int);
            SqlParameter superseded = CreateSqlParameter("superseded", document.Superseded, SqlDbType.Bit);
            SqlParameter id_superseded_by = CreateSqlParameter("id_superseded_by", document.ID_NumberSupersededBy, SqlDbType.Int);
            SqlParameter pdf_document_name = CreateSqlParameter("pdf_document_name", document.PDFDocumentName, SqlDbType.Text);
            SqlParameter orig_doc_name = CreateSqlParameter("orig_doc_name", document.OriginalPDFDocumentName, SqlDbType.Text);
            SqlParameter full_doc_path = CreateSqlParameter("full_doc_path", document.FullDocPath, SqlDbType.Text);
            SqlParameter web_doc_path = CreateSqlParameter("web_doc_path", document.WebDocPath, SqlDbType.Text);
            SqlParameter uploaded_by = CreateSqlParameter("uploaded_by", document.UploadedBy, SqlDbType.Text);
            string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_Document @id_number, @superseded, @id_superseded_by, @pdf_document_name, @orig_doc_name," +
                " @full_doc_path, @web_doc_path, @uploaded_by", id_number, superseded, id_superseded_by, pdf_document_name, orig_doc_name, full_doc_path, web_doc_path, uploaded_by).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
        }
    }
}
