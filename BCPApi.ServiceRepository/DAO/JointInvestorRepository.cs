﻿using BCPApi.Database;
using BCPApi.Entities.BO;
using BCPApi.Entities.BO.Registration;
using BCPApi.Entities.Data;
using BCPApi.Entities.Domain;
using BCPApi.Entities.Exceptions;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class JointInvestorRepository: BaseRegistrationService, IJointInvestorRepository
    {
        protected IBaseDbContext Db { get; set; }
        private Lazy<ITokenServiceRepository> tokenService;
        public ITokenServiceRepository TokenService { get { return tokenService.Value; } }
        private Lazy<IEmailServiceRepository> emailService;
        public IEmailServiceRepository EmailService { get { return emailService.Value; } }
        private Lazy<ISmsServiceRepository> smsService;
        public ISmsServiceRepository SmsService { get { return smsService.Value; } }
        //private Lazy<IAuditTrailService> auditTrailService;
        //public IAuditTrailService AuditTrailService { get { return auditTrailService.Value; } }

        public JointInvestorRepository(IBaseDbContext db, Lazy<ITokenServiceRepository> tokenService, Lazy<IEmailServiceRepository> emailService, Lazy<ISmsServiceRepository> smsService)//, Lazy<IAuditTrailService> auditTrailService)
        {
            Db = db;
            this.tokenService = tokenService;
            this.emailService = emailService;
            this.smsService = smsService;
            //this.auditTrailService = auditTrailService;
        }

        public RegistrationJointFieldsBO GetRegistrationFields()
        {
            return Db.DataBase.SqlQuery<RegistrationJointFieldsBO>("exec CRM_Get_Web_Registration_Fields_Joint").FirstOrDefault();
        }

        public RegistrationJointBO GetRegistration(int webClientIDNumber)
        {
            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);

            RegistrationJointBO investor = Db.DataBase.SqlQuery<RegistrationJointBO>("exec CRM_Get_Registration_Joint @WebClientIDNumber", WebClientIDNumber).FirstOrDefault();
            investor.Assert("Investor not found.", i => investor != null);
            investor.DateOfBirthString = (investor.DateOfBirth.HasValue ? investor.DateOfBirth.Value.ToString("dd/MM/yyyy") : null);
            investor.DateOfBirth2String = (investor.DateOfBirth2.HasValue ? investor.DateOfBirth2.Value.ToString("dd/MM/yyyy") : null);
            investor.WebClientIDNumber = webClientIDNumber;
            return investor;
        }

        private void ValidateJointRegistration(RegistrationJointBO investor)
        {
            investor.Assert("Investor Type is required.", i => !string.IsNullOrWhiteSpace(i.InvestorTypeEntered));
            if (!investor.NoAgent)
            {
                if (investor.RegistrationInProgress && !investor.RegistrationComplete)
                {
                    investor.Assert("Advisor First Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentFirstName));
                    investor.Assert("Advisor Last Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentLastName));
                    investor.Assert("Advisor Company Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentName));
                    //investor.Assert("Advisor Address Line 1 is required.", i => !string.IsNullOrWhiteSpace(i.AgentAddress1));
                    investor.Assert("Advisor City is required.", i => !string.IsNullOrWhiteSpace(i.AgentCity));
                    investor.Assert("Advisor County is required.", i => !string.IsNullOrWhiteSpace(i.AgentCounty));

                    investor.AgentContact = string.Join(" ", new string[] { investor.AgentFirstName, investor.AgentLastName });
                    investor.AgentAddress = string.Join("\r\n", new string[] { investor.AgentAddress1, investor.AgentAddress2, investor.AgentCity, investor.AgentCounty, investor.AgentPostcode }.Where(a => !string.IsNullOrWhiteSpace(a)));
                }
                investor.Assert("Advisor Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentContact));
                investor.Assert("Advisor Company Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentName));
                investor.Assert("Advisor Adress is required.", i => !string.IsNullOrWhiteSpace(i.AgentAddress));
            }
            investor.Assert("Investor 1 Title is required.", i => !string.IsNullOrWhiteSpace(i.Title));
            investor.Assert("Investor 1 First Name is required.", i => !string.IsNullOrWhiteSpace(i.FirstName));
            investor.Assert("Investor 1 Last Name is required.", i => !string.IsNullOrWhiteSpace(i.LastName));
            investor.Assert("Investor 1 Date of Birth is required.", i => !string.IsNullOrWhiteSpace(i.DateOfBirthString));
            try
            {
                investor.DateOfBirth = Convert.ToDateTime(DateTime.ParseExact(investor.DateOfBirthString, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                throw new ValidationException("Investor 1 Date of Birth is invalid.", ex);
            }

            investor.Assert("Investor 2 tle is required.", i => !string.IsNullOrWhiteSpace(i.Title2));
            investor.Assert("Investor 2 First Name is required.", i => !string.IsNullOrWhiteSpace(i.FirstName2));
            investor.Assert("Investor 2 Last Name is required.", i => !string.IsNullOrWhiteSpace(i.LastName2));
            investor.Assert("Investor 2 Date of Birth is required.", i => !string.IsNullOrWhiteSpace(i.DateOfBirth2String));
            try
            {
                investor.DateOfBirth2 = Convert.ToDateTime(DateTime.ParseExact(investor.DateOfBirth2String, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                throw new ValidationException("Investor 2 Date of Birth is invalid.", ex);
            }

            investor.Assert("Email is required.", i => !string.IsNullOrWhiteSpace(i.Onlineemail));
            investor.Assert("Mobile is required.", i => !string.IsNullOrWhiteSpace(i.Mobile));
            investor.Assert("Address Line 1 is required.", i => !string.IsNullOrWhiteSpace(i.Address1));
            investor.Assert("City is required.", i => !string.IsNullOrWhiteSpace(i.Town_City));
            investor.Assert("County is required.", i => !string.IsNullOrWhiteSpace(i.County_ZipCode));
            investor.Assert("Country is required.", i => !string.IsNullOrWhiteSpace(i.Address_Country));
        }

        private RegistrationJointBO AddJointRegistration(RegistrationJointBO investor)
        {
            ValidateJointRegistration(investor);

            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", DateTime.Now, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", investor.RegistrationInProgress, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", false, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Title = CreateSqlParameter("Title", investor.Title, SqlDbType.Text);
            SqlParameter FirstName = CreateSqlParameter("FirstName", investor.FirstName, SqlDbType.Text);
            SqlParameter LastName = CreateSqlParameter("LastName", investor.LastName, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter DateOfBirth = CreateSqlParameter("DateOfBirth", investor.DateOfBirth, SqlDbType.DateTime2);
            SqlParameter Person1CountryOfBirth = CreateSqlParameter("Person1CountryOfBirth", investor.Person1CountryOfBirth, SqlDbType.Text);
            SqlParameter Person1PlaceOfBirth = CreateSqlParameter("Person1PlaceOfBirth", investor.Person1PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number = CreateSqlParameter("PPS_Number", investor.PPS_Number, SqlDbType.Text);
            SqlParameter Title2 = CreateSqlParameter("Title2", investor.Title2, SqlDbType.Text);
            SqlParameter FirstName2 = CreateSqlParameter("FirstName2", investor.FirstName2, SqlDbType.Text);
            SqlParameter LastName2 = CreateSqlParameter("LastName2", investor.LastName2, SqlDbType.Text);
            SqlParameter DateOfBirth2 = CreateSqlParameter("DateOfBirth2", investor.DateOfBirth2, SqlDbType.DateTime2);
            SqlParameter Person2CountryOfBirth = CreateSqlParameter("Person2CountryOfBirth", investor.Person2CountryOfBirth, SqlDbType.Text);
            SqlParameter Person2PlaceOfBirth = CreateSqlParameter("Person2PlaceOfBirth", investor.Person2PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number2 = CreateSqlParameter("PPS_Number2", investor.PPS_Number2, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);

            investor.WebClientIDNumber = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Registration_Joint @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, @InvestorTypeEntered, " +
                "@InvestorSubTypeEntered, @Title, @FirstName, @LastName, @Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, @DateOfBirth, " +
                "@Person1CountryOfBirth, @Person1PlaceOfBirth, @PPS_Number, @Title2, @FirstName2, @LastName2, @DateOfBirth2, @Person2CountryOfBirth, @Person2PlaceOfBirth, @PPS_Number2, @Phone, " +
                "@Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, @OnlineDateRegistered", DateRecordAdded, RegistrationInProgress, RegistrationComplete,
                InvestorTypeEntered, InvestorSubTypeEntered, Title, FirstName, LastName, Address1, Address2, Address3, Town_City, County_ZipCode, Postcode, Address_Country, DateOfBirth,
                Person1CountryOfBirth, Person1PlaceOfBirth, PPS_Number, Title2, FirstName2, LastName2, DateOfBirth2, Person2CountryOfBirth, Person2PlaceOfBirth, PPS_Number2, Phone, Mobile,
                Onlineemail, AgentContact, AgentName, AgentAddress, NoAgent, OnlineDateSetUp, OnlineDateRegistered).FirstOrDefault();
            return investor;
        }

        public RegistrationJointBO SaveRegistrationAndSendNotification(RegistrationJointBO investor)
        {
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            RegistrationJointBO registrationJoint = this.AddJointRegistration(investor);
            try
            {
                SendVerificationEmail(registrationJoint);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return registrationJoint;
        }

        public RegistrationJointBO ContinueRegistration(string notificationCode)
        {
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            TokenBO tokenDetails = TokenService.GetTokenDetails(notificationCode, NotificationTokenType.JointRegistrationEmailVerification.Id);
            RegistrationJointBO investor = this.GetRegistration(tokenDetails.WebClientIDNumber);
            investor.Assert("Investor registration already completed or not found.", i => i != null);
            return investor;
        }

        private RegistrationJointBO UpdateJointRegistration(RegistrationJointBO investor)
        {
            ValidateJointRegistration(investor);

            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", investor.WebClientIDNumber, SqlDbType.Int);
            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", DateTime.Now, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", investor.RegistrationInProgress, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", false, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Title = CreateSqlParameter("Title", investor.Title, SqlDbType.Text);
            SqlParameter FirstName = CreateSqlParameter("FirstName", investor.FirstName, SqlDbType.Text);
            SqlParameter LastName = CreateSqlParameter("LastName", investor.LastName, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter DateOfBirth = CreateSqlParameter("DateOfBirth", investor.DateOfBirth, SqlDbType.DateTime2);
            SqlParameter Person1CountryOfBirth = CreateSqlParameter("Person1CountryOfBirth", investor.Person1CountryOfBirth, SqlDbType.Text);
            SqlParameter Person1PlaceOfBirth = CreateSqlParameter("Person1PlaceOfBirth", investor.Person1PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number = CreateSqlParameter("PPS_Number", investor.PPS_Number, SqlDbType.Text);
            SqlParameter Title2 = CreateSqlParameter("Title2", investor.Title2, SqlDbType.Text);
            SqlParameter FirstName2 = CreateSqlParameter("FirstName2", investor.FirstName2, SqlDbType.Text);
            SqlParameter LastName2 = CreateSqlParameter("LastName2", investor.LastName2, SqlDbType.Text);
            SqlParameter DateOfBirth2 = CreateSqlParameter("DateOfBirth2", investor.DateOfBirth2, SqlDbType.DateTime2);
            SqlParameter Person2CountryOfBirth = CreateSqlParameter("Person2CountryOfBirth", investor.Person2CountryOfBirth, SqlDbType.Text);
            SqlParameter Person2PlaceOfBirth = CreateSqlParameter("Person2PlaceOfBirth", investor.Person2PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number2 = CreateSqlParameter("PPS_Number2", investor.PPS_Number2, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);



            string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_Registration_Joint @WebClientIDNumber, @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, " +
                "@InvestorTypeEntered, @InvestorSubTypeEntered, @Title, @FirstName, @LastName, @Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, " +
                "@DateOfBirth, @Person1CountryOfBirth, @Person1PlaceOfBirth, @PPS_Number, @Title2, @FirstName2, @LastName2, @DateOfBirth2, @Person2CountryOfBirth, @Person2PlaceOfBirth, " +
                "@PPS_Number2, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, @OnlineDateRegistered ", WebClientIDNumber, DateRecordAdded,
                RegistrationInProgress, RegistrationComplete, InvestorTypeEntered, InvestorSubTypeEntered, Title, FirstName, LastName, Address1, Address2, Address3, Town_City, County_ZipCode,
                Postcode, Address_Country, DateOfBirth, Person1CountryOfBirth, Person1PlaceOfBirth, PPS_Number, Title2, FirstName2, LastName2, DateOfBirth2, Person2CountryOfBirth,
                Person2PlaceOfBirth, PPS_Number2, Phone, Mobile, Onlineemail, AgentContact, AgentName, AgentAddress, NoAgent, OnlineDateSetUp, OnlineDateRegistered).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
            return investor;
        }

        public void SendRegistration2FactorCode(int webClientIDNumber, string phone)
        {
            phone.Assert("Phone is required.", p => !string.IsNullOrWhiteSpace(p));
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            RegistrationJointBO investor = this.GetRegistration(webClientIDNumber);
            if (!phone.Equals(investor.Mobile))
            {
                investor.Mobile = phone;
                UpdateJointRegistration(investor);
            }
            try
            {
                DateTime now = DateTime.Now;
                TokenBO investorNotification = new TokenBO()
                {
                    WebClientIDNumber = webClientIDNumber,
                    InvestorNotificationType_Id = NotificationTokenType.CompleteRegistrationSmsVerification.Id,
                    Token_Code = TokenBO.GenerateTextMessageHash(),
                    Token_Created_Date = now,
                    Token_Expiry_Date = now.AddHours(1)
                };
                TokenService.AddToken(investorNotification);
                if (!ApplicationSettings.Instance.SendRegistration2FactorCodeByEmail)
                {
                    //send sms
                    SmsService.SendSms(phone, string.Format("Your BCP Vespro verification code is {0}. Please note: This code will expire in 1 hour.", investorNotification.Token_Code));
                    //AuditTrailService.AddAuditTrailRecord(string.Empty, "Joint registration", "Send two factor authentication code \"" + investorNotification.Token_Code + "\" by sms to: " + phone, NetworkUtils.GetRequestIpAddress(), null, null, string.Empty, NetworkUtils.GetRequestUserAgent());

                }
                else
                {
                    EmailService.SendEmail(new EmailBO()
                    {
                        From = ApplicationSettings.Instance.MailFrom,
                        To = investor.Onlineemail,
                        Subject = "Your BCP Vespro verification code",
                        Content = string.Format("Your BCP Vespro verification code is {0}. Please note: This code will expire in 1 hour.", investorNotification.Token_Code)
                    });
                    //AuditTrailService.AddAuditTrailRecord(string.Empty, "Joint registration", "Send two factor authentication code \"" + investorNotification.Token_Code + "\" by email to: " + investor.Onlineemail, NetworkUtils.GetRequestIpAddress(), null, null, string.Empty, NetworkUtils.GetRequestUserAgent());
                }
                //transaction.Commit();
            }
            catch (Exception ex)
            {
                //transaction.Rollback();
                throw ex;
            }
        }

        public void SendVerificationEmail(RegistrationJointBO investor)
        {
            DateTime now = DateTime.Now;
            TokenBO investorNotification = new TokenBO()
            {
                WebClientIDNumber = investor.WebClientIDNumber,
                InvestorNotificationType_Id = NotificationTokenType.JointRegistrationEmailVerification.Id,
                Token_Code = TokenBO.GenerateEmailHash(),
                Token_Created_Date = now,
                Token_Expiry_Date = now.AddHours(1)
            };
            TokenService.AddToken(investorNotification);
            EmailService.SendEmail(new EmailBO()
            {
                From = ApplicationSettings.Instance.MailFrom,
                To = investor.Onlineemail,
                Subject = "BCP Vespro Email Address Validation",
                Content = RegistrationConfirmationEmailContent(string.Join(" & ", string.Join(" ", investor.Title, investor.LastName), string.Join(" ", investor.Title2, investor.LastName2)), JointRegistrationConfirmationLink(investorNotification.Token_Code))
            });
        }
    }
}
