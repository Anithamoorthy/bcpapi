﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class ResultBO
    {
        [JsonIgnore]
        public byte Result { get; set; }
        [JsonIgnore]
        public string Reason { get; set; }
        [JsonIgnore]
        public string ResultCode { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
    }
}
