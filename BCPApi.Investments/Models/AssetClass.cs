﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCPApi.Investments.Models
{
    public class AssetClassList
    {
        public int StockACBID { get; set; }
        public int StockID { get; set; }
        public string AssetClass { get; set; }
        public double Allocation { get; set; }
    }
}