﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using BCPApi.Service.Security;
using Newtonsoft.Json;
using BCPApi.Database;

namespace BCPApi.UserAuth.Models
{
    public class AuthRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string AccountNumber { get; set; }
        public string PW_Hash { get; set; }
        public string IPAddress { get; set; }
    }

    public class AuthResponse
    {
        [JsonProperty("clientidnumber")]
        public int ClientIDNumber { get; set; }
        [JsonProperty("clientaccountnumber")]
        public string ClientAccountNumber { get; set; }
        [JsonProperty("firstname")]
        public string FirstName { get; set; }
        [JsonProperty("lastname")]
        public string LastName { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
    }
    public class UserAuth : IUserAuth
    {
        //protected IBaseDbContext Db { get; set; }
        protected BaseDbContext Db;
        public UserAuth()
        {
            Db = new BaseDbContext();
        }

        public IList<AuthResponse> CheckUserAuth(string username, string password)
        {
            string passwordSaltHash = GetPasswordSaltHash(username), passwordHashSha1 = GeneratePasswordHashSha1(password, passwordSaltHash), passwordHashSha256 = GeneratePasswordHashSha256(password, passwordSaltHash);
            try
            {
                SqlParameter AC_Num = CreateSqlParameter("AC_Num", username.ToUpper(), SqlDbType.Text),
                    PW_Hash = CreateSqlParameter("PW_Hash", passwordHashSha1, SqlDbType.Text);
                IList<AuthResponse> loginCheck = Db.Database.SqlQuery<AuthResponse>("exec VeApp_User_Login_Check @AC_Num, @PW_Hash ", AC_Num, PW_Hash).ToList();
                if (loginCheck != null && loginCheck[0].ClientIDNumber != 0)
                {
                    UpdatePasswordHash(username.ToUpper(), passwordHashSha256);
                }
                else
                {
                    AC_Num = CreateSqlParameter("AC_Num", username.ToUpper(), SqlDbType.Text);
                    PW_Hash = CreateSqlParameter("PW_Hash", passwordHashSha256, SqlDbType.Text);
                    loginCheck = Db.Database.SqlQuery<AuthResponse>("exec VeApp_User_Login_Check @AC_Num, @PW_Hash ", AC_Num, PW_Hash).ToList();
                }     
                return loginCheck;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdatePasswordHash(string accountNumber, string passwordHash)
        {
            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
            SqlParameter pwHash = CreateSqlParameter("PW_Hash", passwordHash, SqlDbType.Text);
            try
            {
                string response = Db.Database.SqlQuery<string>("exec CRM_Update_CRM_User_Record_PW @AC_Num, @PW_Hash ", acNum, pwHash).FirstOrDefault();
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AuthResponse> CheckUserAuth1(string username,string password)
        {
            List<AuthResponse> loginResponse = new List<AuthResponse>();
            AuthResponse loginRes = new AuthResponse();
            try
            {
                string passwordSaltHash = GetPasswordSaltHash(username), passwordHashSha1 = GeneratePasswordHashSha1(password, passwordSaltHash), passwordHashSha256 = GeneratePasswordHashSha256(password, passwordSaltHash);

                string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCPConnection"].ToString();
                using (SqlConnection ConLogin = new SqlConnection(constr))
                {
                    ConLogin.Open();
                    using (SqlCommand CmdLogin = new SqlCommand("[VeApp_User_Login_Check]", ConLogin))
                    {
                        CmdLogin.Parameters.AddWithValue("@ACNumberIn", username);
                        CmdLogin.Parameters.AddWithValue("@PWHashIn", passwordHashSha1);
                        CmdLogin.CommandType = CommandType.StoredProcedure;
                        using (var reader = CmdLogin.ExecuteReader())
                        {
                            if (reader.HasRows & reader.Read() & reader[0] != null & (Convert.ToString(reader[0]) != "" & Convert.ToString(reader[0]) != "0"))
                            {
                                loginRes = new AuthResponse();
                                loginRes.ClientIDNumber = reader[0] as int? ?? default(int);
                                loginRes.ClientAccountNumber = reader[1] as string;
                                loginRes.FirstName = reader[2] as string;
                                loginRes.LastName = reader[3] as string;
                                loginRes.City = reader[4] as string;
                                loginRes.Country= reader[5] as string;
                                loginResponse.Add(loginRes);

                                UpdatePasswordHash(username.ToUpper(), passwordHashSha256);
                            }
                            else
                            {
                                if (!reader.IsClosed) reader.Close();
                                CmdLogin.CommandText = "VeApp_User_Login_Check";
                                CmdLogin.Parameters.Clear();
                                CmdLogin.Parameters.AddWithValue("@ACNumberIn", username);
                                CmdLogin.Parameters.AddWithValue("@PWHashIn", passwordHashSha256);
                                CmdLogin.CommandType = CommandType.StoredProcedure;
                                using (var sreader = CmdLogin.ExecuteReader())
                                {
                                    if (sreader.HasRows & sreader.Read() & sreader[0] != null & (Convert.ToString(sreader[0]) != "" & Convert.ToString(sreader[0]) != "0"))
                                    {
                                        loginRes = new AuthResponse();
                                        loginRes.ClientIDNumber = sreader[0] as int? ?? default(int);
                                        loginRes.ClientAccountNumber = sreader[1] as string;
                                        loginRes.FirstName = sreader[2] as string;
                                        loginRes.LastName = sreader[3] as string;
                                        loginRes.City = sreader[4] as string;
                                        loginRes.Country=sreader[5] as string;
                                        loginResponse.Add(loginRes);
                                    }
                                }
                            }                            
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return loginResponse;
        }

        private string GetPasswordSaltHash(string accountNumber)
        {
          //  accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));

            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);

            try
            {
                string response = Db.Database.SqlQuery<string>("exec CRM_Get_User_Salt @AC_Num ", acNum).FirstOrDefault();
                if (string.IsNullOrWhiteSpace(response))
                {
                    response = GeneratePasswordSaltHash(accountNumber);
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string GeneratePasswordSaltHash(string accountNumber)
        {
            //accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));

            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
            try
            {
                string PW_Salt = Db.Database.SqlQuery<string>("exec CRM_Create_User_Salt @AC_Num ", acNum).FirstOrDefault();
                UpdatePasswordSaltHash(accountNumber, PW_Salt);
                return PW_Salt;
            }
            catch (Exception ex) { throw ex; }
        }

        public string UpdatePasswordSaltHash(string accountNumber, string passwordSaltHash)
        {
            //UserBasicVO user = GetUserBasicByAccountNumber(accountNumber);
            //accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));
            //passwordSaltHash.Assert("Password Salt is required.", psh => !string.IsNullOrWhiteSpace(psh));
            //user.Assert("User not allowed.", u => u != null && u.Result > 0 && u.WebEnabled.HasValue && u.WebEnabled.Value);

            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
            SqlParameter pwSaltHash = CreateSqlParameter("PW_Salt", passwordSaltHash, SqlDbType.Text);
            try
            {
                string response = Db.Database.SqlQuery<string>("exec CRM_Update_CRM_User_Record_Salt @AC_Num, @PW_Salt ", acNum, pwSaltHash).FirstOrDefault();
                //response.Assert(response, r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));

                return response;
            }
            catch (Exception ex) { throw ex; }
        }

        protected SqlParameter CreateSqlParameter(string parameterName, object value, SqlDbType sqlDbType)
        {
            return new SqlParameter() { ParameterName = parameterName, Value = value ?? DBNull.Value, SqlDbType = sqlDbType };
        }

        //public string UpdatePasswordHash(string accountNumber, string passwordHash)
        //{
        //    string response = "";
        //    SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
        //    SqlParameter pwHash = CreateSqlParameter("PW_Hash", passwordHash, SqlDbType.Text);

        //    string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCPConnection"].ToString();
        //    using (SqlConnection ConLogin = new SqlConnection(constr))
        //    {
        //        ConLogin.Open();
        //        using (SqlCommand CmdLogin = new SqlCommand("CRM_Update_CRM_User_Record_PW", ConLogin))
        //        {
        //            CmdLogin.Parameters.AddWithValue("@ACNumberIn", accountNumber);
        //            CmdLogin.Parameters.AddWithValue("@PWHashIn", passwordHash);
        //            CmdLogin.CommandType = CommandType.StoredProcedure;
        //            response = CmdLogin.ExecuteScalar().ToString();
        //        }

        //        return response;
        //    }
        //}

        //public string UpdatePasswordSaltHash(string accountNumber, string passwordSaltHash)
        //{
        //    string response = "";
        //    string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCPConnection"].ToString();
        //    using (SqlConnection ConLogin = new SqlConnection(constr))
        //    {
        //        ConLogin.Open();
        //        using (SqlCommand CmdLogin = new SqlCommand("CRM_Update_CRM_User_Record_Salt", ConLogin))
        //        {
        //            CmdLogin.Parameters.AddWithValue("@ACNumberIn", accountNumber);
        //            CmdLogin.Parameters.AddWithValue("@PWSaltIn", passwordSaltHash);
        //            CmdLogin.CommandType = CommandType.StoredProcedure;
        //            response = CmdLogin.ExecuteScalar().ToString();
        //        }
        //    }            
        //    return response;
        //}
        //private string GeneratePasswordSaltHash(string accountNumber)
        //{
        //    string PW_Salt = "";
        //    string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCPConnection"].ToString();
        //    using (SqlConnection ConLogin = new SqlConnection(constr))
        //    {
        //        ConLogin.Open();
        //        using (SqlCommand CmdLogin = new SqlCommand("CRM_Create_User_Salt", ConLogin))
        //        {
        //            CmdLogin.Parameters.AddWithValue("@ACNumberIn", accountNumber);
        //            CmdLogin.CommandType = CommandType.StoredProcedure;
        //            PW_Salt = CmdLogin.ExecuteScalar().ToString();
        //        }
        //    }

        //    UpdatePasswordSaltHash(accountNumber, PW_Salt);
        //    return PW_Salt;
        //}
        //private string GetPasswordSaltHash(string accountNumber)
        //{
        //    string response = "";
        //    string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCPConnection"].ToString();
        //    using (SqlConnection ConLogin = new SqlConnection(constr))
        //    {
        //        ConLogin.Open();
        //        using (SqlCommand CmdLogin = new SqlCommand("CRM_Get_User_Salt", ConLogin))
        //        {
        //            CmdLogin.Parameters.AddWithValue("@ACNumberIn", accountNumber);
        //            CmdLogin.CommandType = CommandType.StoredProcedure;
        //            response = CmdLogin.ExecuteScalar().ToString();
        //        }
        //    }

        //    if (string.IsNullOrWhiteSpace(response))
        //    {
        //        response = GeneratePasswordSaltHash(accountNumber);
        //    }

        //    return response;
        //}

        private string GeneratePasswordHashSha1(string password, string passwordSalt)
        {
            return Encrypter.EncryptSha1(passwordSalt, password);
        }

        private string GeneratePasswordHashSha256(string password, string passwordSalt)
        {
            return Encrypter.EncryptSha256(passwordSalt, password);
        }

    }
}