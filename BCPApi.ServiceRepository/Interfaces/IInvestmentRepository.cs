﻿using BCPApi.Entities.BO;
using BCPApi.Entities.BO.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IInvestmentRepository
    {
        IList<InvestmentBO> GetClientInvestments(int clientNumber, DateTime? startDate, DateTime? endDate);
        IList<InvestmentBO> GetClientInvestmentById(int clientNumber, int ClientHoldingId, DateTime? startDate, DateTime? endDate);
        string CompleteInvestorRegistration(int webClientIDNumber, string password, string twoFactorCode, short investorNotificationTypeId, string completeRegistrationToken);
        IList<RegistrationDeclarationBO> GetRegistrationDeclarations(int webClientIDNumber, short clientType, string clientSubType);
        IList<RegistrationDeclarationBO> AddRegistrationDeclarations(IList<RegistrationDeclarationBO> registrationDeclarations);
        IList<RegistrationDocumentBO> GetRegistrationDocuments(int webClientIDNumber, string clientSubType);
        IList<RegistrationDocumentBO> AddRegistrationDocuments(IList<RegistrationDocumentBO> registrationDocuments);
        IList<ClientTransactionBO> GetClientTransactions(int clientNumber, int? stockId, DateTime? startDate, DateTime? endDate);
        IList<InvestmentDocumentBO> GetInvestmentDocuments(int investmentId);
        IList<InvestmentDocumentBO> SaveInvestmentDocuments(IList<InvestmentDocumentBO> investmentDocuments, int clientId, string clientName, UserBasicBO user);
    }
}
