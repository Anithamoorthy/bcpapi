﻿using System;

namespace BCPApi.Entities.BO.Registration
{
    public class RegistrationJointBO : BaseRegistrationBO
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string DateOfBirthString { get; set; }
        public string Person1CountryOfBirth { get; set; }
        public string Person1PlaceOfBirth { get; set; }
        public string PPS_Number { get; set; }
        public string Title2 { get; set; }
        public string FirstName2 { get; set; }
        public string LastName2 { get; set; }
        public DateTime? DateOfBirth2 { get; set; }
        public string DateOfBirth2String { get; set; }
        public string Person2CountryOfBirth { get; set; }
        public string Person2PlaceOfBirth { get; set; }
        public string PPS_Number2 { get; set; }
    }
}
