﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class NonIndividualSelfDeclarationPersonBO
    {
        public int WebClientNISDPersonID { get; set; }
        public int WebClientNISDID { get; set; }
        public bool ControlByOwnership { get; set; }
        public bool ControlByOther { get; set; }
        public bool ControlByManagement { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string CountryOfBirth { get; set; }

        public IList<NonIndividualSelfDeclarationPersonTaxBO> Taxes { get; set; }
    }
}
