﻿using BCPApi.Entities.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IForgotpwdRepository
    {
        void SendResetPasswordEmail(string username);
        UserBasicBO ValidateResetPasswordToken(string token);
        string SendResetPassword2FactorCode(string token);
        void ResetPassword(string resetCode, string password, string confirmCode);
    }
}
