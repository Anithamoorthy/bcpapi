﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities
{
    public class AuthResponse
    {
        [JsonProperty("clientidnumber")]
        public int ClientIDNumber { get; set; }
        [JsonProperty("clientaccountnumber")]
        public string ClientAccountNumber { get; set; }
        [JsonProperty("firstname")]
        public string FirstName { get; set; }
        [JsonProperty("lastname")]
        public string LastName { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
    }
}
