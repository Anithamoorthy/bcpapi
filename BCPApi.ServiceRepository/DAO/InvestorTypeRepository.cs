﻿using BCPApi.Database;
using BCPApi.Entity.BO;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class InvestorTypeRepository: IInvestorTypeRepository
    {
        protected IBaseDbContext Db { get; set; }

        public InvestorTypeRepository(IBaseDbContext db)
        {
            Db = db;
        }

        public IList<InvestorTypeBO> GetByClientType(string clientType)
        {
            IList<InvestorTypeBO> investorTypes = Db.DataBase.SqlQuery<InvestorTypeBO>("exec CRM_Get_Client_Types").ToList();
            return investorTypes.Where(it => string.Equals(it.ClientType, clientType, System.StringComparison.OrdinalIgnoreCase)).OrderBy(it => it.Sort_Order).ToList();
        }
    }
}
