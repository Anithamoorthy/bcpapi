﻿using BCPApi.Database;
using BCPApi.Entities.BO;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class AssetRepository : CommonService, IAssetRepository
    {
        protected IBaseDbContext Db = new BaseDbContext();
        public AssetRepository(IBaseDbContext db)
        {
            Db = db;
        }

        public IList<AssetBO> GetAssetAddFunds()
        {
            return Db.DataBase.SqlQuery<AssetBO>("exec CRM_Get_Asset_Chart_Add_Funds").Select(a => { a.AssetClasses = GetAssetClasses(a.StockID); return a; }).ToList();
        }       
    }
}
