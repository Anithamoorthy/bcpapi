﻿using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BCPApi.Invesments.Controllers
{
    [RoutePrefix("investments")]
    public class TransactionsController : ApiController
    {
        public IInvestmentRepository investorService { get; set; }

        public TransactionsController(IInvestmentRepository investment)
        {
            investorService = investment;
        }

        [Route("get-transactions"), HttpPost]
        public IHttpActionResult GetTransactions(int Client_ID_Number, int? stockId, DateTime? startDate, DateTime? endDate)
        {
            return Ok(investorService.GetClientTransactions(Client_ID_Number, stockId, startDate, endDate));
        }
    }
}
