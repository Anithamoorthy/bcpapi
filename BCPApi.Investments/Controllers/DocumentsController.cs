﻿using BCPApi.Entities.BO;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BCPApi.Invesments.Controllers
{
    [RoutePrefix("investments")]
    public class DocumentsController : ApiController
    {
        public IInvestmentRepository investorService { get; set; }
        public IDocumentRepository DocumentService { get; set; }

        public DocumentsController(IInvestmentRepository investment, IDocumentRepository documentService)
        {
            investorService = investment;
            DocumentService = documentService;
        }

        [Route("get-investment-documents"), HttpPost]
        public IHttpActionResult GetInvestmentDocuments(int investmentId)
        {
            return  Ok(investorService.GetInvestmentDocuments(investmentId));
        }

        [Route("save-investment-documents"), HttpPost]
        public IHttpActionResult SaveInvestmentDocuments(IList<InvestmentDocumentBO> documents,int clientNumber,string clientName, UserBasicBO AuthorizedUser)
        {
            return Ok(investorService.SaveInvestmentDocuments(documents, clientNumber, clientName, AuthorizedUser));
        }

        [Route("download-document"), HttpPost]
        public IHttpActionResult DownloadDocument(InvestmentDocumentBO file)
        {
            return Ok(DocumentService.DownloadS3Document(file.Document_Link));
        }

        [Route("get-documents"), HttpPost]
        public IHttpActionResult GetDocuments(bool IsClient,bool IsMasterClient,int Client_ID_Number, string accountNumber, string docType, bool? readStatus)
        {
            if (IsClient || (IsMasterClient && Client_ID_Number != 0))
            {

            }
                return Ok(DocumentService.GetClientDocuments(Client_ID_Number, docType, readStatus));
            //else
            //    return new JsonResultView(DocumentService.GetAgentDocuments(AuthorizedUser.UserName, accountNumber, docType, readStatus));
        }

    }
}
