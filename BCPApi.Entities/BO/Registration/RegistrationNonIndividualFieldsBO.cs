﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO.Registration
{
    public class RegistrationNonIndividualFieldsBO
    {
        public string ApplicantType { get; set; }
        public string ContactTitle_RequiredText { get; set; }
        public string ContactTitle_Required { get; set; }
        public string ContactTitle_Label { get; set; }
        public string ContactFirstName_RequiredText { get; set; }
        public string ContactFirstName_Required { get; set; }
        public string ContactFirstName_Label { get; set; }
        public string ContactLastName_RequiredText { get; set; }
        public string ContactLastName_Required { get; set; }
        public string ContactLastName_Label { get; set; }
        public string ContactTitle2_RequiredText { get; set; }
        public string ContactTitle2_Required { get; set; }
        public string ContactTitle2_Label { get; set; }
        public string ContactFirstName2_RequiredText { get; set; }
        public string ContactFirstName2_Required { get; set; }
        public string ContactFirstName2_Label { get; set; }
        public string ContactLastName2_RequiredText { get; set; }
        public string ContactLastName2_Required { get; set; }
        public string ContactLastName2_Label { get; set; }
        public string Company_Name_RequiredText { get; set; }
        public string Company_Name_Required { get; set; }
        public string Company_Name_Label { get; set; }
        public string Address1_RequiredText { get; set; }
        public string Address1_Required { get; set; }
        public string Address1_Label { get; set; }
        public string Address2_RequiredText { get; set; }
        public string Address2_Required { get; set; }
        public string Address2_Label { get; set; }
        public string Address3_RequiredText { get; set; }
        public string Address3_Required { get; set; }
        public string Address3_Label { get; set; }
        public string Town_City_RequiredText { get; set; }
        public string Town_City_Required { get; set; }
        public string Town_City_Label { get; set; }
        public string County_ZipCode_RequiredText { get; set; }
        public string County_ZipCode_Required { get; set; }
        public string County_ZipCode_Label { get; set; }
        public string Postcode_RequiredText { get; set; }
        public string Postcode_Required { get; set; }
        public string Postcode_Label { get; set; }
        public string Address_Country_RequiredText { get; set; }
        public string Address_Country_Required { get; set; }
        public string Address_Country_Label { get; set; }
        public string Country_of_Incorporation_RequiredText { get; set; }
        public string Country_of_Incorporation_Required { get; set; }
        public string Country_of_Incorporation_Label { get; set; }
        public string Corporate_Tax_ID_RequiredText { get; set; }
        public string Corporate_Tax_ID_Required { get; set; }
        public string Corporate_Tax_ID_Label { get; set; }
        public string Phone_RequiredText { get; set; }
        public string Phone_Required { get; set; }
        public string Phone_Label { get; set; }
        public string Mobile_RequiredText { get; set; }
        public string Mobile_Required { get; set; }
        public string Mobile_Label { get; set; }
        public string Onlineemail_RequiredText { get; set; }
        public string Onlineemail_Required { get; set; }
        public string Onlineemail_Label { get; set; }
        public string AgentContact_RequiredText { get; set; }
        public string AgentContact_Required { get; set; }
        public string AgentContact_Label { get; set; }
        public string AgentName_RequiredText { get; set; }
        public string AgentName_Required { get; set; }
        public string AgentName_Label { get; set; }
        public string AgentAddress_RequiredText { get; set; }
        public string AgentAddress_Required { get; set; }
        public string AgentAddress_Label { get; set; }
        public string AgentNo_RequiredText { get; set; }
        public string AgentNo_Required { get; set; }
        public string AgentNo_Label { get; set; }
        public string Tax_Exempt_RequiredText { get; set; }
        public string Tax_Exempt_Required { get; set; }
        public string Tax_Exempt_Label { get; set; }
        public string BankName_RequiredText { get; set; }
        public string BankName_Required { get; set; }
        public string BankName_Label { get; set; }
        public string BankAddress_RequiredText { get; set; }
        public string BankAddress_Required { get; set; }
        public string BankAddress_Label { get; set; }
        public string BankAccountName_RequiredText { get; set; }
        public string BankAccountName_Required { get; set; }
        public string BankAccountName_Label { get; set; }
        public string BankBIC_RequiredText { get; set; }
        public string BankBIC_Required { get; set; }
        public string BankBIC_Label { get; set; }
        public string BankIBAN_RequiredText { get; set; }
        public string BankIBAN_Required { get; set; }
        public string BankIBAN_Label { get; set; }
    }
}
