﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.Data
{
    public class ApplicationSettings
    {
        static readonly object padlock = new object();
        private static ApplicationSettings instance;
        public static ApplicationSettings Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ApplicationSettings();
                        instance.LoadSettings();
                    }
                    return instance;
                }
            }
        }

        static ApplicationSettings()
        {
            ApplicationSettings.Instance.ToString();
        }

        private ApplicationSettings() { }

        private void LoadSettings()
        {
            List<string> keys = new List<string>(ConfigurationManager.AppSettings.AllKeys);
            PropertyInfo[] properties = typeof(ApplicationSettings).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            object currentValue = null;
            for (int idx = 0, len = properties.Length; idx < len; idx++)
            {
                if (keys.Contains(properties[idx].Name))
                    currentValue = ConfigurationManager.AppSettings[properties[idx].Name];
                {
                    currentValue = Convert.ChangeType(currentValue, properties[idx].PropertyType);
                    properties[idx].SetValue(instance, currentValue);
                }
            }
        }

        public string WebsiteBaseUrl { get; set; }

        public string ApplicationName { get; set; }
        public bool LoggingEnabled { get; set; }
        public string LoggingSourceName { get; set; }
        public string LoggingTargetLogName { get; set; }
        public bool LoggingFileLogEnabled { get; set; }
        public string LoggingSubject { get; set; }
        public string LoggingPath { get; set; }
        public string PrivateKeyWordEncrypter { get; set; }
        public string UploadDirectory { get; set; }
        public string FileMappedLocalDirectory { get; set; }
        public bool SendLoginCode { get; set; }

        /* Related to Email Service */
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }

        public string TestEmailTo { get; set; }
        public string MailFrom { get; set; }
        public string MailBcc { get; set; }
        public bool SendRegistration2FactorCodeByEmail { get; set; }
        public bool SendResetPassword2FactorCodeByEmail { get; set; }
        public bool SendLogin2FactorCodeByEmail { get; set; }
        /* Related to Email Service */

        /* Related to SMS Service */
        public string SmsAccountReference { get; set; }
        public string SmsUsername { get; set; }
        public string SmsPassword { get; set; }
        public string SmsFrom { get; set; }
        public string TestSmsTo { get; set; }
        /* Related to SMS Service */

        public string AppReference { get; set; }

        /* Related to AWS S3 */
        public string AWSBucketName { get; set; }
        /* Related to AWS S3 */
    }
}
