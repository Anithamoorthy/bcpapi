﻿using BCPApi.Database;
using BCPApi.Entities.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public partial class CommonService
    {
        protected IBaseDbContext _Db = new BaseDbContext();
        
        protected SqlParameter CreateSqlParameter(string parameterName, object value, SqlDbType sqlDbType)
        {
            return new SqlParameter() { ParameterName = parameterName, Value = value ?? DBNull.Value, SqlDbType = sqlDbType };
        }

        public IList<AssetClassBO> GetAssetClasses(int stockId)
        {
            SqlParameter Stock_ID = CreateSqlParameter("Stock_ID", stockId, SqlDbType.Int);
            return _Db.DataBase.SqlQuery<AssetClassBO>("exec CRM_Get_Stock_AssetClasses @Stock_ID", Stock_ID).ToList();
        }
    }
}
