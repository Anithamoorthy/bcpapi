﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.Domain
{
    public class InvestorRegistrationType
    {
        public short Id { get; private set; }
        public string ClientType { get; private set; }
        public string InvestorType { get; private set; }

        public static readonly InvestorRegistrationType Individual = new InvestorRegistrationType() { Id = 1, ClientType = "Individual", InvestorType = "Personal Investor" };
        public static readonly InvestorRegistrationType Joint = new InvestorRegistrationType() { Id = 2, ClientType = "Joint Individual", InvestorType = "Joint Investor" };
        public static readonly InvestorRegistrationType NonIndividual = new InvestorRegistrationType() { Id = 3, ClientType = "Non Individual", InvestorType = "Non Individual" };
        public static readonly InvestorRegistrationType Registered = new InvestorRegistrationType() { Id = 4, ClientType = "Registered", InvestorType = "Registered" };
        public static readonly InvestorRegistrationType ResetPassword = new InvestorRegistrationType() { Id = 5, ClientType = "Reset Password", InvestorType = "Reset Password" };


        public static readonly IList<InvestorRegistrationType> List = new InvestorRegistrationType[] { Individual, Joint, NonIndividual, Registered, ResetPassword }.ToArray<InvestorRegistrationType>();

        public static InvestorRegistrationType Get(short id)
        {
            for (int idx = 0, len = InvestorRegistrationType.List.Count; idx < len; idx++)
            {
                if (InvestorRegistrationType.List[idx].Id == id)
                {
                    return InvestorRegistrationType.List[idx];
                }
            }
            return null;
        }
        public static InvestorRegistrationType GetByClientType(string clientType)
        {
            for (int idx = 0, len = InvestorRegistrationType.List.Count; idx < len; idx++)
            {
                if (InvestorRegistrationType.List[idx].ClientType.Equals(clientType))
                {
                    return InvestorRegistrationType.List[idx];
                }
            }
            return null;
        }
        public static InvestorRegistrationType GetByInvestorType(string investorType)
        {
            for (int idx = 0, len = InvestorRegistrationType.List.Count; idx < len; idx++)
            {
                if (InvestorRegistrationType.List[idx].InvestorType.Equals(investorType))
                {
                    return InvestorRegistrationType.List[idx];
                }
            }
            return null;
        }
    }
}
