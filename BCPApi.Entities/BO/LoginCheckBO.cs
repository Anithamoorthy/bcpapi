﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class LoginCheckBO
    {
        public bool Client { get; set; }
        public bool Agent { get; set; }
        public bool BCPUser { get; set; }
        public bool BCPMaster { get; set; }
        public bool AgentBranch { get; set; }
        public bool AgentAll { get; set; }
        public bool AgentFinancial { get; set; }
        public bool Pensioneer { get; set; }
        public bool ClientMaster { get; set; }
    }
}
