﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Database
{
    public interface IBaseDbContext : IDisposable, IObjectContextAdapter
    {
        System.Data.Entity.Database DataBase { get; }

        DbEntityEntry<T> Entry<T>(T entity) where T : class;
    }
}
