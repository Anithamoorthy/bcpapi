﻿using BCPApi.Entities.Domain;

namespace BCPApi.Entities.BO.Registration
{
    public class RegistrationNonIndividualBO : BaseRegistrationBO
    {
        public string Company_Name { get; set; }
        public short InvestorTypeGroup_Id { get; set; }
        public NonIndividualRegistrationTypeGroup InvestorTypeGroup { get { return NonIndividualRegistrationTypeGroup.GetById(InvestorTypeGroup_Id); } }
        public string Company_Name_Label { get; set; }
        public string ContactTitle { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactTitle2 { get; set; }
        public string ContactFirstName2 { get; set; }
        public string ContactLastName2 { get; set; }
    }
}
