﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.UserAuth.Models
{
    public interface IUserAuth
    {
        IList<AuthResponse> CheckUserAuth(string username, string password);
    }
}
