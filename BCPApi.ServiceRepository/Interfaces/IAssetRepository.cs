﻿using BCPApi.Entities.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IAssetRepository
    {
        IList<AssetBO> GetAssetAddFunds();
    }
}
