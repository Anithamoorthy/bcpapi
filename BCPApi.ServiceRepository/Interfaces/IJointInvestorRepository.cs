﻿using BCPApi.Entities.BO.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IJointInvestorRepository
    {
        RegistrationJointFieldsBO GetRegistrationFields();
        RegistrationJointBO GetRegistration(int webClientIDNumber);
        RegistrationJointBO SaveRegistrationAndSendNotification(RegistrationJointBO individual);
        RegistrationJointBO ContinueRegistration(string notificationCode);
        void SendRegistration2FactorCode(int webClientIDNumber, string phone);
        void SendVerificationEmail(RegistrationJointBO investor);
    }
}
