﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class ClientContactPreferenceBO
    {
        public int ClientIDNumber { get; set; }
        public string ClientAccountNumber { get; set; }
        public bool SendVesproCodesToEmail { get; set; }
    }
}
