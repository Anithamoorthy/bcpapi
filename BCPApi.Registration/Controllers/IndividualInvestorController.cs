﻿using BCPApi.Entities.BO.Registration;
using BCPApi.Entities.Domain;
using BCPApi.Entity.BO;
using BCPApi.ServiceRepository.DAO;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace BCPApi.Registration.Controllers
{
    [RoutePrefix("individual")]
    public class IndividualInvestorController : ApiController
    {
        [Dependency]
        public IInvestmentRepository InvestorService { get; set; }
        [Dependency]
        public IIndividualInvestorRepository IndividualInvestorService { get; set; }
        [Dependency]
        public IInvestorTypeRepository InvestorTypeService { get; set; }

        [Route("get-client-type"), HttpPost]
        // POST: api/IndividualInvestor
        public IHttpActionResult GetByClientType()
        {
            return Ok(InvestorTypeService.GetByClientType(InvestorRegistrationType.Individual.ClientType));
        }

        [Route("save-registration"), HttpPost]
        public IHttpActionResult SaveRegistration(RegistrationIndividualBO investor)
        {
            investor.RegistrationInProgress = true;
            investor.RegistrationComplete = false;
            return Ok(IndividualInvestorService.SaveRegistrationAndSendNotification(investor).WebClientIDNumber);
        }

        [Route("continue-registration"), HttpPost]
        public IHttpActionResult ContinueRegistration(string notificationCode)
        {
            RegistrationIndividualBO investor = IndividualInvestorService.ContinueRegistration(notificationCode);
            investor.ContinueRegistrationToken = notificationCode;            
            return Ok(investor);
        }

        [Route("send-registration-2-factor-code"), HttpPost]
        public IHttpActionResult SendRegistration2FactorCode(int WebClientIDNumber, string phone)
        {
            IndividualInvestorService.SendRegistration2FactorCode(WebClientIDNumber, phone);
            return Ok("The two factor authentication code has been sent successfully.");
        }

        [Route("complete-registration"), HttpPost]
        public IHttpActionResult CompleteRegistration(int WebClientIDNumber, string password, string twoFactorCode, string ContinueRegistrationToken)
        {
            return Ok(string.Join("Client Account Number ",InvestorService.CompleteInvestorRegistration(WebClientIDNumber, password, twoFactorCode, NotificationTokenType.IndividualRegistrationEmailVerification.Id, ContinueRegistrationToken)));
        }

        [Route("send-verification-email"), HttpPost]
        public IHttpActionResult SendVerificationEmail(RegistrationIndividualBO investor)
        {
            IndividualInvestorService.SendVerificationEmail(investor);
            return Ok(new { Status = true, Message = "The verification email has been sent successfully." });
        }

        [Route("get-registration-declarations"), HttpPost]
        public IHttpActionResult GetRegistrationDeclarations(int WebClientIDNumber,string InvestorSubTypeEntered)
        {
            return Ok(InvestorService.GetRegistrationDeclarations(WebClientIDNumber, InvestorRegistrationType.Individual.Id, InvestorSubTypeEntered));
        }

        [Route("add-registration-declarations"), HttpPost]
        public IHttpActionResult AddRegistrationDeclarations(List<RegistrationDeclarationBO> registrationDeclarations)
        {
            registrationDeclarations.ForEach(d => d.WebClientIDNumber = registrationDeclarations[0].WebClientIDNumber);
            return Ok(InvestorService.AddRegistrationDeclarations(registrationDeclarations));
        }

        [Route("get-registration-documents"), HttpPost]
        public IHttpActionResult GetRegistrationDocuments(int WebClientIDNumber,string InvestorSubTypeEntered)
        {
            return Ok(InvestorService.GetRegistrationDocuments(WebClientIDNumber, InvestorSubTypeEntered));
        }

        [Route("add-registration-documents"), HttpPost]
        public IHttpActionResult AddRegistrationDocuments(List<RegistrationDocumentBO> registrationDocuments)
        {
            registrationDocuments.ForEach(d => d.WebClientIDNumber = registrationDocuments[0].WebClientIDNumber);
            return Ok(InvestorService.AddRegistrationDocuments(registrationDocuments));
        }

    }
}
