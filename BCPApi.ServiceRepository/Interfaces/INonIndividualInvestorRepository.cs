﻿using BCPApi.Entities.BO;
using BCPApi.Entities.BO.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface INonIndividualInvestorRepository
    {
        RegistrationNonIndividualFieldsBO GetRegistrationFields();
        RegistrationNonIndividualBO GetRegistration(int webClientIDNumber);
        RegistrationNonIndividualBO SaveRegistrationAndSendNotification(RegistrationNonIndividualBO individual);
        RegistrationNonIndividualBO ContinueRegistration(string notificationCode);
        void SendRegistration2FactorCode(int webClientIDNumber, string phone);
        void SendVerificationEmail(RegistrationNonIndividualBO investor);
        NonIndividualSelfDeclarationBO GetLastSelfDeclarationByClient(int webClientIDNumber);
        NonIndividualSelfDeclarationBO SaveSelfDeclaration(NonIndividualSelfDeclarationBO selfDeclaration, string username);
    }
}
