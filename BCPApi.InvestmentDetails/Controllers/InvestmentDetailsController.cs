﻿using BCPApi.Entities.BO;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BCPApi.InvestmentDetails.Controllers
{
    [RoutePrefix("investmentdetails")]
    public class InvestmentDetailsController : ApiController
    {        
        public IInvestmentRepository investorService { get; set; }

        public InvestmentDetailsController(IInvestmentRepository investment)
        {
            investorService = investment;
        }

        [Route("get-investment-by-id"), HttpPost]
        public IHttpActionResult GetClientInvestmentById([FromBody]InvestmentDetailsBO client)
        {
            if (client == null || !ModelState.IsValid)
            {
                return BadRequest("Invalid!");
            }

            return Ok(investorService.GetClientInvestmentById(client.Client_ID_Number, client.Holding_ID, null, null));
        }
    }
}
