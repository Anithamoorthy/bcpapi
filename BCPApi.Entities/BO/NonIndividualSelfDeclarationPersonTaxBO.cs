﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class NonIndividualSelfDeclarationPersonTaxBO
    {
        public int WebClientSomething { get; set; }
        public int WebClientNISDPersonID { get; set; }
        public string TaxCountry { get; set; }
        public string TIN { get; set; }
        public string ReasonForNoTIN { get; set; }
    }
}
