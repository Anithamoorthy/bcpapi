﻿using BCPApi.Entities.BO;
using BCPApi.Entities.Data;
using BCPApi.Entities.Exceptions;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class EmailServiceRepository : IEmailServiceRepository
    {
        public void SendEmail(string from, string to, string cc, string bcc, string subject, string content, IList<AttachmentBO> attachments)
        {
            SmtpClient client = new SmtpClient(ApplicationSettings.Instance.SmtpHost, ApplicationSettings.Instance.SmtpPort);

            client.Credentials = new NetworkCredential(ApplicationSettings.Instance.SmtpUsername, ApplicationSettings.Instance.SmtpPassword);

            from.Assert("Sender is required.", f => !string.IsNullOrWhiteSpace(f));
            to.Assert("Recipient is required.", t => !string.IsNullOrWhiteSpace(t));
            subject.Assert("Subject is required.", s => !string.IsNullOrWhiteSpace(s));
            content.Assert("Content is required.", c => !string.IsNullOrWhiteSpace(c));

            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.From = new MailAddress(from);
            if (!string.IsNullOrWhiteSpace(ApplicationSettings.Instance.TestEmailTo))
            {
                message.To.Add(ApplicationSettings.Instance.TestEmailTo);
            }
            else
            {
                message.To.Add(to);
            }

            if (!string.IsNullOrWhiteSpace(cc))
            {
                message.CC.Add(cc);
            }
            if (!string.IsNullOrWhiteSpace(bcc))
            {
                message.Bcc.Add(bcc);
            }
            if (attachments != null)
            {
                foreach (AttachmentBO attachment in attachments)
                {
                    message.Attachments.Add(new Attachment(new MemoryStream(attachment.FileData), attachment.FileName));
                }
            }
            message.Subject = subject;
            message.Body = GetContentWithTemplate(content);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            client.Send(message);

            client.Dispose();
            message.Dispose();
        }

        public void SendEmail(EmailBO email)
        {
            this.SendEmail(email.From, email.To, email.Cc, email.Bcc, email.Subject, email.Content, email.Attachments);
        }

        private string GetContentWithTemplate(string content)
        {
            StringBuilder bodyContent = new StringBuilder();

            bodyContent.AppendLine("<html lang=\"en-IE\">");
            bodyContent.AppendLine("<body>");
            bodyContent.AppendLine("    <div align=\"center\">");
            bodyContent.AppendLine("        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100.0%;background:#123659;border-collapse:collapse\">");
            bodyContent.AppendLine("            <tbody>");
            bodyContent.AppendLine("                <tr>");
            bodyContent.AppendLine("                    <td width=\"100%\" valign=\"top\" style=\"width:100.0%;padding:7.5pt 0cm 15.0pt 0cm\">");
            bodyContent.AppendLine("                        <div align=\"center\">");
            bodyContent.AppendLine("                            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"0\" style=\"width:750.0pt;background:white;border-collapse:collapse\">");
            bodyContent.AppendLine("                                <tbody>");
            bodyContent.AppendLine("                                    <tr style=\"height:300.0pt\">");
            bodyContent.AppendLine("                                        <td valign=\"top\" style=\"padding:0cm 0cm 0cm 0cm;height:300.0pt\">");
            bodyContent.AppendLine("                                            <div align=\"center\">");
            bodyContent.AppendLine("                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100.0%;background:white;border-collapse:collapse\">");
            bodyContent.AppendLine("                                                    <tbody>");
            bodyContent.AppendLine("                                                        <tr>");
            bodyContent.AppendLine("                                                            <td valign=\"top\" style=\"padding:0cm 0cm 0cm 0cm\">");
            bodyContent.AppendLine("                                                                <div align=\"center\">");
            bodyContent.AppendLine("                                                                    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"0\" style=\"width:750.0pt;border-collapse:collapse\">");
            bodyContent.AppendLine("                                                                        <tbody>");
            bodyContent.AppendLine("                                                                            <tr>");
            bodyContent.AppendLine("                                                                                <td width=\"1000\" valign=\"top\" style=\"width:750.0pt;padding:0cm 0cm 0cm 0cm\">");
            bodyContent.AppendLine("                                                                                    <div align=\"center\">");
            bodyContent.AppendLine("                                                                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100.0%;border-collapse:collapse\">");
            bodyContent.AppendLine("                                                                                            <tbody>");
            bodyContent.AppendLine("                                                                                                <tr>");
            bodyContent.AppendLine("                                                                                                    <td valign=\"top\" style=\"padding:15.0pt 15.0pt 15.0pt 15.0pt\">");
            bodyContent.AppendLine("                                                                                                        <div align=\"center\">");
            bodyContent.AppendLine("                                                                                                            <div border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100.0%;border-collapse:collapse\">");
            bodyContent.AppendLine("                                                                                                                <div style=\"margin-top:7.5pt;margin-bottom:15.0pt\">");
            bodyContent.AppendLine("                                                                                                                    <p align=\"center\" style=\"text-align:center;line-height:125%\"><b><span style=\"font-size:30.0pt;line-height:125%;font-family:'Helvetica',sans-serif;color:black\"><img border=\"0\" width=\"275\" height=\"100\" style=\"width:2.868in;height:1.0416in\" id=\"m_-8997065955534815748m_-4026310184249126929_x0000_i1026\" src=\"" + ApplicationSettings.Instance.WebsiteBaseUrl + "/content/template/email/imgs/logo.png\" alt=\"BCP\" tabindex=\"0\"></span></b></p><div dir=\"ltr\" style=\"opacity: 0.01; left: 745.844px; top: 4694.95px;\"><div id=\":10t\" role=\"button\" tabindex=\"0\" aria-label=\"Download attachment \" data-tooltip=\"Download\"></div></div>");
            bodyContent.AppendLine("                                                                                                                </div>");
            bodyContent.AppendLine("                                                                                                                <div>");
            bodyContent.AppendLine("                                                                                                                    <span>");
            bodyContent.AppendLine(content);
            bodyContent.AppendLine("                                                                                                                    </span>");
            bodyContent.AppendLine("                                                                                                                </div>");
            bodyContent.AppendLine("                                                                                                                <div style=\"margin-top:7.5pt;margin-bottom:15.0pt\">");
            bodyContent.AppendLine("                                                                                                                    <p align=\"center\" style=\"text-align:center\"><b><span style=\"font-size:30.0pt;font-family:'Helvetica',sans-serif;color:black\"><img border=\"0\" id=\"m_-8997065955534815748m_-4026310184249126929_x0000_i1026\" src=\"" + ApplicationSettings.Instance.WebsiteBaseUrl + "/content/template/email/imgs/footer.png\" alt=\"BCP\" tabindex=\"0\"></span></b></p><div dir=\"ltr\" style=\"opacity: 0.01; left: 745.844px; top: 4694.95px;\"><div id=\":10t\" role=\"button\" tabindex=\"0\" aria-label=\"Download attachment \" data-tooltip=\"Download\"></div></div>");
            bodyContent.AppendLine("                                                                                                                </div>");
            bodyContent.AppendLine("                                                                                                            </div>");
            bodyContent.AppendLine("                                                                                                        </div>");
            bodyContent.AppendLine("                                                                                                    </td>");
            bodyContent.AppendLine("                                                                                                </tr>");
            bodyContent.AppendLine("                                                                                            </tbody>");
            bodyContent.AppendLine("                                                                                        </table>");
            bodyContent.AppendLine("                                                                                    </div>");
            bodyContent.AppendLine("                                                                                </td>");
            bodyContent.AppendLine("                                                                            </tr>");
            bodyContent.AppendLine("                                                                        </tbody>");
            bodyContent.AppendLine("                                                                    </table>");
            bodyContent.AppendLine("                                                                </div>");
            bodyContent.AppendLine("                                                            </td>");
            bodyContent.AppendLine("                                                        </tr>");
            bodyContent.AppendLine("                                                    </tbody>");
            bodyContent.AppendLine("                                                </table>");
            bodyContent.AppendLine("                                            </div>");
            bodyContent.AppendLine("                                        </td>");
            bodyContent.AppendLine("                                    </tr>");
            bodyContent.AppendLine("                                </tbody>");
            bodyContent.AppendLine("                            </table>");
            bodyContent.AppendLine("                        </div>");
            bodyContent.AppendLine("                    </td>");
            bodyContent.AppendLine("                </tr>");
            bodyContent.AppendLine("            </tbody>");
            bodyContent.AppendLine("        </table>");
            bodyContent.AppendLine("    </div>");
            bodyContent.AppendLine("</body>");
            bodyContent.AppendLine("</html>");

            return bodyContent.ToString();
        }
    }
}
