﻿using Newtonsoft.Json;
using System;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO
{
    public class BaseRegistrationBO
    {
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public int WebClientIDNumber { get; set; }
        public DateTime? DateRecordAdded { get; set; }
        [JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public bool RegistrationInProgress { get; set; }
        [JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public bool RegistrationComplete { get; set; }
        public string InvestorTypeEntered { get; set; }
        public string InvestorSubTypeEntered { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town_City { get; set; }
        public string County_ZipCode { get; set; }
        public string Postcode { get; set; }
        public string Address_Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Onlineemail { get; set; }
        public string AgentContact { get; set; }
        public string AgentName { get; set; }
        public string AgentAddress { get; set; }
        public bool NoAgent { get; set; }
        public DateTime? OnlineDateSetUp { get; set; }
        public DateTime? OnlineDateRegistered { get; set; }

        public string AgentFirstName { get; set; }
        public string AgentLastName { get; set; }
        public string AgentAddress1 { get; set; }
        public string AgentAddress2 { get; set; }
        public string AgentCity { get; set; }
        public string AgentCounty { get; set; }
        public string AgentPostcode { get; set; }
        [JsonIgnore]
        public string ContinueRegistrationToken { get; set; }

        public bool IsARF { get { return (InvestorSubTypeEntered ?? string.Empty).Contains("ARF"); } }
        public bool IsAMRF { get { return (InvestorSubTypeEntered ?? string.Empty).Contains("AMRF"); } }
        public bool IsPRB { get { return (InvestorSubTypeEntered ?? string.Empty).Contains("PRB"); } }
        public bool IsSSAP { get { return (InvestorSubTypeEntered ?? string.Empty).Contains("SSAP"); } }
        public bool IsCU { get { return (InvestorSubTypeEntered ?? string.Empty).Contains("Credit Union"); } }
    }
}
