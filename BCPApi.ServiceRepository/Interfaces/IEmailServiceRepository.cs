﻿using BCPApi.Entities.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IEmailServiceRepository
    {
        void SendEmail(string from, string to, string cc, string bcc, string subject, string content, IList<AttachmentBO> attachments);

        void SendEmail(EmailBO email);        
    }
}
