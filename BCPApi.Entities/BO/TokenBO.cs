﻿using BCPApi.Entities.Domain;
using BCPApi.ServiceRepository.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class TokenBO
    {

        private const string ADDITIONAL_HASH = "hA$HC0d3_12Ba7d151a4R082o788469EfaE6f5f05_0223";

        [JsonIgnore]
        public int Token_ID { get; set; }
        public string Token_Code { get; set; }
        public string AC_Num { get; set; }
        public int WebClientIDNumber { get; set; }
        public short InvestorNotificationType_Id { get; set; }
        public NotificationTokenType InvestorNotificationType
        {
            get
            {
                return NotificationTokenType.Get(InvestorNotificationType_Id);
            }
        }
        public DateTime? Token_Used_Date { get; set; }
        public DateTime? Token_Expiry_Date { get; set; }
        public DateTime? Token_Created_Date { get; set; }

        public static string GenerateEmailHash()
        {
            return Encrypter.EncryptSha256(ADDITIONAL_HASH, DateTime.Now.ToString());
        }

        public static string GenerateTextMessageHash()
        {
            Random r = new Random();
            return string.Join(string.Empty, new int[] { r.Next(0, 10), r.Next(0, 10), r.Next(0, 10), r.Next(0, 10), r.Next(0, 10), r.Next(0, 10) });
        }

    }
}
