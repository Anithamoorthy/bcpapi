﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.Domain
{
    public class NotificationTokenType
    {
        public short Id { get; private set; }
        public string Name { get; private set; }

        public static readonly NotificationTokenType IndividualRegistrationEmailVerification = new NotificationTokenType() { Id = 1, Name = "Individual Email Verification" };
        public static readonly NotificationTokenType JointRegistrationEmailVerification = new NotificationTokenType() { Id = 2, Name = "Joint Email Verification" };
        public static readonly NotificationTokenType NonIndividualRegistrationEmailVerification = new NotificationTokenType() { Id = 3, Name = "Non Individual Email Verification" };
        public static readonly NotificationTokenType OnlineRegistrationEmailVerification = new NotificationTokenType() { Id = 4, Name = "Online Email Verification" };
        public static readonly NotificationTokenType CompleteRegistrationSmsVerification = new NotificationTokenType() { Id = 5, Name = "Complete Registration Sms Verification" };
        public static readonly NotificationTokenType CompleteOnlineRegistrationSmsVerification = new NotificationTokenType() { Id = 6, Name = "Complete Online Registration Sms Verification" };
        public static readonly NotificationTokenType InvestorLoginSms = new NotificationTokenType() { Id = 7, Name = "Investor Login Sms" };
        public static readonly NotificationTokenType InvestorForgotPasswordEmail = new NotificationTokenType() { Id = 8, Name = "Investor Forgot Password Email" };
        public static readonly NotificationTokenType InvestorForgotPasswordSms = new NotificationTokenType() { Id = 9, Name = "Investor Forgot Password Sms" };
        public static readonly NotificationTokenType InvestorEmailVerification = new NotificationTokenType() { Id = 10, Name = "Investor Email Verification" };
        public static readonly NotificationTokenType MobileEditVerification = new NotificationTokenType() { Id = 11, Name = "Mobile Edit Verification" };
        public static readonly NotificationTokenType EmailEditVerification = new NotificationTokenType() { Id = 12, Name = "Email Edit Verification" };

        public static readonly IList<NotificationTokenType> List = new NotificationTokenType[] { IndividualRegistrationEmailVerification, JointRegistrationEmailVerification,
            NonIndividualRegistrationEmailVerification, OnlineRegistrationEmailVerification, CompleteRegistrationSmsVerification, CompleteOnlineRegistrationSmsVerification, InvestorLoginSms,
            InvestorForgotPasswordEmail, InvestorForgotPasswordSms, InvestorEmailVerification, MobileEditVerification, EmailEditVerification }.ToArray<NotificationTokenType>();

        public static NotificationTokenType Get(short id)
        {
            for (int idx = 0, len = NotificationTokenType.List.Count; idx < len; idx++)
            {
                if (NotificationTokenType.List[idx].Id == id)
                {
                    return NotificationTokenType.List[idx];
                }
            }
            return null;
        }
    }
}
