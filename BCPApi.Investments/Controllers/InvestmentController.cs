﻿using BCPApi.ServiceRepository.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BCPApi.Investments.Controllers
{
    [RoutePrefix("investments")]
    public class InvestmentController : ApiController
    {
        public IInvestmentRepository investorService { get; set; }

        public InvestmentController(IInvestmentRepository investment)
        {
            investorService = investment;
        }

        [Route("get-investments"), HttpPost]
        public IHttpActionResult GetClientInvestments(int clientNumber)
        {
            if (clientNumber == 0|| !ModelState.IsValid)
            {
                return BadRequest("Invalid!");
            }

            //BCPApi.Investments.Models.Investment InvestorService = new Models.Investment();

            //System.Collections.Generic.List<Models.InvestmentBO> getinvestment = investorService.GetClientInvestments(clientNumber);

            return Ok(investorService.GetClientInvestments(clientNumber, null, null));
        }
    }
}
