﻿using BCPApi.Entities.Data;
using Newtonsoft.Json;
using System;

namespace BCPApi.Entities.BO
{
    public class DocumentBO
    {
        public int ID_Number { get; set; }
        [JsonIgnore]
        public int? Client_ID_Number { get; set; }
        public string AgentCode { get; set; }
        public string ClientAccountNumber { get; set; }
        public DateTime ItemDate { get; set; }
        public string DocumentTypeLong { get; set; }
        public string Details { get; set; }
        public string PDFDocumentName { get; set; }
        public string Uploadpath { get; set; }
        public bool Superseded { get; set; }
        public int? ID_NumberSupersededBy { get; set; }
        public string OriginalPDFDocumentName { get; set; }
        public string WebDocPath { get; set; }
        public string FullDocPath { get; set; }
        public string UploadedBy { get; set; }
        public string ClientName { get; set; }
        public bool? ReadByClient { get; set; }
        public DateTime? DateReadByClient { get; set; }
        public int? ReadByAdvisor { get; set; }
        public DateTime? DateReadByAdvisor { get; set; }

        public static DocumentBO CreateNew(int? clientId, string docName, string username, string docType, string details, string clientName, string agentCode = null)
        {
            string uploadPath = (clientId.HasValue ? clientId.Value.ToString() : agentCode),
                filePath = string.Format("/{0}/{1}", uploadPath, docName),
                localFullPath = string.Format("{0}#{1}{2}#", docName, ApplicationSettings.Instance.FileMappedLocalDirectory, (filePath ?? "/").Replace("/", "\\")).Replace("\\\\", "\\");
            return new DocumentBO()
            {
                Client_ID_Number = clientId,
                AgentCode = agentCode,
                ItemDate = DateTime.Now,
                DocumentTypeLong = docType,
                Details = details,
                PDFDocumentName = docName,
                Uploadpath = uploadPath,
                FullDocPath = localFullPath,
                WebDocPath = filePath,
                ID_NumberSupersededBy = 0,
                OriginalPDFDocumentName = docName,
                UploadedBy = username,
                ClientName = clientName
            };
        }
    }
}
