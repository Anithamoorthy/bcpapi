﻿using BCPApi.Entities.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entity.BO
{
    public class InvestorTypeBO
    {
        public int WebClientTypeID { get; set; }
        public string InvestorTypeEntered { get; set; }
        public string InvestorTypeEnteredDescription { get; set; }
        public string InvestorSubTypeEntered { get; set; }
        public string ClientType { get; set; }
        public string ClientSubType { get; set; }
        public byte? Sort_Order { get; set; }
        public bool? TaxExemptRelevant { get; set; }
        public string Company_Name_Label { get; set; }
        public string Country_of_Incorporation_Label { get; set; }
        public string Corporate_Tax_ID_Label { get; set; }
        public string InverstorTypePopUp { get; set; }
        public string InvestorSubTypePopUP { get; set; }
        public NonIndividualRegistrationTypeGroup ClientSubTypeGroup
        {
            get
            {
                return NonIndividualRegistrationTypeGroup.GetBySubType(ClientSubType);
            }
        }
    }
}
