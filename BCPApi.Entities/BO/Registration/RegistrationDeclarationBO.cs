﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO.Registration
{
    public class RegistrationDeclarationBO
    {
        public int DeclarationID { get; set; }
        public string Dec_Label { get; set; }
        public bool Dec_Marketing { get; set; }
        public byte Sort_Order { get; set; }

        //Registration Fields
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public int WebClientIDNumber { get; set; }
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public int WebRegDecID { get; set; }
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public bool? Dec_Signed { get; set; }
    }
}
