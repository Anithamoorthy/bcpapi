﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO.Registration
{
    public class RegistrationDocumentBO
    {
        public int WebDocID { get; set; }
        public string Document_Description { get; set; }
        public string Document_Link { get; set; }
        public byte Sort_Order { get; set; }

        //Registration Fields
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public int WebClientIDNumber { get; set; }
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public int WebRegDocID { get; set; }
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public bool? Viewed { get; set; }
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public DateTime? Viewed_Date { get; set; }
        //[JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public bool? Receipt_Confirmed { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public string DocumentUrl
        {
            get
            {
                return (Document_Link ?? string.Empty).Split(new string[] { "#" }, StringSplitOptions.RemoveEmptyEntries).DefaultIfEmpty(string.Empty).LastOrDefault();
            }
        }

        [JsonIgnore, ScriptIgnore(ApplyToOverrides = true)]
        public string FileName
        {
            get
            {
                string link = DocumentUrl;
                int beginFileName = link.LastIndexOf("/");
                if (link.Length > beginFileName)
                    beginFileName++;
                return link.Substring(beginFileName);
            }
        }
    }
}
