﻿using BCPApi.Entities.BO.Registration;
using BCPApi.Entities.Domain;
using BCPApi.Entity.BO;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace BCPApi.Registration.Controllers
{
    [RoutePrefix("joint")]
    public class JointInvestorController : ApiController
    {
        [Dependency]
        public IInvestmentRepository InvestorService { get; set; }
        [Dependency]
        public IJointInvestorRepository JointInvestorService { get; set; }
        [Dependency]
        public IInvestorTypeRepository InvestorTypeService { get; set; }

        [Route("get-registration-declarations"), HttpPost]
        public IHttpActionResult GetRegistrationDeclarations(int WebClientIDNumber,string InvestorSubTypeEntered)
        {
            return Ok(InvestorService.GetRegistrationDeclarations(WebClientIDNumber, InvestorRegistrationType.Joint.Id, InvestorSubTypeEntered));
        }

        [Route("add-registration-declarations"), HttpPost]
        public IHttpActionResult AddRegistrationDeclarations(List<RegistrationDeclarationBO> registrationDeclarations)
        {
            registrationDeclarations.ForEach(d => d.WebClientIDNumber = registrationDeclarations[0].WebClientIDNumber);
            return Ok(InvestorService.AddRegistrationDeclarations(registrationDeclarations));
        }

        [Route("get-registration-documents"), HttpPost]
        public IHttpActionResult GetRegistrationDocuments(int WebClientIDNumber,string InvestorSubTypeEntered)
        {
            return Ok(InvestorService.GetRegistrationDocuments(WebClientIDNumber, InvestorSubTypeEntered));
        }

        [Route("add-registration-documents"), HttpPost]
        public IHttpActionResult AddRegistrationDocuments(List<RegistrationDocumentBO> registrationDocuments)
        {
            registrationDocuments.ForEach(d => d.WebClientIDNumber = registrationDocuments[0].WebClientIDNumber);
            return Ok(InvestorService.AddRegistrationDocuments(registrationDocuments));
        }

        [Route("save-registration"), HttpPost]
        public IHttpActionResult SaveRegistration(RegistrationJointBO investor)
        {
            investor.RegistrationInProgress = true;
            investor.RegistrationComplete = false;
            IList<InvestorTypeBO> jointTypes = InvestorTypeService.GetByClientType(InvestorRegistrationType.Joint.ClientType);
            if (jointTypes.Count == 1)
            {
                investor.InvestorTypeEntered = jointTypes[0].InvestorTypeEntered;
                investor.InvestorSubTypeEntered = jointTypes[0].InvestorSubTypeEntered;
            }
            investor = JointInvestorService.SaveRegistrationAndSendNotification(investor);           
            return Ok(investor);
        }

        [Route("continue-registration"), HttpPost]
        public IHttpActionResult ContinueRegistration(string notificationCode)
        {
            RegistrationJointBO investor = JointInvestorService.ContinueRegistration(notificationCode);
            investor.ContinueRegistrationToken = notificationCode;            
            return Ok(investor);
        }

        [Route("send-registration-2-factor-code")]
        public IHttpActionResult SendRegistration2FactorCode(int WebClientIDNumber,string phone)
        {
            JointInvestorService.SendRegistration2FactorCode(WebClientIDNumber, phone);
            return Ok(new { Status = true, Message = "The two factor authentication code has been sent successfully." });
        }

        [Route("complete-registration"), HttpPost]
        public IHttpActionResult CompleteRegistration(int WebClientIDNumber,string password, string twoFactorCode,string ContinueRegistrationToken)
        {
            return Ok(InvestorService.CompleteInvestorRegistration(WebClientIDNumber, password, twoFactorCode, NotificationTokenType.JointRegistrationEmailVerification.Id, ContinueRegistrationToken));
        }

        [Route("get-registration"), HttpPost]
        public IHttpActionResult GetRegistration(int WebClientIdNumber)
        {
            return Ok(JointInvestorService.GetRegistration(WebClientIdNumber));
        }

        [Route("send-verification-email"), HttpPost]
        public IHttpActionResult GetRegistration(RegistrationJointBO JointRegistration)
        {
            JointInvestorService.SendVerificationEmail(JointRegistration);
            return Ok(new { Status = true, Message = "The verification email has been sent successfully." });
        }
    }
}
