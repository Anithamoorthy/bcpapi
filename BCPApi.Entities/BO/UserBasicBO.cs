﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO
{
    public class UserBasicBO : ResultBO
    {
        public string Email { get; set; }
        [JsonIgnore]
        public bool? WebEnabled { get; set; }
        public bool? Activated { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public ClientDetailsBO ClientDetails { get; set; }
        public AgentDetailsBO AgentDetails { get; set; }
        public ClientDetailsBO MasterClientDetails { get; set; }

        public string DisplayName
        {
            get
            {
                if (IsClient || (IsMasterClient && ClientDetails != null))
                    return (ClientDetails ?? new ClientDetailsBO()).DisplayFormalName;
                else if (IsAgent)
                    return (AgentDetails ?? new AgentDetailsBO()).DisplayFormalName;
                else
                    return Join(" ", new string[] { FirstName, LastName });
            }
        }

        [JsonIgnore]
        public string ContinueRegistrationToken { get; set; }

        [ScriptIgnore]
        public bool IsClient
        {
            get
            {
                if (LoginCheck != null)
                    return LoginCheck.Client && !LoginCheck.ClientMaster;
                else
                    return "Client".Equals(UserType, StringComparison.OrdinalIgnoreCase);
            }
        }

        [ScriptIgnore]
        public bool IsAgent
        {
            get
            {
                if (LoginCheck != null)
                    return LoginCheck.Agent;
                else
                    return "Agent".Equals(UserType, StringComparison.OrdinalIgnoreCase);
            }
        }

        [ScriptIgnore]
        public bool IsBCPUser
        {
            get
            {
                if (LoginCheck != null)
                    return LoginCheck.BCPUser;
                else
                    return "BCP User".Equals(UserType, StringComparison.OrdinalIgnoreCase);
            }
        }

        [ScriptIgnore]
        public bool IsBCPMaster
        {
            get
            {
                if (LoginCheck != null)
                    return LoginCheck.BCPMaster;
                else
                    return "BCP Master".Equals(UserType, StringComparison.OrdinalIgnoreCase);
            }
        }

        [ScriptIgnore]
        public bool IsMasterClient
        {
            get
            {
                if (LoginCheck != null)
                    return LoginCheck.ClientMaster;
                else
                    return "Master Client".Equals(UserType, StringComparison.OrdinalIgnoreCase);
            }
        }

        [ScriptIgnore]
        public bool IsPensioneer
        {
            get
            {
                if (LoginCheck != null)
                    return LoginCheck.Pensioneer;
                else
                    return "Pensioneer".Equals(UserType, StringComparison.OrdinalIgnoreCase);
            }
        }

        [ScriptIgnore]
        public string ClientCurrencySymbol { get { return (ClientDetails ?? new ClientDetailsBO()).CurrencySymbol; } }

        [JsonIgnore]
        public LoginCheckBO LoginCheck { get; set; }

        private string Join(string separator, string[] values)
        {
            return string.Join(separator, values.Where(w => !string.IsNullOrWhiteSpace(w))).Trim();
        }
    }
}
