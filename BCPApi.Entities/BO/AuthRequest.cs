﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities
{
    public class AuthRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string AccountNumber { get; set; }
        public string PW_Hash { get; set; }
        public string IPAddress { get; set; }
    }
}
