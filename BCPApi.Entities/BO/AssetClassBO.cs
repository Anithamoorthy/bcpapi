﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class AssetClassBO
    {
        public int StockACBID { get; set; }
        public int StockID { get; set; }
        public string AssetClass { get; set; }
        public double Allocation { get; set; }
    }
}
