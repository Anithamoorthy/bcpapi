﻿using BCPApi.Database;
using BCPApi.Entities.BO;
using BCPApi.Entities.BO.Registration;
using BCPApi.Entities.Data;
using BCPApi.Entities.Domain;
using BCPApi.Entities.Exceptions;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class IndividualInvestorRepository: BaseRegistrationService, IIndividualInvestorRepository
    {
        protected IBaseDbContext Db { get; set; }
        private Lazy<IInvestmentRepository> investorService;
        public IInvestmentRepository InvestorService { get { return investorService.Value; } }
        private Lazy<ITokenServiceRepository> tokenService;
        public ITokenServiceRepository TokenService { get { return tokenService.Value; } }
        private Lazy<IEmailServiceRepository> emailService;
        public IEmailServiceRepository EmailService { get { return emailService.Value; } }
        private Lazy<ISmsServiceRepository> smsService;
        public ISmsServiceRepository SmsService { get { return smsService.Value; } }
        //private Lazy<IAuditTrailService> auditTrailService;
        //public IAuditTrailService AuditTrailService { get { return auditTrailService.Value; } }

        public IndividualInvestorRepository(IBaseDbContext db, Lazy<IInvestmentRepository> investorService, Lazy<ITokenServiceRepository> tokenService, Lazy<IEmailServiceRepository> emailService, Lazy<ISmsServiceRepository> smsService)
        {
            Db = db;
            this.investorService = investorService;
            this.tokenService = tokenService;
            this.emailService = emailService;
            this.smsService = smsService;
        }

        public RegistrationIndividualFieldsBO GetRegistrationFields()
        {
            return Db.DataBase.SqlQuery<RegistrationIndividualFieldsBO>("exec CRM_Get_Web_Registration_Fields_Individual").FirstOrDefault();
        }

        public RegistrationIndividualBO GetRegistration(int webClientIDNumber)
        {
            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);

            RegistrationIndividualBO investor = Db.DataBase.SqlQuery<RegistrationIndividualBO>("exec CRM_Get_Registration_Individual @WebClientIDNumber", WebClientIDNumber).FirstOrDefault();
            investor.Assert("Investor not found.", i => investor != null);
            investor.DateOfBirthString = (investor.DateOfBirth.HasValue ? investor.DateOfBirth.Value.ToString("dd/MM/yyyy") : null);
            investor.WebClientIDNumber = webClientIDNumber;
            return investor;
        }

        private void ValidateIndividualRegistration(RegistrationIndividualBO investor)
        {
            investor.Assert("Investor Type is required.", i => !string.IsNullOrWhiteSpace(i.InvestorTypeEntered) && !string.IsNullOrWhiteSpace(i.InvestorSubTypeEntered));
            if (investor.IsARF || investor.IsAMRF || investor.IsPRB)
            {
                investor.Assert(string.Format("Name of life office or {0} provider is required.", (investor.IsARF ? "ARF" : (investor.IsAMRF ? "AMRF" : "PRB"))), i => !string.IsNullOrWhiteSpace(i.SourceOfFunds));
                investor.Assert("Policy or reference number from which funds will be sent to BCP is required.", i => !string.IsNullOrWhiteSpace(i.SourcePolicy));
                if (investor.IsARF)
                {
                    investor.Assert("You must select one Qualifying Fund Manager option.", i => i.ARF_Has_AMRF.HasValue);
                    if (investor.ARF_Has_AMRF.Value)
                    {
                        investor.Assert("Qualifying Fund Manager Name is required.", i => !string.IsNullOrWhiteSpace(i.QFM_Name));
                        investor.Assert("Approved Minimum Retirement Fund reference no is required.", i => !string.IsNullOrWhiteSpace(i.AMRF_Ref));
                        investor.Assert("Amount of original investment is required.", i => i.AMRF_Original_Investment.HasValue && i.AMRF_Original_Investment.Value > 0);
                    }
                    else
                    {
                        investor.Assert("You must select one annuity option.", i => i.AMRF_No_Annuity.HasValue);
                        investor.Assert("You must select one option as \"Yes\".", i => i.AMRF_No_Over75 || i.AMRF_No_Pension || i.AMRF_No_Annuity.Value);
                        if (investor.AMRF_No_Annuity.Value)
                        {
                            investor.Assert("Insurance Company is required.", i => !string.IsNullOrWhiteSpace(i.AMRF_No_InsuranceCo));
                            investor.Assert("Reference Number is required.", i => !string.IsNullOrWhiteSpace(i.AMRF_No_RefNum));
                            investor.Assert("Amount of Premium is required.", i => i.AMRF_No_Premium.HasValue && i.AMRF_No_Premium.Value > 0);
                            investor.Assert("Start Date of Annuity is required.", i => !string.IsNullOrWhiteSpace(i.AMRF_No_AnnuityStartString));
                            try
                            {
                                investor.AMRF_No_AnnuityStart = Convert.ToDateTime(DateTime.ParseExact(investor.AMRF_No_AnnuityStartString, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                            }
                            catch (Exception ex)
                            {
                                throw new ValidationException("Start Date of Annuity is invalid.", ex);
                            }
                        }
                    }
                }
            }
            if (!investor.NoAgent)
            {
                if (investor.RegistrationInProgress && !investor.RegistrationComplete)
                {
                    investor.Assert("Advisor First Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentFirstName));
                    investor.Assert("Advisor Last Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentLastName));
                    investor.Assert("Advisor Company Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentName));
                    //investor.Assert("Advisor Address Line 1 is required.", i => !string.IsNullOrWhiteSpace(i.AgentAddress1));
                    investor.Assert("Advisor City is required.", i => !string.IsNullOrWhiteSpace(i.AgentCity));
                    investor.Assert("Advisor County is required.", i => !string.IsNullOrWhiteSpace(i.AgentCounty));

                    investor.AgentContact = string.Join(" ", new string[] { investor.AgentFirstName, investor.AgentLastName });
                    investor.AgentAddress = string.Join("\r\n", new string[] { investor.AgentAddress1, investor.AgentAddress2, investor.AgentCity, investor.AgentCounty, investor.AgentPostcode }.Where(a => !string.IsNullOrWhiteSpace(a)));
                }
                investor.Assert("Advisor Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentContact));
                investor.Assert("Advisor Company Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentName));
                investor.Assert("Advisor Adress is required.", i => !string.IsNullOrWhiteSpace(i.AgentAddress));
            }
            investor.Assert("Title is required.", i => !string.IsNullOrWhiteSpace(i.Title));
            investor.Assert("First Name is required.", i => !string.IsNullOrWhiteSpace(i.FirstName));
            investor.Assert("Last Name is required.", i => !string.IsNullOrWhiteSpace(i.LastName));
            investor.Assert("Date of Birth is required.", i => !string.IsNullOrWhiteSpace(i.DateOfBirthString));
            try
            {
                investor.DateOfBirth = Convert.ToDateTime(DateTime.ParseExact(investor.DateOfBirthString, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                throw new ValidationException("Date of Birth is invalid.", ex);
            }
            investor.Assert("Email is required.", i => !string.IsNullOrWhiteSpace(i.Onlineemail));
            investor.Assert("Mobile is required.", i => !string.IsNullOrWhiteSpace(i.Mobile));
            investor.Assert("Address Line 1 is required.", i => !string.IsNullOrWhiteSpace(i.Address1));
            investor.Assert("City is required.", i => !string.IsNullOrWhiteSpace(i.Town_City));
            investor.Assert("County is required.", i => !string.IsNullOrWhiteSpace(i.County_ZipCode));
            investor.Assert("Country is required.", i => !string.IsNullOrWhiteSpace(i.Address_Country));
        }

        private RegistrationIndividualBO AddIndividualRegistrationARF(RegistrationIndividualBO investor)
        {
            ValidateIndividualRegistration(investor);

            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", DateTime.Now, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", true, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", false, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Title = CreateSqlParameter("Title", investor.Title, SqlDbType.Text);
            SqlParameter FirstName = CreateSqlParameter("FirstName", investor.FirstName, SqlDbType.Text);
            SqlParameter LastName = CreateSqlParameter("LastName", investor.LastName, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter DateOfBirth = CreateSqlParameter("DateOfBirth", investor.DateOfBirth, SqlDbType.DateTime2);
            SqlParameter Person1CountryOfBirth = CreateSqlParameter("Person1CountryOfBirth", investor.Person1CountryOfBirth, SqlDbType.Text);
            SqlParameter Person1PlaceOfBirth = CreateSqlParameter("Person1PlaceOfBirth", investor.Person1PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number = CreateSqlParameter("PPS_Number", investor.PPS_Number, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);

            SqlParameter SourceOfFunds = CreateSqlParameter("SourceOfFunds", investor.SourceOfFunds, SqlDbType.Text);
            SqlParameter SourcePolicy = CreateSqlParameter("SourcePolicy", investor.SourcePolicy, SqlDbType.Text);
            SqlParameter ARF_Has_AMRF = CreateSqlParameter("ARF_Has_AMRF", investor.ARF_Has_AMRF, SqlDbType.Bit);
            SqlParameter QFM_Name = CreateSqlParameter("QFM_Name", investor.QFM_Name, SqlDbType.Text);
            SqlParameter AMRF_Ref = CreateSqlParameter("AMRF_Ref", investor.AMRF_Ref, SqlDbType.Text);
            SqlParameter AMRF_Original_Investment = CreateSqlParameter("AMRF_Original_Investment", investor.AMRF_Original_Investment, SqlDbType.Money);
            SqlParameter AMRF_No_Over75 = CreateSqlParameter("AMRF_No_Over75", investor.AMRF_No_Over75, SqlDbType.Bit);
            SqlParameter AMRF_No_Pension = CreateSqlParameter("AMRF_No_Pension", investor.AMRF_No_Pension, SqlDbType.Bit);
            SqlParameter AMRF_No_Annuity = CreateSqlParameter("AMRF_No_Annuity", investor.AMRF_No_Annuity, SqlDbType.Bit);
            SqlParameter AMRF_No_InsuranceCo = CreateSqlParameter("AMRF_No_InsuranceCo", investor.AMRF_No_InsuranceCo, SqlDbType.Text);
            SqlParameter AMRF_No_RefNum = CreateSqlParameter("AMRF_No_RefNum", investor.AMRF_No_RefNum, SqlDbType.Text);
            SqlParameter AMRF_No_Premium = CreateSqlParameter("AMRF_No_Premium", investor.AMRF_No_Premium, SqlDbType.Money);
            SqlParameter AMRF_No_AnnuityStart = CreateSqlParameter("AMRF_No_AnnuityStart", investor.AMRF_No_AnnuityStart, SqlDbType.DateTime2);

            investor.WebClientIDNumber = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Registration_Individual_ARF @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, " +
                "@InvestorTypeEntered, @InvestorSubTypeEntered, @Title, @FirstName, @LastName, @Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, " +
                "@DateOfBirth, @Person1CountryOfBirth, @Person1PlaceOfBirth, @PPS_Number, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, " +
                "@OnlineDateRegistered, @SourceOfFunds, @SourcePolicy, @ARF_Has_AMRF, @QFM_Name, @AMRF_Ref, @AMRF_Original_Investment, @AMRF_No_Over75, @AMRF_No_Pension, @AMRF_No_Annuity, " +
                "@AMRF_No_InsuranceCo, @AMRF_No_RefNum, @AMRF_No_Premium, @AMRF_No_AnnuityStart", DateRecordAdded, RegistrationInProgress, RegistrationComplete, InvestorTypeEntered,
                InvestorSubTypeEntered, Title, FirstName, LastName, Address1, Address2, Address3, Town_City, County_ZipCode, Postcode, Address_Country, DateOfBirth, Person1CountryOfBirth,
                Person1PlaceOfBirth, PPS_Number, Phone, Mobile, Onlineemail, AgentContact, AgentName, AgentAddress, NoAgent, OnlineDateSetUp, OnlineDateRegistered, SourceOfFunds, SourcePolicy,
                ARF_Has_AMRF, QFM_Name, AMRF_Ref, AMRF_Original_Investment, AMRF_No_Over75, AMRF_No_Pension, AMRF_No_Annuity, AMRF_No_InsuranceCo, AMRF_No_RefNum, AMRF_No_Premium,
                AMRF_No_AnnuityStart).FirstOrDefault();
            return investor;
        }

        private RegistrationIndividualBO AddIndividualRegistrationAMRFPRB(RegistrationIndividualBO investor)
        {
            ValidateIndividualRegistration(investor);

            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", DateTime.Now, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", true, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", false, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Title = CreateSqlParameter("Title", investor.Title, SqlDbType.Text);
            SqlParameter FirstName = CreateSqlParameter("FirstName", investor.FirstName, SqlDbType.Text);
            SqlParameter LastName = CreateSqlParameter("LastName", investor.LastName, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter DateOfBirth = CreateSqlParameter("DateOfBirth", investor.DateOfBirth, SqlDbType.DateTime2);
            SqlParameter Person1CountryOfBirth = CreateSqlParameter("Person1CountryOfBirth", investor.Person1CountryOfBirth, SqlDbType.Text);
            SqlParameter Person1PlaceOfBirth = CreateSqlParameter("Person1PlaceOfBirth", investor.Person1PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number = CreateSqlParameter("PPS_Number", investor.PPS_Number, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);

            SqlParameter SourceOfFunds = CreateSqlParameter("SourceOfFunds", investor.SourceOfFunds, SqlDbType.Text);
            SqlParameter SourcePolicy = CreateSqlParameter("SourcePolicy", investor.SourcePolicy, SqlDbType.Text);

            investor.WebClientIDNumber = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Registration_Individual_AMRFPRB @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, @InvestorTypeEntered, @InvestorSubTypeEntered, @Title, " +
                "@FirstName, @LastName, @Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, @DateOfBirth, @Person1CountryOfBirth, @Person1PlaceOfBirth, " +
                "@PPS_Number, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, @OnlineDateRegistered, @SourceOfFunds, @SourcePolicy",
                DateRecordAdded, RegistrationInProgress, RegistrationComplete, InvestorTypeEntered, InvestorSubTypeEntered, Title, FirstName, LastName, Address1, Address2, Address3, Town_City,
                County_ZipCode, Postcode, Address_Country, DateOfBirth, Person1CountryOfBirth, Person1PlaceOfBirth, PPS_Number, Phone, Mobile, Onlineemail, AgentContact, AgentName, AgentAddress,
                NoAgent, OnlineDateSetUp, OnlineDateRegistered, SourceOfFunds, SourcePolicy).FirstOrDefault();
            return investor;
        }

        private RegistrationIndividualBO AddIndividualRegistration(RegistrationIndividualBO investor)
        {
            ValidateIndividualRegistration(investor);

            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", DateTime.Now, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", true, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", false, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Title = CreateSqlParameter("Title", investor.Title, SqlDbType.Text);
            SqlParameter FirstName = CreateSqlParameter("FirstName", investor.FirstName, SqlDbType.Text);
            SqlParameter LastName = CreateSqlParameter("LastName", investor.LastName, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter DateOfBirth = CreateSqlParameter("DateOfBirth", investor.DateOfBirth, SqlDbType.DateTime2);
            SqlParameter Person1CountryOfBirth = CreateSqlParameter("Person1CountryOfBirth", investor.Person1CountryOfBirth, SqlDbType.Text);
            SqlParameter Person1PlaceOfBirth = CreateSqlParameter("Person1PlaceOfBirth", investor.Person1PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number = CreateSqlParameter("PPS_Number", investor.PPS_Number, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);

            investor.WebClientIDNumber = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Registration_Individual @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, @InvestorTypeEntered, @InvestorSubTypeEntered, @Title, " +
                "@FirstName, @LastName, @Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, @DateOfBirth, @Person1CountryOfBirth, @Person1PlaceOfBirth, " +
                "@PPS_Number, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, @OnlineDateRegistered", DateRecordAdded, RegistrationInProgress,
                RegistrationComplete, InvestorTypeEntered, InvestorSubTypeEntered, Title, FirstName, LastName, Address1, Address2, Address3, Town_City, County_ZipCode, Postcode, Address_Country,
                DateOfBirth, Person1CountryOfBirth, Person1PlaceOfBirth, PPS_Number, Phone, Mobile, Onlineemail, AgentContact, AgentName, AgentAddress, NoAgent, OnlineDateSetUp,
                OnlineDateRegistered).FirstOrDefault();
            return investor;
        }

        public RegistrationIndividualBO SaveRegistrationAndSendNotification(RegistrationIndividualBO investor)
        {
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            RegistrationIndividualBO registrationIndividual = null;
            if (investor.IsARF)
            {
                registrationIndividual = this.AddIndividualRegistrationARF(investor);
            }
            else if (investor.IsAMRF || investor.IsPRB)
            {
                registrationIndividual = this.AddIndividualRegistrationAMRFPRB(investor);
            }
            else
            {
                registrationIndividual = this.AddIndividualRegistration(investor);
            }
            try
            {
                SendVerificationEmail(registrationIndividual);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return registrationIndividual;
        }

        public RegistrationIndividualBO ContinueRegistration(string notificationCode)
        {
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            TokenBO tokenDetails = TokenService.GetTokenDetails(notificationCode, NotificationTokenType.IndividualRegistrationEmailVerification.Id);
            RegistrationIndividualBO investor = this.GetRegistration(tokenDetails.WebClientIDNumber);
            investor.Assert("Investor registration already completed or not found.", i => i != null);
            return investor;
        }

        private RegistrationIndividualBO UpdateIndividualRegistrationARF(RegistrationIndividualBO investor)
        {
            ValidateIndividualRegistration(investor);

            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", investor.WebClientIDNumber, SqlDbType.Int);
            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", investor.DateRecordAdded, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", investor.RegistrationInProgress, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", investor.RegistrationComplete, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Title = CreateSqlParameter("Title", investor.Title, SqlDbType.Text);
            SqlParameter FirstName = CreateSqlParameter("FirstName", investor.FirstName, SqlDbType.Text);
            SqlParameter LastName = CreateSqlParameter("LastName", investor.LastName, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter DateOfBirth = CreateSqlParameter("DateOfBirth", investor.DateOfBirth, SqlDbType.DateTime2);
            SqlParameter Person1CountryOfBirth = CreateSqlParameter("Person1CountryOfBirth", investor.Person1CountryOfBirth, SqlDbType.Text);
            SqlParameter Person1PlaceOfBirth = CreateSqlParameter("Person1PlaceOfBirth", investor.Person1PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number = CreateSqlParameter("PPS_Number", investor.PPS_Number, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);

            SqlParameter SourceOfFunds = CreateSqlParameter("SourceOfFunds", investor.SourceOfFunds, SqlDbType.Text);
            SqlParameter SourcePolicy = CreateSqlParameter("SourcePolicy", investor.SourcePolicy, SqlDbType.Text);
            SqlParameter ARF_Has_AMRF = CreateSqlParameter("ARF_Has_AMRF", investor.ARF_Has_AMRF, SqlDbType.Bit);
            SqlParameter QFM_Name = CreateSqlParameter("QFM_Name", investor.QFM_Name, SqlDbType.Text);
            SqlParameter AMRF_Ref = CreateSqlParameter("AMRF_Ref", investor.AMRF_Ref, SqlDbType.Text);
            SqlParameter AMRF_Original_Investment = CreateSqlParameter("AMRF_Original_Investment", investor.AMRF_Original_Investment, SqlDbType.Money);
            SqlParameter AMRF_No_Over75 = CreateSqlParameter("AMRF_No_Over75", investor.AMRF_No_Over75, SqlDbType.Bit);
            SqlParameter AMRF_No_Pension = CreateSqlParameter("AMRF_No_Pension", investor.AMRF_No_Pension, SqlDbType.Bit);
            SqlParameter AMRF_No_Annuity = CreateSqlParameter("AMRF_No_Annuity", investor.AMRF_No_Annuity, SqlDbType.Bit);
            SqlParameter AMRF_No_InsuranceCo = CreateSqlParameter("AMRF_No_InsuranceCo", investor.AMRF_No_InsuranceCo, SqlDbType.Text);
            SqlParameter AMRF_No_RefNum = CreateSqlParameter("AMRF_No_RefNum", investor.AMRF_No_RefNum, SqlDbType.Text);
            SqlParameter AMRF_No_Premium = CreateSqlParameter("AMRF_No_Premium", investor.AMRF_No_Premium, SqlDbType.Money);
            SqlParameter AMRF_No_AnnuityStart = CreateSqlParameter("AMRF_No_AnnuityStart", investor.AMRF_No_AnnuityStart, SqlDbType.DateTime2);

            string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_Registration_Individual_ARF @WebClientIDNumber, @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, " +
                "@InvestorTypeEntered, @InvestorSubTypeEntered, @Title, @FirstName, @LastName, @Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, " +
                "@DateOfBirth, @Person1CountryOfBirth, @Person1PlaceOfBirth, @PPS_Number, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, " +
                "@OnlineDateRegistered, @SourceOfFunds, @SourcePolicy, @ARF_Has_AMRF, @QFM_Name, @AMRF_Ref, @AMRF_Original_Investment, @AMRF_No_Over75, @AMRF_No_Pension, @AMRF_No_Annuity, " +
                "@AMRF_No_InsuranceCo, @AMRF_No_RefNum, @AMRF_No_Premium, @AMRF_No_AnnuityStart", WebClientIDNumber, DateRecordAdded, RegistrationInProgress, RegistrationComplete,
                InvestorTypeEntered, InvestorSubTypeEntered, Title, FirstName, LastName, Address1, Address2, Address3, Town_City, County_ZipCode, Postcode, Address_Country, DateOfBirth,
                Person1CountryOfBirth, Person1PlaceOfBirth, PPS_Number, Phone, Mobile, Onlineemail, AgentContact, AgentName, AgentAddress, NoAgent, OnlineDateSetUp, OnlineDateRegistered,
                SourceOfFunds, SourcePolicy, ARF_Has_AMRF, QFM_Name, AMRF_Ref, AMRF_Original_Investment, AMRF_No_Over75, AMRF_No_Pension, AMRF_No_Annuity, AMRF_No_InsuranceCo, AMRF_No_RefNum,
                AMRF_No_Premium, AMRF_No_AnnuityStart).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
            return investor;
        }

        private RegistrationIndividualBO UpdateIndividualRegistrationAMRFPRB(RegistrationIndividualBO investor)
        {
            ValidateIndividualRegistration(investor);

            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", investor.WebClientIDNumber, SqlDbType.Int);
            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", investor.DateRecordAdded, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", investor.RegistrationInProgress, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", investor.RegistrationComplete, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Title = CreateSqlParameter("Title", investor.Title, SqlDbType.Text);
            SqlParameter FirstName = CreateSqlParameter("FirstName", investor.FirstName, SqlDbType.Text);
            SqlParameter LastName = CreateSqlParameter("LastName", investor.LastName, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter DateOfBirth = CreateSqlParameter("DateOfBirth", investor.DateOfBirth, SqlDbType.DateTime2);
            SqlParameter Person1CountryOfBirth = CreateSqlParameter("Person1CountryOfBirth", investor.Person1CountryOfBirth, SqlDbType.Text);
            SqlParameter Person1PlaceOfBirth = CreateSqlParameter("Person1PlaceOfBirth", investor.Person1PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number = CreateSqlParameter("PPS_Number", investor.PPS_Number, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);

            SqlParameter SourceOfFunds = CreateSqlParameter("SourceOfFunds", investor.SourceOfFunds, SqlDbType.Text);
            SqlParameter SourcePolicy = CreateSqlParameter("SourcePolicy", investor.SourcePolicy, SqlDbType.Text);

            string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_Registration_Individual_AMRFPRB @WebClientIDNumber, @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, " +
                "@InvestorTypeEntered, @InvestorSubTypeEntered, @Title, @FirstName, @LastName, @Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, " +
                "@DateOfBirth, @Person1CountryOfBirth, @Person1PlaceOfBirth, @PPS_Number, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, " +
                "@OnlineDateRegistered, @SourceOfFunds, @SourcePolicy", WebClientIDNumber, DateRecordAdded, RegistrationInProgress, RegistrationComplete, InvestorTypeEntered, InvestorSubTypeEntered,
                Title, FirstName, LastName, Address1, Address2, Address3, Town_City, County_ZipCode, Postcode, Address_Country, DateOfBirth, Person1CountryOfBirth, Person1PlaceOfBirth, PPS_Number,
                Phone, Mobile, Onlineemail, AgentContact, AgentName, AgentAddress, NoAgent, OnlineDateSetUp, OnlineDateRegistered, SourceOfFunds, SourcePolicy).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
            return investor;
        }

        private RegistrationIndividualBO UpdateIndividualRegistration(RegistrationIndividualBO investor)
        {
            ValidateIndividualRegistration(investor);

            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", investor.WebClientIDNumber, SqlDbType.Int);
            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", investor.DateRecordAdded, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", investor.RegistrationInProgress, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", investor.RegistrationComplete, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Title = CreateSqlParameter("Title", investor.Title, SqlDbType.Text);
            SqlParameter FirstName = CreateSqlParameter("FirstName", investor.FirstName, SqlDbType.Text);
            SqlParameter LastName = CreateSqlParameter("LastName", investor.LastName, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter DateOfBirth = CreateSqlParameter("DateOfBirth", investor.DateOfBirth, SqlDbType.DateTime2);
            SqlParameter Person1CountryOfBirth = CreateSqlParameter("Person1CountryOfBirth", investor.Person1CountryOfBirth, SqlDbType.Text);
            SqlParameter Person1PlaceOfBirth = CreateSqlParameter("Person1PlaceOfBirth", investor.Person1PlaceOfBirth, SqlDbType.Text);
            SqlParameter PPS_Number = CreateSqlParameter("PPS_Number", investor.PPS_Number, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);

            string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_Registration_Individual @WebClientIDNumber, @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, " +
                "@InvestorTypeEntered, @InvestorSubTypeEntered, @Title, @FirstName, @LastName, @Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, " +
                "@DateOfBirth, @Person1CountryOfBirth, @Person1PlaceOfBirth, @PPS_Number, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, " +
                "@OnlineDateRegistered ", WebClientIDNumber, DateRecordAdded, RegistrationInProgress, RegistrationComplete, InvestorTypeEntered, InvestorSubTypeEntered, Title, FirstName, LastName,
                Address1, Address2, Address3, Town_City, County_ZipCode, Postcode, Address_Country, DateOfBirth, Person1CountryOfBirth, Person1PlaceOfBirth, PPS_Number, Phone, Mobile, Onlineemail,
                AgentContact, AgentName, AgentAddress, NoAgent, OnlineDateSetUp, OnlineDateRegistered).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
            return investor;
        }

        public void SendRegistration2FactorCode(int webClientIDNumber, string phone)
        {
            phone.Assert("Phone is required.", p => !string.IsNullOrWhiteSpace(p));
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            RegistrationIndividualBO investor = this.GetRegistration(webClientIDNumber);
            if (!phone.Equals(investor.Mobile))
            {
                investor.Mobile = phone;
                if (investor.IsARF)
                {
                    this.UpdateIndividualRegistrationARF(investor);
                }
                else if (investor.IsAMRF || investor.IsPRB)
                {
                    this.UpdateIndividualRegistrationAMRFPRB(investor);
                }
                else
                {
                    this.UpdateIndividualRegistration(investor);
                }
                UpdateIndividualRegistration(investor);
            }
            try
            {
                DateTime now = DateTime.Now;
                TokenBO investorNotification = new TokenBO()
                {
                    WebClientIDNumber = webClientIDNumber,
                    InvestorNotificationType_Id = NotificationTokenType.CompleteRegistrationSmsVerification.Id,
                    Token_Code = TokenBO.GenerateTextMessageHash(),
                    Token_Created_Date = now,
                    Token_Expiry_Date = now.AddHours(1)
                };
                TokenService.AddToken(investorNotification);
                if (!ApplicationSettings.Instance.SendRegistration2FactorCodeByEmail)
                {
                    //Send sms
                    SmsService.SendSms(phone, string.Format("Your BCP Vespro verification code is {0}. Please note: This code will expire in 1 hour.", investorNotification.Token_Code));
                    //AuditTrailService.AddAuditTrailRecord(string.Empty, "Individual registration", "Send two factor authentication code \"" + investorNotification.Token_Code + "\" by sms to: " + phone, NetworkUtils.GetRequestIpAddress(), null, null, string.Empty, NetworkUtils.GetRequestUserAgent());
                }
                else
                {
                    EmailService.SendEmail(new EmailBO()
                    {
                        From = ApplicationSettings.Instance.MailFrom,
                        To = investor.Onlineemail,
                        Subject = "Your BCP Vespro verification code",
                        Content = string.Format("Your BCP Vespro verification code is {0}. Please note: This code will expire in 1 hour.", investorNotification.Token_Code)
                    });
                    //AuditTrailService.AddAuditTrailRecord(string.Empty, "Individual registration", "Send two factor authentication code \"" + investorNotification.Token_Code + "\" by email to: " + investor.Onlineemail, NetworkUtils.GetRequestIpAddress(), null, null, string.Empty, NetworkUtils.GetRequestUserAgent());
                }
                //transaction.Commit();
            }
            catch (Exception ex)
            {
                //transaction.Rollback();
                throw ex;
            }
        }

        public void SendVerificationEmail(RegistrationIndividualBO investor)
        {
            DateTime now = DateTime.Now;
            TokenBO investorNotification = new TokenBO()
            {
                WebClientIDNumber = investor.WebClientIDNumber,
                InvestorNotificationType_Id = NotificationTokenType.IndividualRegistrationEmailVerification.Id,
                Token_Code = TokenBO.GenerateEmailHash(),
                Token_Created_Date = now,
                Token_Expiry_Date = now.AddHours(1)
            };
            TokenService.AddToken(investorNotification);
            EmailService.SendEmail(new EmailBO()
            {
                From = ApplicationSettings.Instance.MailFrom,
                To = investor.Onlineemail,
                Subject = "BCP Vespro Email Address Validation",
                Content = RegistrationConfirmationEmailContent(string.Join(" ", investor.Title, investor.LastName), IndividualRegistrationConfirmationLink(investorNotification.Token_Code))
            });
        }

    }
}
