﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class InvestmentDetailsBO
    {
        public int Client_ID_Number { get; set; }
        public int Holding_ID { get; set; }
    }
}
