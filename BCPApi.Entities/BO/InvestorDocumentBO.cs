﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO
{
    public class InvestorDocumentBO
    {
        public int WebDocID { get; set; }
        public string Document_Description { get; set; }
        public string Document_Link { get; set; }
        public bool Incl_All { get; set; }
        public bool Incl_ARF { get; set; }
        public bool Incl_AMRF { get; set; }
        public bool Incl_PRB { get; set; }
        public bool Incl_CU { get; set; }
        public byte Sort_Order { get; set; }
        public bool Direct { get; set; }
        public bool Agent { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public string DocumentUrl
        {
            get
            {
                return (Document_Link ?? string.Empty).Split(new string[] { "#" }, StringSplitOptions.RemoveEmptyEntries).DefaultIfEmpty(string.Empty).LastOrDefault();
            }
        }
    }
}
