﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO.Registration
{
    public class RegistrationAccountBO
    {
        public int ClientIDNumber { get; set; }
        public string ClientAccountNumber { get; set; }
    }
}
