﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO
{
    public class InvestmentBO
    {
        public string Sector { get; set; }
        public int Client_ID_Number { get; set; }
        public string Stock_Name { get; set; }
        public DateTime? Date_Acquired { get; set; }
        public string Currency { get; set; }
        public double Quantity1 { get; set; }
        public double ValPrice { get; set; }
        public decimal ValValue1 { get; set; }
        public double ValValueAlt { get; set; }
        public DateTime? ValDate { get; set; }
        public decimal BuyVal1 { get; set; }
        public double BuyValAlt { get; set; }
        public int Holding_ID { get; set; }
        public int StockID { get; set; }
        public double CurrentPrice { get; set; }
        public double CurrentVal { get; set; }
        public decimal PercentageChange { get; set; }
        public decimal Amount_Change { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? Date_Last_Updated { get; set; }
        public string Last_Updated_By { get; set; }
        public string StockCurrency { get; set; }
        public double StockPrice { get; set; }
        public double ExchangeRate { get; set; }
        public DateTime? Date_Updated { get; set; }
        public DateTime? StockPriceDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string MasterHoldingName { get; set; }
        public double? AssetPerformance { get; set; }
        public string KickOut { get; set; }
        public int KickOutStock { get; set; }
        public bool ELGStock { get; set; }
        public IList<AssetClassBO> AssetClasses { get; set; }
        [ScriptIgnore]
        public string CurrencySymbol
        {
            get
            {
                string currency = Currency ?? "EUR";
                string symbol = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures)
                    .Where(c => !c.IsNeutralCulture)
                    .Select(c =>
                    {
                        try { return new System.Globalization.RegionInfo(c.LCID); }
                        catch { return null; }
                    })
                    .Where(ri => ri != null && ri.ISOCurrencySymbol == currency)
                    .Select(ri => ri.CurrencySymbol)
                    .FirstOrDefault();
                return symbol ?? "€";
            }
        }
    }
}
