﻿using BCPApi.Database;
using BCPApi.Entities.BO;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class TokenServiceRepository : CommonService, ITokenServiceRepository
    {
        //protected IBaseDbContext Db = new BaseDbContext();

        protected IBaseDbContext Db { get; set; }

        public TokenServiceRepository(IBaseDbContext db)
        {
            Db = db;
        }

        public void AddToken(TokenBO tokenBO)
        {
            SqlParameter Token_Code = CreateSqlParameter("Token_Code", tokenBO.Token_Code, SqlDbType.Text),
                AC_Num = CreateSqlParameter("AC_Num", tokenBO.AC_Num, SqlDbType.Text),
                WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", tokenBO.WebClientIDNumber, SqlDbType.Int),
                InvestorNotificationTypeID = CreateSqlParameter("InvestorNotificationTypeID", tokenBO.InvestorNotificationType_Id, SqlDbType.SmallInt),
                Token_Used_Date = CreateSqlParameter("Token_Used_Date", null, SqlDbType.DateTime2),
                Token_Expiry_Date = CreateSqlParameter("Token_Expiry_Date", tokenBO.Token_Expiry_Date, SqlDbType.DateTime2),
                Token_Created_Date = CreateSqlParameter("Token_Created_Date", tokenBO.Token_Created_Date, SqlDbType.DateTime2);
            int tokenId = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Token @Token_Code, @AC_Num, @WebClientIDNumber, @InvestorNotificationTypeID, @Token_Used_Date, @Token_Expiry_Date, @Token_Created_Date",
                Token_Code, AC_Num, WebClientIDNumber, InvestorNotificationTypeID, Token_Used_Date, Token_Expiry_Date, Token_Created_Date).FirstOrDefault();           
        }

        public void UpdateToken(TokenBO tokenBO)
        {
            SqlParameter Token_ID = CreateSqlParameter("Token_ID", tokenBO.Token_ID, SqlDbType.Int),
                Token_Code = CreateSqlParameter("Token_Code", tokenBO.Token_Code, SqlDbType.Text),
                AC_Num = CreateSqlParameter("AC_Num", tokenBO.AC_Num, SqlDbType.Text),
                WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", tokenBO.WebClientIDNumber, SqlDbType.Int),
                InvestorNotificationTypeID = CreateSqlParameter("InvestorNotificationTypeID", tokenBO.InvestorNotificationType_Id, SqlDbType.SmallInt),
                Token_Used_Date = CreateSqlParameter("Token_Used_Date", tokenBO.Token_Used_Date, SqlDbType.DateTime2),
                Token_Expiry_Date = CreateSqlParameter("Token_Expiry_Date", tokenBO.Token_Expiry_Date, SqlDbType.DateTime2);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Update_Token @Token_ID, @Token_Code, @AC_Num, @WebClientIDNumber, @InvestorNotificationTypeID, @Token_Used_Date, @Token_Expiry_Date",
                Token_ID, Token_Code, AC_Num, WebClientIDNumber, InvestorNotificationTypeID, Token_Used_Date, Token_Expiry_Date).FirstOrDefault();            
        }

        public void DeleteToken(int tokenId)
        {
            SqlParameter Token_ID = CreateSqlParameter("Token_ID", tokenId, SqlDbType.Int);
            string result = Db.DataBase.SqlQuery<string>("exec CRM_Delete_Token @Token_ID", Token_ID).FirstOrDefault();
        }

        public void CheckToken(string tokenCode, string acNum, int webClientNumber, short tokenType)
        {
            SqlParameter Token_Code = CreateSqlParameter("Token_Code", tokenCode, SqlDbType.Text),
                AC_Num = CreateSqlParameter("AC_Num", acNum, SqlDbType.Text),
                WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientNumber, SqlDbType.Int),
                InvestorNotificationTypeID = CreateSqlParameter("InvestorNotificationTypeID", tokenType, SqlDbType.SmallInt);
            int result = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Check_Token @Token_Code, @AC_Num, @WebClientIDNumber, @InvestorNotificationTypeID",
                Token_Code, AC_Num, WebClientIDNumber, InvestorNotificationTypeID).FirstOrDefault();
        }
        public TokenBO GetTokenDetails(string tokenCode, short tokenTypeId)
        {
            SqlParameter Token_ID = CreateSqlParameter("Token_Code", tokenCode, SqlDbType.Text);
            TokenBO tokenDetails = Db.DataBase.SqlQuery<TokenBO>("exec CRM_Get_Token_Details @Token_Code", Token_ID).FirstOrDefault();
            return tokenDetails;
        }
    }
}
