﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class InvestmentDeclarationBO
    {
        public int WIDecID { get; set; }
        public int WInvestmentID { get; set; }
        public string Dec_Label { get; set; }
        public bool? DecSigned { get; set; }
        public bool Dec_Marketing { get; set; }
        public byte Sort_Order { get; set; }
    }
}
