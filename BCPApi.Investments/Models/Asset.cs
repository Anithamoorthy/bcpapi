﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BCPApi.Investments.Models
{
    public class Asset
    {
        public int StockID { get; set; }
        public string Stock_Name { get; set; }
        public List<AssetClassList> AssetClasses { get; set; }
    }

    public class AssetDAO : IAsset
    {
        public List<Asset> GetAssetAddFunds()
        {
            List<Asset> assets = new List<Asset>();
            Asset asset = new Asset();
            try
            {
                string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCPConnection"].ToString();
                using (SqlConnection ConLogin = new SqlConnection(constr))
                {
                    ConLogin.Open();
                    using (SqlCommand CmdLogin = new SqlCommand("[CRM_Get_Asset_Chart_Add_Funds]", ConLogin))
                    {
                        CmdLogin.CommandType = CommandType.StoredProcedure;
                        using (var reader = CmdLogin.ExecuteReader())
                        {
                            while (reader.HasRows & reader.Read())
                            {
                                asset = new Asset();
                                asset.Stock_Name = reader[0] as string;
                                asset.StockID = reader[1] as int? ?? default(int);
                                asset.AssetClasses = new BCPApi.Investments.Models.Investment().GetAssetClasses(asset.StockID);
                                assets.Add(asset);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return assets;
        }
    }
}