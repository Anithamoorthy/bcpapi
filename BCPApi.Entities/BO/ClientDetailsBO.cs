﻿using BCPApi.Entities.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO
{
    public class ClientDetailsBO
    {
        [JsonIgnore]
        public int Client_ID_Number { get; set; }
        public string ClientAccountNumber { get; set; }
        [JsonIgnore]
        public string Status { get; set; }
        public string ClientType { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [ScriptIgnore]
        public string Company_Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town_City { get; set; }
        public string County_ZipCode { get; set; }
        public string Postcode { get; set; }
        public string Address_Country { get; set; }
        [ScriptIgnore]
        public string Business_Phone { get; set; }
        [ScriptIgnore]
        public string Mobile { get; set; }
        [ScriptIgnore]
        public string Onlineemail { get; set; }
        [ScriptIgnore]
        public DateTime? DateOfBirth { get; set; }
        [ScriptIgnore]
        public string PPS_Number { get; set; }
        public string Title2 { get; set; }
        public string FirstName2 { get; set; }
        public string LastName2 { get; set; }
        [ScriptIgnore]
        public DateTime? DOB2 { get; set; }
        [ScriptIgnore]
        public string PPS_Number2 { get; set; }
        public string BCP_Partner { get; set; }
        public bool ThirdPartyAgent { get; set; }
        public string ExternalAgentCode { get; set; }
        public string AgentBranchCode { get; set; }
        public int? AdvisorID { get; set; }
        [ScriptIgnore]
        public string AgentContact { get; set; }
        [ScriptIgnore]
        public string AgentName { get; set; }
        [ScriptIgnore]
        public string AgentTradingName { get; set; }
        public string InvestmentType { get; set; }
        [ScriptIgnore]
        public bool Deceased { get; set; }
        [ScriptIgnore]
        public bool Non_Dirt { get; set; }
        [ScriptIgnore]
        public bool ARF_Account { get; set; }
        [ScriptIgnore]
        public bool BCP_QFM { get; set; }
        [ScriptIgnore]
        public bool PRBAccount { get; set; }
        [ScriptIgnore]
        public DateTime? AMLLastReviewedDate { get; set; }
        [ScriptIgnore]
        public string DefaultRegistration { get; set; }
        public string ContactTitle { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        [ScriptIgnore]
        public string Person1_TaxUS { get; set; }
        [ScriptIgnore]
        public string Person1_CitizenUS { get; set; }
        [ScriptIgnore]
        public string Person1_PlaceOfBirth { get; set; }
        [ScriptIgnore]
        public string Person1_CountryOfBirth { get; set; }
        [ScriptIgnore]
        public string Person1_TaxResOther { get; set; }
        [ScriptIgnore]
        public string Person1_TaxOtherCountry { get; set; }
        [ScriptIgnore]
        public string Person1_OtherTIN { get; set; }
        [ScriptIgnore]
        public string Person2_TaxUS { get; set; }
        [ScriptIgnore]
        public string Person2_CitizenUS { get; set; }
        [ScriptIgnore]
        public string Person2_PlaceOfBirth { get; set; }
        [ScriptIgnore]
        public string Person2_CountryOfBirth { get; set; }
        [ScriptIgnore]
        public string Person2_TaxResOther { get; set; }
        [ScriptIgnore]
        public string Person2_TaxOtherCountry { get; set; }
        [ScriptIgnore]
        public string Person2_OtherTIN { get; set; }
        public bool? OnlineAccount { get; set; }
        [ScriptIgnore]
        public DateTime? OnlineDateRegistered { get; set; }
        public bool? OnlineRegistered { get; set; }
        [ScriptIgnore]
        public string PensionProvider { get; set; }
        [ScriptIgnore]
        public string Pensioneer { get; set; }
        [ScriptIgnore]
        public bool? OnlineMaster { get; set; }
        [ScriptIgnore]
        public string OnlineMasterAcNum { get; set; }
        [ScriptIgnore]
        public string Person1_TaxResIreland { get; set; }
        [ScriptIgnore]
        public string Person1_TaxOtherCountry1 { get; set; }
        [ScriptIgnore]
        public string Person1_OtherTIN1 { get; set; }
        [ScriptIgnore]
        public string Person1_TaxOtherCountry2 { get; set; }
        [ScriptIgnore]
        public string Person1_OtherTIN2 { get; set; }
        [ScriptIgnore]
        public string Person2_TaxResIreland { get; set; }
        [ScriptIgnore]
        public string Person2_TaxOtherCountry1 { get; set; }
        [ScriptIgnore]
        public string Person2_OtherTIN1 { get; set; }
        [ScriptIgnore]
        public string Person2_TaxOtherCountry2 { get; set; }
        [ScriptIgnore]
        public string Person2_OtherTIN2 { get; set; }
        [ScriptIgnore]
        public string BankAccountName { get; set; }
        [ScriptIgnore]
        public string BankBIC { get; set; }
        [ScriptIgnore]
        public string BankIBAN { get; set; }
        [ScriptIgnore]
        public string BankName { get; set; }
        [ScriptIgnore]
        public string BankAddress { get; set; }
        [ScriptIgnore]
        public string Country_of_Incorporation { get; set; }
        [ScriptIgnore]
        public string Corporate_Tax_ID { get; set; }
        [ScriptIgnore]
        public string Address1_upd { get; set; }
        [ScriptIgnore]
        public string Address2_upd { get; set; }
        [ScriptIgnore]
        public string TownCity_upd { get; set; }
        [ScriptIgnore]
        public string County_upd { get; set; }
        [ScriptIgnore]
        public string Postcode_upd { get; set; }
        [ScriptIgnore]
        public string Phone_upd { get; set; }
        [ScriptIgnore]
        public string AgentName_upd { get; set; }
        [ScriptIgnore]
        public string AgentBranch_upd { get; set; }
        [ScriptIgnore]
        public string AgentContact_upd { get; set; }
        [ScriptIgnore]
        public string BankAccountName_upd { get; set; }
        [ScriptIgnore]
        public string BankBIC_upd { get; set; }
        [ScriptIgnore]
        public string BankIBAN_upd { get; set; }
        [ScriptIgnore]
        public string BankName_upd { get; set; }
        [ScriptIgnore]
        public string BankAddress_upd { get; set; }
        [ScriptIgnore]
        public bool? Address_Update_Pending { get; set; }
        [ScriptIgnore]
        public bool? Agent_Update_Pending { get; set; }
        [ScriptIgnore]
        public bool? Bank_Update_Pending { get; set; }
        [ScriptIgnore]
        public string Currency { get; set; }

        [JsonIgnore, ScriptIgnore]
        public string MobileVerificationToken { get; set; }

        [JsonIgnore, ScriptIgnore]
        public string EmailVerificationToken { get; set; }

        [ScriptIgnore]
        public string DisplayName
        {
            get
            {
                string displayName = string.Empty;
                if (!string.IsNullOrWhiteSpace(FirstName) && !string.IsNullOrWhiteSpace(LastName))
                {
                    displayName = Join(" ", new string[] { FirstName, LastName });
                    if (!string.IsNullOrWhiteSpace(FirstName2) && !string.IsNullOrWhiteSpace(LastName2))
                    {
                        displayName = Join(" & ", new string[] { displayName, Join(" ", new string[] { FirstName2, LastName2 }) });
                    }
                }
                else
                {
                    displayName = Join(" ", new string[] { ContactFirstName, ContactLastName });
                }
                return displayName;
            }
        }

        [ScriptIgnore]
        public string DisplayFormalName
        {
            get
            {
                string displayFormalName = string.Empty;
                if (!string.IsNullOrWhiteSpace(Title) && !string.IsNullOrWhiteSpace(LastName))
                {
                    displayFormalName = Join(" ", new string[] { Title, LastName });
                    if (!string.IsNullOrWhiteSpace(Title2) && !string.IsNullOrWhiteSpace(LastName2))
                    {
                        displayFormalName = Join(" & ", new string[] { displayFormalName, Join(" ", new string[] { Title2, LastName2 }) });
                    }
                }
                else
                {
                    displayFormalName = Join(" ", new string[] { ContactTitle, ContactLastName });
                }
                return displayFormalName;
            }
        }

        [ScriptIgnore]
        public string FullAddress
        {
            get
            {
                return Join(", ", new string[] { Address1, Address2, Town_City, County_ZipCode, Postcode, Address_Country });
            }
        }

        [ScriptIgnore]
        public string FullAddressUpdate
        {
            get
            {
                return Join(", ", new string[] { Address1_upd, Address2_upd, TownCity_upd, County_upd, Postcode_upd, Address_Country });
            }
        }

        [ScriptIgnore]
        public string FullBankDetails
        {
            get
            {
                return Join(", ", new string[] { BankAccountName, BankBIC, BankIBAN, BankName, BankAddress });
            }
        }

        [ScriptIgnore]
        public string FullBankDetailsUpdate
        {
            get
            {
                return Join(", ", new string[] { BankAccountName_upd, BankBIC_upd, BankIBAN_upd, BankName_upd, BankAddress_upd });
            }
        }

        [ScriptIgnore]
        public string CurrencySymbol
        {
            get
            {
                string currency = Currency ?? "EUR";
                string symbol = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures)
                    .Where(c => !c.IsNeutralCulture)
                    .Select(c =>
                    {
                        try { return new System.Globalization.RegionInfo(c.LCID); }
                        catch { return null; }
                    })
                    .Where(ri => ri != null && ri.ISOCurrencySymbol == currency)
                    .Select(ri => ri.CurrencySymbol)
                    .FirstOrDefault();
                return symbol ?? "€";
            }
        }

        [ScriptIgnore]
        public string Person1Name
        {
            get
            {
                string person1Name = string.Empty;
                if (!string.IsNullOrWhiteSpace(FirstName) && !string.IsNullOrWhiteSpace(LastName))
                {
                    person1Name = Join(" ", new string[] { FirstName, LastName });
                }
                else
                {
                    person1Name = Join(" ", new string[] { ContactFirstName, ContactLastName });
                }
                return person1Name;
            }
        }

        [ScriptIgnore]
        public string Person1FormalName
        {
            get
            {
                string person1Name = string.Empty;
                if (!string.IsNullOrWhiteSpace(Title) && !string.IsNullOrWhiteSpace(FirstName) && !string.IsNullOrWhiteSpace(LastName))
                {
                    person1Name = Join(" ", new string[] { Title, FirstName, LastName });
                }
                else
                {
                    person1Name = Join(" ", new string[] { ContactTitle, ContactFirstName, ContactLastName });
                }
                return person1Name;
            }
        }

        [ScriptIgnore]
        public string Person2Name
        {
            get
            {
                return Join(" ", new string[] { FirstName2, LastName2 });
            }
        }

        [ScriptIgnore]
        public string Person2FormalName
        {
            get
            {
                return Join(" ", new string[] { Title2, FirstName2, LastName2 });
            }
        }

        private string Join(string separator, string[] values)
        {
            return string.Join(separator, values.Where(w => !string.IsNullOrWhiteSpace(w))).Trim();
        }
        public string Username { get; set; }

        [ScriptIgnore]
        public bool IsIndividualClient { get { return InvestorRegistrationType.Individual.ClientType.Equals(ClientType, StringComparison.OrdinalIgnoreCase); } }
        [ScriptIgnore]
        public bool IsJointClient { get { return InvestorRegistrationType.Joint.ClientType.Equals(ClientType, StringComparison.OrdinalIgnoreCase); } }
        [ScriptIgnore]
        public bool IsNonIndividualClient { get { return InvestorRegistrationType.NonIndividual.ClientType.Equals(ClientType, StringComparison.OrdinalIgnoreCase); } }

        //[ScriptIgnore]
        //public bool IsCorporate
        //{
        //    get
        //    {
        //        return IsNonIndividualClient && NonIndividualRegistrationTypeGroup.CorporateInvestor.Equals(NonIndividualRegistrationTypeGroup.GetBySubType(InvestmentType));
        //    }
        //}
    }
}
