﻿using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BCPApi.UserAuth.Controllers
{
    public class ForgotpasswordController : ApiController
    {
        // GET: api/Forgotpassword
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Forgotpassword/5
        public string Get(int id)
        {
            return "value";
        }

        IForgotpwdRepository forgotpwdservice;

        public ForgotpasswordController(IForgotpwdRepository forgotpwd)
        {
            forgotpwdservice = forgotpwd;
        }

        // POST: api/Forgotpassword
        public IHttpActionResult Post(string username)
        {
            if (username == null || !ModelState.IsValid)
            {
                return BadRequest("Invalid!");
            }
            forgotpwdservice.SendResetPasswordEmail(username);
            return Ok("Reset password email has been sent successfully!");
        }

        // PUT: api/Forgotpassword/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Forgotpassword/5
        public void Delete(int id)
        {
        }
    }
}
