﻿using BCPApi.Database;
using BCPApi.ServiceRepository.DAO;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dependencies;
using Unity;
using Unity.Injection;

namespace BCPVesproLoginApi
{

    public static class UnityConfiguration
    {
        public static IUnityContainer Config()
        {
            IUnityContainer container = new UnityContainer();
            //container.RegisterType<IMyService, Myservice>();
            //container.RegisterType<IGenericRepository, GenericRepository>();
            //container.RegisterType<DbContext, MyEntities>();
            container.RegisterType<UserAuthRepository>(new InjectionConstructor(new BaseDbContext()));
            container.RegisterType<IUserAuthRepository, UserAuthRepository>();

            container.RegisterType<TokenServiceRepository>(new InjectionConstructor(new BaseDbContext()));
            container.RegisterType<ITokenServiceRepository, TokenServiceRepository>();

            //container.RegisterType<EmailServiceRepository>(new InjectionConstructor(new BCPApi.Database.BaseDbContext()));
            container.RegisterType<IEmailServiceRepository, EmailServiceRepository>();

            //container.RegisterType<SmsServiceRepository>(new InjectionConstructor(new BCPApi.Database.BaseDbContext()));
            container.RegisterType<ISmsServiceRepository, SmsServiceRepository>();

            //InjectionMember[] param = { new InjectionConstructor(new BaseDbContext(), new Lazy<ITokenServiceRepository>(), new Lazy<IUserAuthRepository>(), new Lazy<IEmailServiceRepository>(), new Lazy<ISmsServiceRepository>()) };
            //container.RegisterType<ForgotpwdRepository>(param);
            container.RegisterType<ForgotpwdRepository>(new InjectionConstructor(new BaseDbContext()));
            container.RegisterType<IForgotpwdRepository, ForgotpwdRepository>();

            container.RegisterType<AssetRepository>(new InjectionConstructor(new BaseDbContext()));
            container.RegisterType<IAssetRepository, AssetRepository>();

            container.RegisterType<DocumentRepository>(new InjectionConstructor(new BaseDbContext()));
            container.RegisterType<IDocumentRepository, DocumentRepository>();

            container.RegisterType<InvestmentRepository>(
                new InjectionConstructor(
                    new BCPApi.Database.BaseDbContext(),
                    new Lazy<IUserAuthRepository>(() => new UserAuthRepository(
                        new BaseDbContext())
                        ),
                    new Lazy<ITokenServiceRepository>(() => new TokenServiceRepository(
                        new BaseDbContext())
                        ),
                    new Lazy<IEmailServiceRepository>(() => new EmailServiceRepository()),
                    new Lazy<ISmsServiceRepository>(() => new SmsServiceRepository()),
                    new Lazy<IInvestorTypeRepository>(() => new InvestorTypeRepository(
                        new BaseDbContext())
                        ),
                    new Lazy<IDocumentRepository>(() => new DocumentRepository(
                        new BaseDbContext())
                        )
                    )
                );
            container.RegisterType<IInvestmentRepository, InvestmentRepository>();

            container.RegisterType<IndividualInvestorRepository>(
                new InjectionConstructor(
                    new BCPApi.Database.BaseDbContext(),
                    new Lazy<IInvestmentRepository>(() => new InvestmentRepository(
                       new BCPApi.Database.BaseDbContext(),
                       new Lazy<IUserAuthRepository>(() => new UserAuthRepository(
                           new BaseDbContext())
                           ),
                       new Lazy<ITokenServiceRepository>(() => new TokenServiceRepository(
                           new BaseDbContext())
                           ),
                       new Lazy<IEmailServiceRepository>(() => new EmailServiceRepository()),
                       new Lazy<ISmsServiceRepository>(() => new SmsServiceRepository()),
                       new Lazy<IInvestorTypeRepository>(() => new InvestorTypeRepository(
                           new BaseDbContext())
                           ),
                       new Lazy<IDocumentRepository>(() => new DocumentRepository(
                           new BaseDbContext()))
                           )
                    ),
                    new Lazy<ITokenServiceRepository>(() => new TokenServiceRepository(
                       new BaseDbContext())
                        ),
                    new Lazy<IEmailServiceRepository>(() => new EmailServiceRepository()),
                    new Lazy<ISmsServiceRepository>(() => new SmsServiceRepository())
                    )
                );
            container.RegisterType<IIndividualInvestorRepository, IndividualInvestorRepository>();

            container.RegisterType<InvestorTypeRepository>(
                new InjectionConstructor(
                    new BCPApi.Database.BaseDbContext()
                    )
                );
            container.RegisterType<IInvestorTypeRepository, InvestorTypeRepository>();

            container.RegisterType<NonIndividualInvestorRepository>(
                new InjectionConstructor(
                    new BCPApi.Database.BaseDbContext(),
                    new Lazy<ITokenServiceRepository>(() => new TokenServiceRepository(
                       new BaseDbContext())),
                    new Lazy<IEmailServiceRepository>(() => new EmailServiceRepository()),
                    new Lazy<ISmsServiceRepository>(() => new SmsServiceRepository())
                    )
                );
            container.RegisterType<INonIndividualInvestorRepository, NonIndividualInvestorRepository>();

            // return the container so it can be used for the dependencyresolver.  
            return container;
        }
    }

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Register Unity with Web API.
            var container = UnityConfiguration.Config();
            config.DependencyResolver = new UnityResolver(container);
        }

        public class UnityResolver : IDependencyResolver
        {
            protected IUnityContainer container;

            public UnityResolver(IUnityContainer container)
            {
                if (container == null)
                {
                    throw new ArgumentNullException("container");
                }
                this.container = container;
            }

            public object GetService(Type serviceType)
            {
                try
                {
                    return container.Resolve(serviceType);
                }
                catch (ResolutionFailedException)
                {
                    return null;
                }
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                try
                {
                    return container.ResolveAll(serviceType);
                }
                catch (ResolutionFailedException)
                {
                    return new List<object>();
                }
            }

            public IDependencyScope BeginScope()
            {
                var child = container.CreateChildContainer();
                return new UnityResolver(child);
            }

            public void Dispose()
            {
                container.Dispose();
            }
        }
    }
}
