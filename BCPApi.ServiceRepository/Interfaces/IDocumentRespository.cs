﻿using BCPApi.Entities.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IDocumentRepository
    {
        //IList<DocumentBO> GetAgentDocuments(string accountNumber, string clientAccountNumber, string docType, bool? readStatus);
        IList<DocumentBO> GetClientDocuments(int clientId, string docType, bool? readStatus);
        //DocumentVO InsertDocument(DocumentVO document);
        //void DeleteDocument(int id);
        //void UpdateDocument(DocumentVO document);
        void MarkDocumentReadByAgent(int documentId, int agentId);
        void MarkDocumentReadByClient(int documentId);
        byte[] DownloadDocument(DocumentBO file, bool isClient, int? agentId);
        void UploadDocument(int clientId, string username, string docType, string details, HttpPostedFileWrapper file);
        void UploadClientDocument(int? clientId, string username, string docType, string details, HttpPostedFileWrapper file, string clientName, string agentCode);
        DocumentBO UploadS3Document(int clientId, string username, string docType, string details, string docName, string docPath, string clientName);
        void ValidateFileBeforeUpload(HttpPostedFileWrapper file, int? clientId, string docName, string username, string agentCode);
        byte[] DownloadS3Document(string path);
    }
}
