﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class AttachmentBO
    {
        public string FileName { get; set; }
        public byte[] FileData { get; set; }
    }
}
