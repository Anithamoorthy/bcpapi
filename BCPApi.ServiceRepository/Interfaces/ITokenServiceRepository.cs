﻿using BCPApi.Entities.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface ITokenServiceRepository
    {
        void AddToken(TokenBO tokenVO);
        void UpdateToken(TokenBO tokenVO);
        void DeleteToken(int tokenId);
        void CheckToken(string tokenCode, string acNum, int webClientNumber, short tokenType);
        TokenBO GetTokenDetails(string tokenCode, short tokenTypeId);
    }
}
