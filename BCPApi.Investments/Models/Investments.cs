﻿using BCPApi.Investments.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BCPApi.Investments.Models
{
    public class InvestmentBO
    {
        public string Sector { get; set; }
        public int Client_ID_Number { get; set; }
        public string Stock_Name { get; set; }
        public DateTime? Date_Acquired { get; set; }
        public string Currency { get; set; }
        public double Quantity1 { get; set; }
        public double ValPrice { get; set; }
        public decimal ValValue1 { get; set; }
        public double ValValueAlt { get; set; }
        public DateTime? ValDate { get; set; }
        public decimal BuyVal1 { get; set; }
        public double BuyValAlt { get; set; }
        public int Holding_ID { get; set; }
        public int StockID { get; set; }
        public double CurrentPrice { get; set; }
        public double CurrentVal { get; set; }
        public decimal PercentageChange { get; set; }
        public decimal Amount_Change { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? Date_Last_Updated { get; set; }
        public string Last_Updated_By { get; set; }
        public string StockCurrency { get; set; }
        public double StockPrice { get; set; }
        public double ExchangeRate { get; set; }
        public DateTime? Date_Updated { get; set; }
        public DateTime? StockPriceDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string MasterHoldingName { get; set; }
        public double? AssetPerformance { get; set; }
        public string KickOut { get; set; }
        public int KickOutStock { get; set; }
        public bool ELGStock { get; set; }
        public IList<AssetClassList> AssetClasses { get; set; }
        [ScriptIgnore]
        public string CurrencySymbol
        {
            get
            {
                string currency = Currency ?? "EUR";
                string symbol = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures)
                    .Where(c => !c.IsNeutralCulture)
                    .Select(c =>
                    {
                        try { return new System.Globalization.RegionInfo(c.LCID); }
                        catch { return null; }
                    })
                    .Where(ri => ri != null && ri.ISOCurrencySymbol == currency)
                    .Select(ri => ri.CurrencySymbol)
                    .FirstOrDefault();
                return symbol ?? "€";
            }
        }
    }
    public class Investment : IInvestment
    {
        public List<InvestmentBO> GetClientInvestments(int clientNumber)
        {
            List<InvestmentBO> investments = new List<InvestmentBO>();
            InvestmentBO investment = new InvestmentBO();
            try
            {
                string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCPConnection"].ToString();
                using (SqlConnection ConLogin = new SqlConnection(constr))
                {
                    ConLogin.Open();
                    using (SqlCommand CmdLogin = new SqlCommand("[CRM_Get_Client_Holdings]", ConLogin))
                    {
                        CmdLogin.Parameters.AddWithValue("@ClientIDNumberIn", clientNumber);
                        CmdLogin.CommandType = CommandType.StoredProcedure;
                        using (var reader = CmdLogin.ExecuteReader())
                        {
                            while (reader.HasRows & reader.Read())
                            {
                                investment = new InvestmentBO();
                                investment.Sector = reader[0] as string;
                                investment.Client_ID_Number = reader[1] as int? ?? default(int);
                                investment.Stock_Name = reader[2] as string;
                                investment.Date_Acquired = reader[3] as DateTime? ?? default(DateTime);
                                investment.Currency = reader[4] as string;
                                investment.Quantity1 = reader[5] as double? ?? default(double);
                                investment.ValPrice = reader[6] as double? ?? default(double);
                                investment.ValValue1 = reader[7] as decimal? ?? default(decimal);
                                investment.ValValueAlt = reader[8] as double? ?? default(double);
                                investment.ValDate = reader[9] as DateTime? ?? default(DateTime);
                                investment.BuyVal1 = reader[10] as decimal? ?? default(decimal);
                                investment.BuyValAlt = reader[11] as double? ?? default(double);
                                investment.Holding_ID = reader[12] as int? ?? default(int);
                                investment.StockID = reader[13] as int? ?? default(int);
                                investment.CurrentPrice = reader[14] as double? ?? default(double);
                                investment.CurrentVal = reader[15] as double? ?? default(double);
                                investment.PercentageChange = reader[16] as decimal? ?? default(decimal);
                                investment.Amount_Change = reader[17] as decimal? ?? default(decimal);
                                investment.PolicyNumber = reader[18] as string;
                                investment.Date_Last_Updated = reader[19] as DateTime? ?? default(DateTime);
                                investment.Last_Updated_By = reader[20] as string;
                                investment.StockCurrency = reader[21] as string;
                                investment.StockPrice = reader[22] as double? ?? default(double);
                                investment.ExchangeRate = reader[23] as double? ?? default(double);
                                investment.Date_Updated = reader[24] as DateTime? ?? default(DateTime);
                                investment.StockPriceDate = reader[25] as DateTime? ?? default(DateTime);
                                investment.MaturityDate = reader[26] as DateTime? ?? default(DateTime);
                                investment.MasterHoldingName = reader[27] as string;
                                investment.AssetPerformance= reader[28] as double? ?? default(double);
                                investment.KickOut = reader[29] as string;
                                investment.KickOutStock = reader[30] as int? ?? default(int);
                                investment.ELGStock = reader[31] as bool? ?? default(bool);
                                investment.AssetClasses = GetAssetClasses(investment.StockID);
                                investments.Add(investment);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return investments;
        }

        //public T CheckNull<T>(object obj)
        //{
        //    return (obj == DBNull.Value ? default(T) : (T)obj);
        //}

        public List<AssetClassList> GetAssetClasses(int stockId)
        {
            List<AssetClassList> assetClasses = new List<AssetClassList>();
            //SqlParameter Stock_ID = CreateSqlParameter("Stock_ID", stockId, SqlDbType.Int);
            //return Db.DataBase.SqlQuery<AssetClassVO>("exec CRM_Get_Stock_AssetClasses @Stock_ID", Stock_ID).ToList();
            string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCPConnection"].ToString();
            using (SqlConnection ConLogin = new SqlConnection(constr))
            {
                ConLogin.Open();
                using (SqlCommand CmdLogin = new SqlCommand("[CRM_Get_Stock_AssetClasses]", ConLogin))
                {
                    CmdLogin.Parameters.AddWithValue("@StockIDIn", stockId);
                    CmdLogin.CommandType = CommandType.StoredProcedure;
                    using (var reader = CmdLogin.ExecuteReader())
                    {
                        if (reader.HasRows & reader.Read())
                        {
                            AssetClassList asset = new AssetClassList();
                            asset.StockID = reader[0] as int? ?? default(int);
                            asset.StockACBID = reader[1] as int? ?? default(int);
                            asset.AssetClass = reader[2] as string;
                            asset.Allocation = reader[3] as double? ?? default(double);
                            assetClasses.Add(asset);
                        }
                    }
                }
            }
            return assetClasses;
        }
    }
}
