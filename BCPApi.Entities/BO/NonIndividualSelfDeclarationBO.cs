﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class NonIndividualSelfDeclarationBO
    {
        public int WebClientNISDID { get; set; }
        public int Client_ID_Number { get; set; }
        public DateTime DateRecordAdded { get; set; }
        public bool DeclarationInProgress { get; set; }
        public bool DeclarationComplete { get; set; }
        public bool EntityNotUSPerson { get; set; }
        public bool EntityIsFATCAFI { get; set; }
        public bool FATCAFI_IrishFI { get; set; }
        public bool FATCAFI_RDCFFI { get; set; }
        public bool FATCAFI_PFFI { get; set; }
        public bool RegdAsFinInstorSE { get; set; }
        public string EntityGIIN { get; set; }
        public bool NoGIIN_GIINAppliedFor { get; set; }
        public bool NoGIIN_ExBO { get; set; }
        public bool NoGIIN_ODCFI { get; set; }
        public bool NoGIIN_NPFFI { get; set; }
        public bool NoGIIN_ExemptFI { get; set; }
        public string NoGIIN_OtherReason { get; set; }
        public bool NotFATCAFI_ActiveNFFE { get; set; }
        public bool NotFATCAFI_PassiveNFFE { get; set; }
        public bool EntityIsCRSFI { get; set; }
        public bool CRSFI_StdFI { get; set; }
        public bool CRSFI_InvEntity { get; set; }
        public bool NotCRSFI_ActiveNFE { get; set; }
        public bool NotCRSFI_PassiveNFE { get; set; }
        public bool NonProfitOrg { get; set; }

        public IList<NonIndividualSelfDeclarationPersonBO> Persons { get; set; }
    }
}
