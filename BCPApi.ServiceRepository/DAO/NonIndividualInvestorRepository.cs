﻿using BCPApi.Database;
using BCPApi.Entities.BO;
using BCPApi.Entities.BO.Registration;
using BCPApi.Entities.Data;
using BCPApi.Entities.Domain;
using BCPApi.Entities.Exceptions;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class NonIndividualInvestorRepository : BaseRegistrationService, INonIndividualInvestorRepository
    {
        protected IBaseDbContext Db { get; set; }
        private Lazy<ITokenServiceRepository> tokenService;
        public ITokenServiceRepository TokenService { get { return tokenService.Value; } }
        private Lazy<IEmailServiceRepository> emailService;
        public IEmailServiceRepository EmailService { get { return emailService.Value; } }
        private Lazy<ISmsServiceRepository> smsService;
        public ISmsServiceRepository SmsService { get { return smsService.Value; } }
        //private Lazy<IAuditTrailService> auditTrailService;
        //public IAuditTrailService AuditTrailService { get { return auditTrailService.Value; } }

        public NonIndividualInvestorRepository(IBaseDbContext db, Lazy<ITokenServiceRepository> tokenService, Lazy<IEmailServiceRepository> emailService, Lazy<ISmsServiceRepository> smsService)//, Lazy<IAuditTrailService> auditTrailService)
        {
            Db = db;
            this.tokenService = tokenService;
            this.emailService = emailService;
            this.smsService = smsService;
            //this.auditTrailService = auditTrailService;
        }

        public RegistrationNonIndividualFieldsBO GetRegistrationFields()
        {
            return Db.DataBase.SqlQuery<RegistrationNonIndividualFieldsBO>("exec CRM_Get_Web_Registration_Fields_NonInd").FirstOrDefault();
        }

        public RegistrationNonIndividualBO GetRegistration(int webClientIDNumber)
        {
            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", webClientIDNumber, SqlDbType.Int);

            RegistrationNonIndividualBO investor = Db.DataBase.SqlQuery<RegistrationNonIndividualBO>("exec CRM_Get_Registration_NonInd @WebClientIDNumber", WebClientIDNumber).FirstOrDefault();
            investor.WebClientIDNumber = webClientIDNumber;
            return investor;
        }

        private void ValidateNonIndividualRegistration(RegistrationNonIndividualBO investor)
        {
            investor.Assert("Investor Type is required.", i => !string.IsNullOrWhiteSpace(i.InvestorTypeEntered) && !string.IsNullOrWhiteSpace(i.InvestorSubTypeEntered));
            if (!investor.NoAgent)
            {
                if (investor.RegistrationInProgress && !investor.RegistrationComplete)
                {
                    investor.Assert("Advisor First Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentFirstName));
                    investor.Assert("Advisor Last Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentLastName));
                    investor.Assert("Advisor Company Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentName));
                    //investor.Assert("Advisor Address Line 1 is required.", i => !string.IsNullOrWhiteSpace(i.AgentAddress1));
                    investor.Assert("Advisor City is required.", i => !string.IsNullOrWhiteSpace(i.AgentCity));
                    investor.Assert("Advisor County is required.", i => !string.IsNullOrWhiteSpace(i.AgentCounty));

                    investor.AgentContact = string.Join(" ", new string[] { investor.AgentFirstName, investor.AgentLastName });
                    investor.AgentAddress = string.Join("\r\n", new string[] { investor.AgentAddress1, investor.AgentAddress2, investor.AgentCity, investor.AgentCounty, investor.AgentPostcode }.Where(a => !string.IsNullOrWhiteSpace(a)));
                }
                investor.Assert("Advisor Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentContact));
                investor.Assert("Advisor Company Name is required.", i => !string.IsNullOrWhiteSpace(i.AgentName));
                investor.Assert("Advisor Adress is required.", i => !string.IsNullOrWhiteSpace(i.AgentAddress));
            }
            investor.Assert("Contact 1 Title is required.", i => !string.IsNullOrWhiteSpace(i.ContactTitle));
            investor.Assert("Contact 1 First Name is required.", i => !string.IsNullOrWhiteSpace(i.ContactFirstName));
            investor.Assert("Contact 1 Last Name is required.", i => !string.IsNullOrWhiteSpace(i.ContactLastName));

            investor.Assert("Contact 2 Title is required.", i => !string.IsNullOrWhiteSpace(i.ContactTitle2));
            investor.Assert("Contact 2 First Name is required.", i => !string.IsNullOrWhiteSpace(i.ContactFirstName2));
            investor.Assert("Contact 2 Last Name is required.", i => !string.IsNullOrWhiteSpace(i.ContactLastName2));

            investor.Assert("Email is required.", i => !string.IsNullOrWhiteSpace(i.Onlineemail));
            investor.Assert("Mobile is required.", i => !string.IsNullOrWhiteSpace(i.Mobile));
            investor.Assert("Address Line 1 is required.", i => !string.IsNullOrWhiteSpace(i.Address1));
            investor.Assert("City is required.", i => !string.IsNullOrWhiteSpace(i.Town_City));
            investor.Assert("County is required.", i => !string.IsNullOrWhiteSpace(i.County_ZipCode));
            investor.Assert("Country is required.", i => !string.IsNullOrWhiteSpace(i.Address_Country));
        }

        public RegistrationNonIndividualBO AddNonIndividualRegistration(RegistrationNonIndividualBO investor)
        {
            ValidateNonIndividualRegistration(investor);

            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", DateTime.Now, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", investor.RegistrationInProgress, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", false, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Company_Name = CreateSqlParameter("Company_Name", investor.Company_Name, SqlDbType.Text);
            SqlParameter ContactTitle = CreateSqlParameter("ContactTitle", investor.ContactTitle, SqlDbType.Text);
            SqlParameter ContactFirstName = CreateSqlParameter("ContactFirstName", investor.ContactFirstName, SqlDbType.Text);
            SqlParameter ContactLastName = CreateSqlParameter("ContactLastName", investor.ContactLastName, SqlDbType.Text);
            SqlParameter ContactTitle2 = CreateSqlParameter("ContactTitle2", investor.ContactTitle2, SqlDbType.Text);
            SqlParameter ContactFirstName2 = CreateSqlParameter("ContactFirstName2", investor.ContactFirstName2, SqlDbType.Text);
            SqlParameter ContactLastName2 = CreateSqlParameter("ContactLastName2", investor.ContactLastName2, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);

            investor.WebClientIDNumber = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_Registration_NonInd @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, @InvestorTypeEntered, " +
                "@InvestorSubTypeEntered, @Company_Name, @ContactTitle, @ContactFirstName, @ContactLastName, @ContactTitle2, @ContactFirstName2, @ContactLastName2, @Address1, @Address2, @Address3, " +
                "@Town_City, @County_ZipCode, @Postcode, @Address_Country, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, @OnlineDateSetUp, @OnlineDateRegistered",
                DateRecordAdded, RegistrationInProgress, RegistrationComplete, InvestorTypeEntered, InvestorSubTypeEntered, Company_Name, ContactTitle, ContactFirstName, ContactLastName, ContactTitle2,
                ContactFirstName2, ContactLastName2, Address1, Address2, Address3, Town_City, County_ZipCode, Postcode, Address_Country, Phone, Mobile, Onlineemail, AgentContact, AgentName,
                AgentAddress, NoAgent, OnlineDateSetUp, OnlineDateRegistered).FirstOrDefault();
            return investor;
        }

        public RegistrationNonIndividualBO SaveRegistrationAndSendNotification(RegistrationNonIndividualBO investor)
        {
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            RegistrationNonIndividualBO registrationNonIndividual = this.AddNonIndividualRegistration(investor);
            try
            {
                SendVerificationEmail(registrationNonIndividual);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return registrationNonIndividual;
        }

        public RegistrationNonIndividualBO ContinueRegistration(string notificationCode)
        {
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            TokenBO tokenDetails = TokenService.GetTokenDetails(notificationCode, NotificationTokenType.NonIndividualRegistrationEmailVerification.Id);
            RegistrationNonIndividualBO investor = this.GetRegistration(tokenDetails.WebClientIDNumber);
            investor.Assert("Investor registration already completed or not found.", i => i != null);
            return investor;
        }

        private RegistrationNonIndividualBO UpdateNonIndividualRegistration(RegistrationNonIndividualBO investor)
        {
            ValidateNonIndividualRegistration(investor);

            SqlParameter WebClientIDNumber = CreateSqlParameter("WebClientIDNumber", investor.WebClientIDNumber, SqlDbType.Int);
            SqlParameter DateRecordAdded = CreateSqlParameter("DateRecordAdded", investor.DateRecordAdded, SqlDbType.DateTime2);
            SqlParameter RegistrationInProgress = CreateSqlParameter("RegistrationInProgress", investor.RegistrationInProgress, SqlDbType.Bit);
            SqlParameter RegistrationComplete = CreateSqlParameter("RegistrationComplete", false, SqlDbType.Bit);
            SqlParameter InvestorTypeEntered = CreateSqlParameter("InvestorTypeEntered", investor.InvestorTypeEntered, SqlDbType.Text);
            SqlParameter InvestorSubTypeEntered = CreateSqlParameter("InvestorSubTypeEntered", investor.InvestorSubTypeEntered, SqlDbType.Text);
            SqlParameter Company_Name = CreateSqlParameter("Company_Name", investor.Company_Name, SqlDbType.Text);
            SqlParameter ContactTitle = CreateSqlParameter("ContactTitle", investor.ContactTitle, SqlDbType.Text);
            SqlParameter ContactFirstName = CreateSqlParameter("ContactFirstName", investor.ContactFirstName, SqlDbType.Text);
            SqlParameter ContactLastName = CreateSqlParameter("ContactLastName", investor.ContactLastName, SqlDbType.Text);
            SqlParameter ContactTitle2 = CreateSqlParameter("ContactTitle2", investor.ContactTitle2, SqlDbType.Text);
            SqlParameter ContactFirstName2 = CreateSqlParameter("ContactFirstName2", investor.ContactFirstName2, SqlDbType.Text);
            SqlParameter ContactLastName2 = CreateSqlParameter("ContactLastName2", investor.ContactLastName2, SqlDbType.Text);
            SqlParameter Address1 = CreateSqlParameter("Address1", investor.Address1, SqlDbType.Text);
            SqlParameter Address2 = CreateSqlParameter("Address2", investor.Address2, SqlDbType.Text);
            SqlParameter Address3 = CreateSqlParameter("Address3", investor.Address3, SqlDbType.Text);
            SqlParameter Town_City = CreateSqlParameter("Town_City", investor.Town_City, SqlDbType.Text);
            SqlParameter County_ZipCode = CreateSqlParameter("County_ZipCode", investor.County_ZipCode, SqlDbType.Text);
            SqlParameter Postcode = CreateSqlParameter("Postcode", investor.Postcode, SqlDbType.Text);
            SqlParameter Address_Country = CreateSqlParameter("Address_Country", investor.Address_Country, SqlDbType.Text);
            SqlParameter Phone = CreateSqlParameter("Phone", investor.Phone, SqlDbType.Text);
            SqlParameter Mobile = CreateSqlParameter("Mobile", investor.Mobile, SqlDbType.Text);
            SqlParameter Onlineemail = CreateSqlParameter("Onlineemail", investor.Onlineemail, SqlDbType.Text);
            SqlParameter AgentContact = CreateSqlParameter("AgentContact", investor.AgentContact, SqlDbType.Text);
            SqlParameter AgentName = CreateSqlParameter("AgentName", investor.AgentName, SqlDbType.Text);
            SqlParameter AgentAddress = CreateSqlParameter("AgentAddress", investor.AgentAddress, SqlDbType.Text);
            SqlParameter NoAgent = CreateSqlParameter("NoAgent", investor.NoAgent, SqlDbType.Bit);
            SqlParameter OnlineDateSetUp = CreateSqlParameter("OnlineDateSetUp", investor.OnlineDateSetUp, SqlDbType.DateTime2);
            SqlParameter OnlineDateRegistered = CreateSqlParameter("OnlineDateRegistered", investor.OnlineDateRegistered, SqlDbType.DateTime2);



            string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_Registration_NonInd @WebClientIDNumber, @DateRecordAdded, @RegistrationInProgress, @RegistrationComplete, " +
                "@InvestorTypeEntered, @InvestorSubTypeEntered, @Company_Name, @ContactTitle, @ContactFirstName, @ContactLastName, @ContactTitle2, @ContactFirstName2, @ContactLastName2, " +
                "@Address1, @Address2, @Address3, @Town_City, @County_ZipCode, @Postcode, @Address_Country, @Phone, @Mobile, @Onlineemail, @AgentContact, @AgentName, @AgentAddress, @NoAgent, " +
                "@OnlineDateSetUp, @OnlineDateRegistered", WebClientIDNumber, DateRecordAdded, RegistrationInProgress, RegistrationComplete, InvestorTypeEntered, InvestorSubTypeEntered, Company_Name,
                ContactTitle, ContactFirstName, ContactLastName, ContactTitle2, ContactFirstName2, ContactLastName2, Address1, Address2, Address3, Town_City, County_ZipCode, Postcode, Address_Country,
                Phone, Mobile, Onlineemail, AgentContact, AgentName, AgentAddress, NoAgent, OnlineDateSetUp, OnlineDateRegistered).FirstOrDefault();
            response.Assert(response, r => "OK".Equals(r, StringComparison.OrdinalIgnoreCase));
            return investor;
        }

        public void SendRegistration2FactorCode(int webClientIDNumber, string phone)
        {
            phone.Assert("Phone is required.", p => !string.IsNullOrWhiteSpace(p));
            //DbContextTransaction transaction = Db.DataBase.BeginTransaction();
            RegistrationNonIndividualBO investor = this.GetRegistration(webClientIDNumber);
            if (!phone.Equals(investor.Mobile))
            {
                investor.Mobile = phone;
                UpdateNonIndividualRegistration(investor);
            }
            try
            {
                DateTime now = DateTime.Now;
                TokenBO investorNotification = new TokenBO()
                {
                    WebClientIDNumber = webClientIDNumber,
                    InvestorNotificationType_Id = NotificationTokenType.CompleteRegistrationSmsVerification.Id,
                    Token_Code = TokenBO.GenerateTextMessageHash(),
                    Token_Created_Date = now,
                    Token_Expiry_Date = now.AddHours(1)
                };
                TokenService.AddToken(investorNotification);
                if (!ApplicationSettings.Instance.SendRegistration2FactorCodeByEmail)
                {
                    //send sms
                    SmsService.SendSms(phone, string.Format("Your BCP Vespro verification code is {0}. Please note: This code will expire in 1 hour.", investorNotification.Token_Code));
                    //AuditTrailService.AddAuditTrailRecord(string.Empty, "Non individual registration", "Send two factor authentication code \"" + investorNotification.Token_Code + "\" by sms to: " + phone, NetworkUtils.GetRequestIpAddress(), null, null, string.Empty, NetworkUtils.GetRequestUserAgent());
                }
                else
                {
                    EmailService.SendEmail(new EmailBO()
                    {
                        From = ApplicationSettings.Instance.MailFrom,
                        To = investor.Onlineemail,
                        Subject = "Your BCP Vespro verification code",
                        Content = string.Format("Your BCP Vespro verification code is {0}. Please note: This code will expire in 1 hour.", investorNotification.Token_Code)
                    });
                   // AuditTrailService.AddAuditTrailRecord(string.Empty, "Non individual registration", "Send two factor authentication code \"" + investorNotification.Token_Code + "\" by email to: " + investor.Onlineemail, NetworkUtils.GetRequestIpAddress(), null, null, string.Empty, NetworkUtils.GetRequestUserAgent());
                }
                //transaction.Commit();
            }
            catch (Exception ex)
            {
                //transaction.Rollback();
                throw ex;
            }
        }

        public void SendVerificationEmail(RegistrationNonIndividualBO investor)
        {
            DateTime now = DateTime.Now;
            TokenBO investorNotification = new TokenBO()
            {
                WebClientIDNumber = investor.WebClientIDNumber,
                InvestorNotificationType_Id = NotificationTokenType.NonIndividualRegistrationEmailVerification.Id,
                Token_Code = TokenBO.GenerateEmailHash(),
                Token_Created_Date = now,
                Token_Expiry_Date = now.AddHours(1)
            };
            TokenService.AddToken(investorNotification);
            //string name = ((investor.InvestorTypeGroup_Id == NonIndividualRegistrationTypeGroup.PensionOrPostRetirementInvestor.Id) ? string.Join(" ", investor.ContactTitle, investor.ContactLastName) : string.Join(" & ", string.Join(" ", investor.ContactTitle, investor.ContactLastName), string.Join(" ", investor.ContactTitle2, investor.ContactLastName2)));
            string name = string.Join(" ", investor.ContactTitle, investor.ContactLastName);
            EmailService.SendEmail(new EmailBO()
            {
                From = ApplicationSettings.Instance.MailFrom,
                To = investor.Onlineemail,
                Subject = "BCP Vespro Email Address Validation",
                Content = RegistrationConfirmationEmailContent(name, NonIndividualRegistrationConfirmationLink(investorNotification.Token_Code))
            });
        }

        public NonIndividualSelfDeclarationBO GetLastSelfDeclarationByClient(int webClientIDNumber)
        {
            SqlParameter clientIDParam = CreateSqlParameter("clientIDNumber", webClientIDNumber, SqlDbType.Int);

            NonIndividualSelfDeclarationBO selfDeclaration = Db.DataBase.SqlQuery<NonIndividualSelfDeclarationBO>("exec CRM_Get_NISelfDecs_For_Client @clientIDNumber", clientIDParam)
                .OrderByDescending(sd => sd.DateRecordAdded).FirstOrDefault();

            if (selfDeclaration != null)
            {
                selfDeclaration.Persons = GetSelfDeclarationPersons(selfDeclaration.WebClientNISDID);
            }

            return selfDeclaration;
        }

        public NonIndividualSelfDeclarationBO SaveSelfDeclaration(NonIndividualSelfDeclarationBO selfDeclaration, string username)
        {
            if (selfDeclaration.WebClientNISDID == 0)
                selfDeclaration = AddSelfDeclaration(selfDeclaration, username);
            else
                selfDeclaration = UpdateSelfDeclaration(selfDeclaration, username);
            if (selfDeclaration.Persons != null)
            {
                for (int i = 0; i < selfDeclaration.Persons.Count; i++)
                {
                    if (selfDeclaration.Persons[i].WebClientNISDPersonID == 0)
                        selfDeclaration.Persons[i] = AddSelfDeclarationPerson(selfDeclaration.Persons[i], username);
                    else
                        selfDeclaration.Persons[i] = UpdateSelfDeclarationPerson(selfDeclaration.Persons[i], username);
                    for (int j = 0; j < selfDeclaration.Persons[i].Taxes.Count; i++)
                    {
                        if (selfDeclaration.Persons[i].Taxes[j].WebClientSomething == 0)
                            selfDeclaration.Persons[i].Taxes[j] = AddSelfDeclarationPersonTax(selfDeclaration.Persons[i].Taxes[j], username);
                        else
                            selfDeclaration.Persons[i].Taxes[j] = AddSelfDeclarationPersonTax(selfDeclaration.Persons[i].Taxes[j], username);
                    }
                }
            }
            return GetLastSelfDeclarationByClient(selfDeclaration.Client_ID_Number);
        }

        private IList<NonIndividualSelfDeclarationPersonBO> GetSelfDeclarationPersons(int webClientNISDID)
        {
            SqlParameter webClientNISDIDParam = CreateSqlParameter("NISDID", webClientNISDID, SqlDbType.Int);
            IList<NonIndividualSelfDeclarationPersonBO> persons = Db.DataBase.SqlQuery<NonIndividualSelfDeclarationPersonBO>("exec CRM_Get_NISelfDec_Persons_Records @NISDID", webClientNISDIDParam).ToList();

            foreach (NonIndividualSelfDeclarationPersonBO person in persons)
            {
                person.Taxes = GetSelfDeclarationPersonTaxes(person.WebClientNISDPersonID);
            }

            return persons;
        }

        private IList<NonIndividualSelfDeclarationPersonTaxBO> GetSelfDeclarationPersonTaxes(int webClientNISDPersonID)
        {
            SqlParameter personIDParam = CreateSqlParameter("PersonID", webClientNISDPersonID, SqlDbType.Int);
            return Db.DataBase.SqlQuery<NonIndividualSelfDeclarationPersonTaxBO>("exec CRM_Get_NISelfDec_Person_Tax_Records @PersonID", personIDParam).ToList();
        }

        private NonIndividualSelfDeclarationBO AddSelfDeclaration(NonIndividualSelfDeclarationBO declaration, string username)
        {
            SqlParameter ClientIDNumberParam = CreateSqlParameter("ClientIDNumber", declaration.Client_ID_Number, SqlDbType.Int);
            SqlParameter DeclarationInProgressParam = CreateSqlParameter("DeclarationInProgress", declaration.DeclarationInProgress, SqlDbType.Int);
            SqlParameter DeclarationCompleteParam = CreateSqlParameter("DeclarationComplete", declaration.DeclarationComplete, SqlDbType.Int);
            SqlParameter EntityNotUSPersonParam = CreateSqlParameter("EntityNotUSPerson", declaration.EntityNotUSPerson, SqlDbType.Int);
            SqlParameter EntityIsFATCAFIParam = CreateSqlParameter("EntityIsFATCAFI", declaration.EntityIsFATCAFI, SqlDbType.Int);
            SqlParameter FATCAFIIrishFIParam = CreateSqlParameter("FATCAFI_IrishFI", declaration.FATCAFI_IrishFI, SqlDbType.Int);
            SqlParameter FATCAFIRDCFFIParam = CreateSqlParameter("FATCAFI_RDCFFI", declaration.FATCAFI_RDCFFI, SqlDbType.Int);
            SqlParameter FATCAFIPFFIParam = CreateSqlParameter("FATCAFI_PFFI", declaration.FATCAFI_PFFI, SqlDbType.Int);
            SqlParameter RegdAsFinInstorSEParam = CreateSqlParameter("RegdAsFinInstorSE", declaration.RegdAsFinInstorSE, SqlDbType.Int);
            SqlParameter EntityGIINParam = CreateSqlParameter("EntityGIIN", declaration.EntityGIIN, SqlDbType.Int);
            SqlParameter NoGIINGIINAppliedForParam = CreateSqlParameter("NoGIIN_GIINAppliedFor", declaration.NoGIIN_GIINAppliedFor, SqlDbType.Int);
            SqlParameter NoGIINExBOParam = CreateSqlParameter("NoGIIN_ExBO", declaration.NoGIIN_ExBO, SqlDbType.Int);
            SqlParameter NoGIINODCFIParam = CreateSqlParameter("NoGIIN_ODCFI", declaration.NoGIIN_ODCFI, SqlDbType.Int);
            SqlParameter NoGIINNPFFIParam = CreateSqlParameter("NoGIIN_NPFFI", declaration.NoGIIN_NPFFI, SqlDbType.Int);
            SqlParameter NoGIINExemptFIParam = CreateSqlParameter("NoGIIN_ExemptFI", declaration.NoGIIN_ExemptFI, SqlDbType.Int);
            SqlParameter NoGIINOtherReasonParam = CreateSqlParameter("NoGIIN_OtherReason", declaration.NoGIIN_OtherReason, SqlDbType.Int);
            SqlParameter NotFATCAFIActiveNFFEParam = CreateSqlParameter("NotFATCAFI_ActiveNFFE", declaration.NotFATCAFI_ActiveNFFE, SqlDbType.Int);
            SqlParameter NotFATCAFIPassiveNFFEParam = CreateSqlParameter("NotFATCAFI_PassiveNFFE", declaration.NotFATCAFI_PassiveNFFE, SqlDbType.Int);
            SqlParameter EntityIsCRSFIParam = CreateSqlParameter("EntityIsCRSFI", declaration.EntityIsCRSFI, SqlDbType.Int);
            SqlParameter CRSFIStdFIParam = CreateSqlParameter("CRSFI_StdFI", declaration.CRSFI_StdFI, SqlDbType.Int);
            SqlParameter CRSFIInvEntityParam = CreateSqlParameter("CRSFI_InvEntity", declaration.CRSFI_InvEntity, SqlDbType.Int);
            SqlParameter NotCRSFIActiveNFEParam = CreateSqlParameter("NotCRSFI_ActiveNFE", declaration.NotCRSFI_ActiveNFE, SqlDbType.Int);
            SqlParameter NotCRSFIPassiveNFEParam = CreateSqlParameter("NotCRSFI_PassiveNFE", declaration.NotCRSFI_PassiveNFE, SqlDbType.Int);
            SqlParameter NonProfitOrgParam = CreateSqlParameter("NonProfitOrg", declaration.NonProfitOrg, SqlDbType.Int);
            SqlParameter UsernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            declaration.WebClientNISDID = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_NISelfDec @ClientIDNumber, @DeclarationInProgress, @DeclarationComplete," +
                "@EntityNotUSPerson, @EntityIsFATCAFI, @FATCAFI_IrishFI, @FATCAFI_RDCFFI, @FATCAFI_PFFI, @RegdAsFinInstorSE, @EntityGIIN, @NoGIIN_GIINAppliedFor, @NoGIIN_ExBO, @NoGIIN_ODCFI," +
                "@NoGIIN_NPFFI, @NoGIIN_ExemptFI, @NoGIIN_OtherReason, @NotFATCAFI_ActiveNFFE, @NotFATCAFI_PassiveNFFE, @EntityIsCRSFI, @CRSFI_StdFI, @CRSFI_InvEntity, @NotCRSFI_ActiveNFE," +
                "@NotCRSFI_PassiveNFE, @NonProfitOrg, @username",
                ClientIDNumberParam, DeclarationInProgressParam, DeclarationCompleteParam, EntityNotUSPersonParam, EntityIsFATCAFIParam, FATCAFIIrishFIParam, FATCAFIRDCFFIParam, FATCAFIPFFIParam,
                RegdAsFinInstorSEParam, EntityGIINParam, NoGIINGIINAppliedForParam, NoGIINExBOParam, NoGIINODCFIParam, NoGIINNPFFIParam, NoGIINExemptFIParam, NoGIINOtherReasonParam,
                NotFATCAFIActiveNFFEParam, NotFATCAFIPassiveNFFEParam, EntityIsCRSFIParam, CRSFIStdFIParam, CRSFIInvEntityParam, NotCRSFIActiveNFEParam, NotCRSFIPassiveNFEParam,
                NonProfitOrgParam, UsernameParam).FirstOrDefault();
            return declaration;
        }

        private NonIndividualSelfDeclarationBO UpdateSelfDeclaration(NonIndividualSelfDeclarationBO declaration, string username)
        {
            SqlParameter WebClientNISDIDParam = CreateSqlParameter("WebClientNISDID", declaration.WebClientNISDID, SqlDbType.Int);
            SqlParameter DeclarationInProgressParam = CreateSqlParameter("DeclarationInProgress", declaration.DeclarationInProgress, SqlDbType.Bit);
            SqlParameter DeclarationCompleteParam = CreateSqlParameter("DeclarationComplete", declaration.DeclarationComplete, SqlDbType.Bit);
            SqlParameter EntityNotUSPersonParam = CreateSqlParameter("EntityNotUSPerson", declaration.EntityNotUSPerson, SqlDbType.Bit);
            SqlParameter EntityIsFATCAFIParam = CreateSqlParameter("EntityIsFATCAFI", declaration.EntityIsFATCAFI, SqlDbType.Bit);
            SqlParameter FATCAFIIrishFIParam = CreateSqlParameter("FATCAFI_IrishFI", declaration.FATCAFI_IrishFI, SqlDbType.Bit);
            SqlParameter FATCAFIRDCFFIParam = CreateSqlParameter("FATCAFI_RDCFFI", declaration.FATCAFI_RDCFFI, SqlDbType.Bit);
            SqlParameter FATCAFIPFFIParam = CreateSqlParameter("FATCAFI_PFFI", declaration.FATCAFI_PFFI, SqlDbType.Bit);
            SqlParameter RegdAsFinInstorSEParam = CreateSqlParameter("RegdAsFinInstorSE", declaration.RegdAsFinInstorSE, SqlDbType.Bit);
            SqlParameter EntityGIINParam = CreateSqlParameter("EntityGIIN", declaration.EntityGIIN, SqlDbType.Text);
            SqlParameter NoGIINGIINAppliedForParam = CreateSqlParameter("NoGIIN_GIINAppliedFor", declaration.NoGIIN_GIINAppliedFor, SqlDbType.Bit);
            SqlParameter NoGIINExBOParam = CreateSqlParameter("NoGIIN_ExBO", declaration.NoGIIN_ExBO, SqlDbType.Bit);
            SqlParameter NoGIINODCFIParam = CreateSqlParameter("NoGIIN_ODCFI", declaration.NoGIIN_ODCFI, SqlDbType.Bit);
            SqlParameter NoGIINNPFFIParam = CreateSqlParameter("NoGIIN_NPFFI", declaration.NoGIIN_NPFFI, SqlDbType.Bit);
            SqlParameter NoGIINExemptFIParam = CreateSqlParameter("NoGIIN_ExemptFI", declaration.NoGIIN_ExemptFI, SqlDbType.Bit);
            SqlParameter NoGIINOtherReasonParam = CreateSqlParameter("NoGIIN_OtherReason", declaration.NoGIIN_OtherReason, SqlDbType.Text);
            SqlParameter NotFATCAFIActiveNFFEParam = CreateSqlParameter("NotFATCAFI_ActiveNFFE", declaration.NotFATCAFI_ActiveNFFE, SqlDbType.Bit);
            SqlParameter NotFATCAFIPassiveNFFEParam = CreateSqlParameter("NotFATCAFI_PassiveNFFE", declaration.NotFATCAFI_PassiveNFFE, SqlDbType.Bit);
            SqlParameter EntityIsCRSFIParam = CreateSqlParameter("EntityIsCRSFI", declaration.EntityIsCRSFI, SqlDbType.Bit);
            SqlParameter CRSFIStdFIParam = CreateSqlParameter("CRSFI_StdFI", declaration.CRSFI_StdFI, SqlDbType.Bit);
            SqlParameter CRSFIInvEntityParam = CreateSqlParameter("CRSFI_InvEntity", declaration.CRSFI_InvEntity, SqlDbType.Bit);
            SqlParameter NotCRSFIActiveNFEParam = CreateSqlParameter("NotCRSFI_ActiveNFE", declaration.NotCRSFI_ActiveNFE, SqlDbType.Bit);
            SqlParameter NotCRSFIPassiveNFEParam = CreateSqlParameter("NotCRSFI_PassiveNFE", declaration.NotCRSFI_PassiveNFE, SqlDbType.Bit);
            SqlParameter NonProfitOrgParam = CreateSqlParameter("NonProfitOrg", declaration.NonProfitOrg, SqlDbType.Bit);
            SqlParameter UsernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string resultCode = Db.DataBase.SqlQuery<string>("exec CRM_Update_NISelfDec @WebClientNISDID, @DeclarationInProgress, @DeclarationComplete," +
                "@EntityNotUSPerson, @EntityIsFATCAFI, @FATCAFI_IrishFI, @FATCAFI_RDCFFI, @FATCAFI_PFFI, @RegdAsFinInstorSE, @EntityGIIN, @NoGIIN_GIINAppliedFor, @NoGIIN_ExBO, @NoGIIN_ODCFI," +
                "@NoGIIN_NPFFI, @NoGIIN_ExemptFI, @NoGIIN_OtherReason, @NotFATCAFI_ActiveNFFE, @NotFATCAFI_PassiveNFFE, @EntityIsCRSFI, @CRSFI_StdFI, @CRSFI_InvEntity, @NotCRSFI_ActiveNFE," +
                "@NotCRSFI_PassiveNFE, @NonProfitOrg, @username",
                WebClientNISDIDParam, DeclarationInProgressParam, DeclarationCompleteParam, EntityNotUSPersonParam, EntityIsFATCAFIParam, FATCAFIIrishFIParam, FATCAFIRDCFFIParam, FATCAFIPFFIParam,
                RegdAsFinInstorSEParam, EntityGIINParam, NoGIINGIINAppliedForParam, NoGIINExBOParam, NoGIINODCFIParam, NoGIINNPFFIParam, NoGIINExemptFIParam, NoGIINOtherReasonParam,
                NotFATCAFIActiveNFFEParam, NotFATCAFIPassiveNFFEParam, EntityIsCRSFIParam, CRSFIStdFIParam, CRSFIInvEntityParam, NotCRSFIActiveNFEParam, NotCRSFIPassiveNFEParam,
                NonProfitOrgParam, UsernameParam).FirstOrDefault();
            "OK".Assert("Self declaration could not be updated.", r => r.Equals(resultCode, StringComparison.OrdinalIgnoreCase));
            return declaration;
        }

        private NonIndividualSelfDeclarationPersonBO AddSelfDeclarationPerson(NonIndividualSelfDeclarationPersonBO person, string username)
        {
            SqlParameter WebClientNISDIDParam = CreateSqlParameter("WebClientNISDID", person.WebClientNISDID, SqlDbType.Int);
            SqlParameter ControlByOwnershipParam = CreateSqlParameter("ControlByOwnership", person.ControlByOwnership, SqlDbType.Bit);
            SqlParameter ControlByOtherParam = CreateSqlParameter("ControlByOther", person.ControlByOther, SqlDbType.Bit);
            SqlParameter ControlByManagementParam = CreateSqlParameter("ControlByManagement", person.ControlByManagement, SqlDbType.Bit);
            SqlParameter TitleParam = CreateSqlParameter("Title", person.Title, SqlDbType.Text);
            SqlParameter FirstNameParam = CreateSqlParameter("FirstName", person.FirstName, SqlDbType.Text);
            SqlParameter LastNameParam = CreateSqlParameter("LastName", person.LastName, SqlDbType.Text);
            SqlParameter Address1Param = CreateSqlParameter("Address1", person.Address1, SqlDbType.Text);
            SqlParameter Address2Param = CreateSqlParameter("Address2", person.Address2, SqlDbType.Text);
            SqlParameter Address3Param = CreateSqlParameter("Address3", person.Address3, SqlDbType.Text);
            SqlParameter TownParam = CreateSqlParameter("Town", person.Town, SqlDbType.Text);
            SqlParameter CountyParam = CreateSqlParameter("County", person.County, SqlDbType.Text);
            SqlParameter PostcodeParam = CreateSqlParameter("Postcode", person.Postcode, SqlDbType.Text);
            SqlParameter CountryParam = CreateSqlParameter("Country", person.Country, SqlDbType.Text);
            SqlParameter DateOfBirthParam = CreateSqlParameter("DateOfBirth", person.DateOfBirth, SqlDbType.DateTime);
            SqlParameter PlaceOfBirthParam = CreateSqlParameter("PlaceOfBirth", person.PlaceOfBirth, SqlDbType.Text);
            SqlParameter CountryOfBirthParam = CreateSqlParameter("CountryOfBirth", person.CountryOfBirth, SqlDbType.Text);
            SqlParameter UsernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            person.WebClientNISDPersonID = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_NISelfDec_Person @WebClientNISDID, @ControlByOwnership, @ControlByOther, @ControlByManagement," +
                "@Title, @FirstName, @LastName, @Address1, @Address2, @Address3, @Town, @County, @Postcode, @Country, @DateOfBirth, @PlaceOfBirth, @CountryOfBirth, @username", WebClientNISDIDParam,
                ControlByOwnershipParam, ControlByOtherParam, ControlByManagementParam, TitleParam, FirstNameParam, LastNameParam, Address1Param, Address2Param, Address3Param, TownParam,
                CountyParam, PostcodeParam, CountryParam, DateOfBirthParam, PlaceOfBirthParam, CountryOfBirthParam, username).FirstOrDefault();
            return person;
        }

        private NonIndividualSelfDeclarationPersonBO UpdateSelfDeclarationPerson(NonIndividualSelfDeclarationPersonBO person, string username)
        {
            SqlParameter WebClientNISDPersonIDParam = CreateSqlParameter("WebClientNISDPersonID", person.WebClientNISDPersonID, SqlDbType.Int);
            SqlParameter ControlByOwnershipParam = CreateSqlParameter("ControlByOwnership", person.ControlByOwnership, SqlDbType.Bit);
            SqlParameter ControlByOtherParam = CreateSqlParameter("ControlByOther", person.ControlByOther, SqlDbType.Bit);
            SqlParameter ControlByManagementParam = CreateSqlParameter("ControlByManagement", person.ControlByManagement, SqlDbType.Bit);
            SqlParameter TitleParam = CreateSqlParameter("Title", person.Title, SqlDbType.Text);
            SqlParameter FirstNameParam = CreateSqlParameter("FirstName", person.FirstName, SqlDbType.Text);
            SqlParameter LastNameParam = CreateSqlParameter("LastName", person.LastName, SqlDbType.Text);
            SqlParameter Address1Param = CreateSqlParameter("Address1", person.Address1, SqlDbType.Text);
            SqlParameter Address2Param = CreateSqlParameter("Address2", person.Address2, SqlDbType.Text);
            SqlParameter Address3Param = CreateSqlParameter("Address3", person.Address3, SqlDbType.Text);
            SqlParameter TownParam = CreateSqlParameter("Town", person.Town, SqlDbType.Text);
            SqlParameter CountyParam = CreateSqlParameter("County", person.County, SqlDbType.Text);
            SqlParameter PostcodeParam = CreateSqlParameter("Postcode", person.Postcode, SqlDbType.Text);
            SqlParameter CountryParam = CreateSqlParameter("Country", person.Country, SqlDbType.Text);
            SqlParameter DateOfBirthParam = CreateSqlParameter("DateOfBirth", person.DateOfBirth, SqlDbType.DateTime);
            SqlParameter PlaceOfBirthParam = CreateSqlParameter("PlaceOfBirth", person.PlaceOfBirth, SqlDbType.Text);
            SqlParameter CountryOfBirthParam = CreateSqlParameter("CountryOfBirth", person.CountryOfBirth, SqlDbType.Text);
            SqlParameter UsernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string resultCode = Db.DataBase.SqlQuery<string>("exec CRM_Update_NISelfDec_Person @WebClientNISDPersonID, @ControlByOwnership, @ControlByOther, @ControlByManagement," +
                "@Title, @FirstName, @LastName, @Address1, @Address2, @Address3, @Town, @County, @Postcode, @Country, @DateOfBirth, @PlaceOfBirth, @CountryOfBirth, @username", WebClientNISDPersonIDParam,
                ControlByOwnershipParam, ControlByOtherParam, ControlByManagementParam, TitleParam, FirstNameParam, LastNameParam, Address1Param, Address2Param, Address3Param, TownParam,
                CountyParam, PostcodeParam, CountryParam, DateOfBirthParam, PlaceOfBirthParam, CountryOfBirthParam, UsernameParam).FirstOrDefault();
            "OK".Assert("Self declaration could not be updated.", r => r.Equals(resultCode, StringComparison.OrdinalIgnoreCase));
            return person;
        }

        private NonIndividualSelfDeclarationPersonTaxBO AddSelfDeclarationPersonTax(NonIndividualSelfDeclarationPersonTaxBO tax, string username)
        {
            SqlParameter WebClientNISDPersonIDParam = CreateSqlParameter("WebClientNISDPersonID", tax.WebClientNISDPersonID, SqlDbType.Int);
            SqlParameter TaxCountryParam = CreateSqlParameter("TaxCountry", tax.TaxCountry, SqlDbType.Text);
            SqlParameter TINParam = CreateSqlParameter("TIN", tax.TIN, SqlDbType.Text);
            SqlParameter ReasonForNoTINParam = CreateSqlParameter("ReasonForNoTIN", tax.ReasonForNoTIN, SqlDbType.Text);
            SqlParameter UsernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            tax.WebClientSomething = (int)Db.DataBase.SqlQuery<decimal>("exec CRM_Add_NISelfDec_Person_Tax @WebClientNISDPersonID, @TaxCountry, @TIN, @ReasonForNoTIN, @username", WebClientNISDPersonIDParam,
                TaxCountryParam, TINParam, ReasonForNoTINParam, UsernameParam).FirstOrDefault();
            return tax;
        }

        private NonIndividualSelfDeclarationPersonTaxBO UpdateSelfDeclarationPersonTax(NonIndividualSelfDeclarationPersonTaxBO tax, string username)
        {
            SqlParameter WebClientSomethingParam = CreateSqlParameter("WebClientSomething", tax.WebClientSomething, SqlDbType.Int);
            SqlParameter TaxCountryParam = CreateSqlParameter("TaxCountry", tax.TaxCountry, SqlDbType.Text);
            SqlParameter TINParam = CreateSqlParameter("TIN", tax.TIN, SqlDbType.Text);
            SqlParameter ReasonForNoTINParam = CreateSqlParameter("ReasonForNoTIN", tax.ReasonForNoTIN, SqlDbType.Text);
            SqlParameter UsernameParam = CreateSqlParameter("username", username, SqlDbType.Text);
            string resultCode = Db.DataBase.SqlQuery<string>("exec CRM_Update_NISelfDec_Person_Tax @WebClientSomething, @TaxCountry, @TIN, @ReasonForNoTIN, @username", WebClientSomethingParam,
                TaxCountryParam, TINParam, ReasonForNoTINParam, UsernameParam).FirstOrDefault();
            "OK".Assert("Self declaration could not be updated.", r => r.Equals(resultCode, StringComparison.OrdinalIgnoreCase));
            return tax;
        }

    }
}
