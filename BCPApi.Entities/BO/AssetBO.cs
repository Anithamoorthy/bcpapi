﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class AssetBO
    {
        public int StockID { get; set; }
        public string Stock_Name { get; set; }
        public IList<AssetClassBO> AssetClasses { get; set; }
    }
}
