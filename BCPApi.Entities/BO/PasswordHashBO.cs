﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.BO
{
    public class PasswordHashBO
    {
        public string PW_Salt { get; set; }
        public string AC_Num { get; set; }
    }
}
