﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.Entities.Domain
{
    public class NonIndividualRegistrationTypeGroup
    {
        private static readonly IList<string> corporateInvestor = new List<string>() { "CU Investment", "Corporate Investment", "Charity/Religious" };
        private static readonly IList<string> pensionOrPostRetirementInvestor = new List<string>() { "SSAP", "ARF", "AMRF", "PRB" };

        public short Id { get; private set; }
        public string Name { get; private set; }

        public static readonly NonIndividualRegistrationTypeGroup CorporateInvestor = new NonIndividualRegistrationTypeGroup() { Id = 1, Name = "Corporate Investor" };
        public static readonly NonIndividualRegistrationTypeGroup PensionOrPostRetirementInvestor = new NonIndividualRegistrationTypeGroup() { Id = 2, Name = "Pension or Post Retirement Investor" };

        public static readonly IList<NonIndividualRegistrationTypeGroup> List = new NonIndividualRegistrationTypeGroup[] { CorporateInvestor, PensionOrPostRetirementInvestor }.ToArray<NonIndividualRegistrationTypeGroup>();

        public static NonIndividualRegistrationTypeGroup GetById(short groupId)
        {
            //for (int idx = 0, len = NonIndividualRegistrationTypeGroup.List.Count; idx < len; idx++)
            //{
            //    if (NonIndividualRegistrationTypeGroup.List[idx].Id.Equals(groupId))
            //    {
            //        return NonIndividualRegistrationTypeGroup.List[idx];
            //    }
            //}
            //return null;
            return NonIndividualRegistrationTypeGroup.List.Where(nirtg => nirtg.Id.Equals(groupId)).FirstOrDefault();
        }

        public static NonIndividualRegistrationTypeGroup GetBySubType(string subType)
        {
            if (corporateInvestor.Any(rtg => rtg.Equals(subType, StringComparison.OrdinalIgnoreCase)))
                return NonIndividualRegistrationTypeGroup.CorporateInvestor;
            else if (pensionOrPostRetirementInvestor.Any(rtg => rtg.Equals(subType, StringComparison.OrdinalIgnoreCase)))
                return NonIndividualRegistrationTypeGroup.PensionOrPostRetirementInvestor;
            else
                return null;
        }
    }
}
