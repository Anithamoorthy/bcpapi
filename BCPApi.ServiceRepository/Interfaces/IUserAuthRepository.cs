﻿using BCPApi.Entities;
using BCPApi.Entities.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.Interfaces
{
    public interface IUserAuthRepository
    {
        IList<AuthResponse> CheckUserAuth(string username, string password);
        string UpdatePasswordHash(string accountNumber, string passwordHash);
        List<AuthResponse> CheckUserAuth1(string username, string password);
        string GetPasswordSaltHash(string accountNumber);
        string GeneratePasswordSaltHash(string accountNumber);
        string UpdatePasswordSaltHash(string accountNumber, string passwordSaltHash);
        string GeneratePasswordHashSha1(string password, string passwordSalt);
        string GeneratePasswordHashSha256(string password, string passwordSalt);
        bool UpdateInvestorRecordStats(string username, short? numFailures);
        UserBasicBO GetUserBasicByAccountNumber(string accountNumber);
        ClientDetailsBO GetClientDetailsByAccountNumber(string accountNumber);
    }
}
