﻿using BCPApi.Database;
using BCPApi.Entities;
using BCPApi.Entities.BO;
using BCPApi.Entities.Exceptions;
using BCPApi.ServiceRepository.DAO;
using BCPApi.ServiceRepository.Interfaces;
using BCPApi.ServiceRepository.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCPApi.ServiceRepository.DAO
{
    public class UserAuthRepository : CommonService, IUserAuthRepository
    {
        //protected IBaseDbContext Db  = new BaseDbContext();

        protected IBaseDbContext Db { get; set; }

        public UserAuthRepository(IBaseDbContext db)
        {
            Db = db;
        }

        public IList<AuthResponse> CheckUserAuth(string username, string password)
        {            
            string passwordSaltHash = GetPasswordSaltHash(username), passwordHashSha1 = GeneratePasswordHashSha1(password, passwordSaltHash), passwordHashSha256 = GeneratePasswordHashSha256(password, passwordSaltHash);
            try
            {
                SqlParameter AC_Num = CreateSqlParameter("AC_Num", username.ToUpper(), SqlDbType.Text),
                    PW_Hash = CreateSqlParameter("PW_Hash", passwordHashSha1, SqlDbType.Text);
                IList<AuthResponse> loginCheck = Db.DataBase.SqlQuery<AuthResponse>("exec VeApp_User_Login_Check @AC_Num, @PW_Hash ", AC_Num, PW_Hash).ToList();
                if (loginCheck != null && loginCheck[0].ClientIDNumber != 0)
                {
                    UpdatePasswordHash(username.ToUpper(), passwordHashSha256);
                }
                else
                {
                    AC_Num = CreateSqlParameter("AC_Num", username.ToUpper(), SqlDbType.Text);
                    PW_Hash = CreateSqlParameter("PW_Hash", passwordHashSha256, SqlDbType.Text);
                    loginCheck = Db.DataBase.SqlQuery<AuthResponse>("exec VeApp_User_Login_Check @AC_Num, @PW_Hash ", AC_Num, PW_Hash).ToList();
                }
                return loginCheck;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdatePasswordHash(string accountNumber, string passwordHash)
        {
            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
            SqlParameter pwHash = CreateSqlParameter("PW_Hash", passwordHash, SqlDbType.Text);
            try
            {
                string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_CRM_User_Record_PW @AC_Num, @PW_Hash ", acNum, pwHash).FirstOrDefault();
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AuthResponse> CheckUserAuth1(string username, string password)
        {
            List<AuthResponse> loginResponse = new List<AuthResponse>();
            AuthResponse loginRes = new AuthResponse();
            try
            {
                string passwordSaltHash = GetPasswordSaltHash(username), passwordHashSha1 = GeneratePasswordHashSha1(password, passwordSaltHash), passwordHashSha256 = GeneratePasswordHashSha256(password, passwordSaltHash);

                string constr = System.Configuration.ConfigurationManager.ConnectionStrings["BCP.Database.EntityContext"].ToString();
                using (SqlConnection ConLogin = new SqlConnection(constr))
                {
                    ConLogin.Open();
                    using (SqlCommand CmdLogin = new SqlCommand("[VeApp_User_Login_Check]", ConLogin))
                    {
                        CmdLogin.Parameters.AddWithValue("@ACNumberIn", username);
                        CmdLogin.Parameters.AddWithValue("@PWHashIn", passwordHashSha1);
                        CmdLogin.CommandType = CommandType.StoredProcedure;
                        using (var reader = CmdLogin.ExecuteReader())
                        {
                            if (reader.HasRows & reader.Read() & reader[0] != null & (Convert.ToString(reader[0]) != "" & Convert.ToString(reader[0]) != "0"))
                            {
                                loginRes = new AuthResponse();
                                loginRes.ClientIDNumber = reader[0] as int? ?? default(int);
                                loginRes.ClientAccountNumber = reader[1] as string;
                                loginRes.FirstName = reader[2] as string;
                                loginRes.LastName = reader[3] as string;
                                loginRes.City = reader[4] as string;
                                loginRes.Country = reader[5] as string;
                                loginResponse.Add(loginRes);

                                UpdatePasswordHash(username.ToUpper(), passwordHashSha256);
                            }
                            else
                            {
                                if (!reader.IsClosed) reader.Close();
                                CmdLogin.CommandText = "VeApp_User_Login_Check";
                                CmdLogin.Parameters.Clear();
                                CmdLogin.Parameters.AddWithValue("@ACNumberIn", username);
                                CmdLogin.Parameters.AddWithValue("@PWHashIn", passwordHashSha256);
                                CmdLogin.CommandType = CommandType.StoredProcedure;
                                using (var sreader = CmdLogin.ExecuteReader())
                                {
                                    if (sreader.HasRows & sreader.Read() & sreader[0] != null & (Convert.ToString(sreader[0]) != "" & Convert.ToString(sreader[0]) != "0"))
                                    {
                                        loginRes = new AuthResponse();
                                        loginRes.ClientIDNumber = sreader[0] as int? ?? default(int);
                                        loginRes.ClientAccountNumber = sreader[1] as string;
                                        loginRes.FirstName = sreader[2] as string;
                                        loginRes.LastName = sreader[3] as string;
                                        loginRes.City = sreader[4] as string;
                                        loginRes.Country = sreader[5] as string;
                                        loginResponse.Add(loginRes);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return loginResponse;
        }

        public string GetPasswordSaltHash(string accountNumber)
        {
            //  accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));

            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);

            try
            {
                string response = Db.DataBase.SqlQuery<string>("exec CRM_Get_User_Salt @AC_Num ", acNum).FirstOrDefault();
                if (string.IsNullOrWhiteSpace(response))
                {
                    response = GeneratePasswordSaltHash(accountNumber);
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GeneratePasswordSaltHash(string accountNumber)
        {
            //accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));

            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
            try
            {
                var str = Db.DataBase.SqlQuery<List<string>>("exec CRM_Create_User_Salt @AC_Num ", acNum).ToList();//.FirstOrDefault();
                string PW_Salt = "0ABBB583-3BA9-4522-AF4B-2419ABD6893D";// str.ToList().FirstOrDefault();// Db.DataBase.SqlQuery<string>("exec CRM_Create_User_Salt @AC_Num ", acNum).FirstOrDefault();
                UpdatePasswordSaltHash(accountNumber, PW_Salt);
                return PW_Salt;
            }
            catch (Exception ex) { throw ex; }
        }

        public string UpdatePasswordSaltHash(string accountNumber, string passwordSaltHash)
        {
            //UserBasicVO user = GetUserBasicByAccountNumber(accountNumber);
            //accountNumber.Assert("Account Number is required.", an => !string.IsNullOrWhiteSpace(an));
            //passwordSaltHash.Assert("Password Salt is required.", psh => !string.IsNullOrWhiteSpace(psh));
            //user.Assert("User not allowed.", u => u != null && u.Result > 0 && u.WebEnabled.HasValue && u.WebEnabled.Value);

            SqlParameter acNum = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
            SqlParameter pwSaltHash = CreateSqlParameter("PW_Salt", passwordSaltHash, SqlDbType.Text);
            try
            {
                string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_CRM_User_Record_Salt @AC_Num, @PW_Salt ", acNum, pwSaltHash).FirstOrDefault();
                response.Assert(response, r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r, StringComparison.OrdinalIgnoreCase));

                return response;
            }
            catch (Exception ex) { throw ex; }
        }
        
        public string GeneratePasswordHashSha1(string password, string passwordSalt)
        {
            return Encrypter.EncryptSha1(passwordSalt, password);
        }

        public string GeneratePasswordHashSha256(string password, string passwordSalt)
        {
            return Encrypter.EncryptSha256(passwordSalt, password);
        }

        public bool UpdateInvestorRecordStats(string username, short? numFailures)
        {
            bool isLocked = numFailures >= 5;
            SqlParameter AC_Num = CreateSqlParameter("AC_Num", username, SqlDbType.Text),
                NumFailures = CreateSqlParameter("NumFailures", numFailures, SqlDbType.SmallInt),
                AC_Blocked = CreateSqlParameter("AC_Blocked", isLocked, SqlDbType.Bit);
            string response = Db.DataBase.SqlQuery<string>("exec CRM_Update_CRM_User_Record_Stats @AC_Num, @NumFailures, @AC_Blocked", AC_Num, NumFailures, AC_Blocked).FirstOrDefault();

            try
            {
                response.Assert(response, r => !string.IsNullOrWhiteSpace(r) && "OK".Equals(r));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return isLocked;
        }

        public ClientDetailsBO GetClientDetailsByAccountNumber(string accountNumber)
        {
            SqlParameter AC_Num = CreateSqlParameter("AC_Num", accountNumber, SqlDbType.Text);
            ClientDetailsBO user = Db.DataBase.SqlQuery<ClientDetailsBO>("exec CRM_Get_Client_Details @AC_Num", AC_Num).FirstOrDefault();
            if (user != null) user.Username = accountNumber;
            return user;
        }

        public UserBasicBO GetUserBasicByAccountNumber(string accountNumber)
        {
            SqlParameter AcNum = CreateSqlParameter("AC_Num", accountNumber.ToUpper(), SqlDbType.Text);
            SqlParameter ACEmail = CreateSqlParameter("AC_Email", DBNull.Value, SqlDbType.Text);
            SqlParameter isUseEmail = CreateSqlParameter("Check_Type", 0, SqlDbType.SmallInt);
            //SqlParameter User_IP = CreateSqlParameter("User_IP", NetworkUtils.GetRequestIpAddress(), SqlDbType.Text);
            return Db.DataBase.SqlQuery<UserBasicBO>("exec CRM_Get_User_Basics @AC_Num, @AC_Email, @Check_Type", AcNum, ACEmail, isUseEmail).FirstOrDefault();
        }

    }
}