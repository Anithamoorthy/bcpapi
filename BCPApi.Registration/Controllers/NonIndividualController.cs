﻿using BCPApi.Entities.BO.Registration;
using BCPApi.Entities.Domain;
using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace BCPApi.Registration.Controllers
{
    [RoutePrefix("nonindividual")]
    public class NonIndividualController : ApiController
    {
        [Dependency]
        public IInvestmentRepository InvestorService { get; set; }
        [Dependency]
        public INonIndividualInvestorRepository NonIndividualInvestorService { get; set; }
        [Dependency]
        public IInvestorTypeRepository InvestorTypeService { get; set; }

        [Route("save-registration"), HttpPost]
        public IHttpActionResult SaveRegistration(RegistrationNonIndividualBO investor)
        {
            investor.RegistrationInProgress = true;
            investor.RegistrationComplete = false;
            investor = NonIndividualInvestorService.SaveRegistrationAndSendNotification(investor);
            //NonIndividualRegistration = investor;
            return Ok(investor);
        }

        [Route("continue-registration"), HttpPost]
        public IHttpActionResult ContinueRegistration(string notificationCode)
        {
            RegistrationNonIndividualBO investor = NonIndividualInvestorService.ContinueRegistration(notificationCode);
            investor.ContinueRegistrationToken = notificationCode;
            //NonIndividualRegistration = investor;
            return Ok(investor);
        }

        [Route("send-registration-2-factor-code"), HttpPost]
        public IHttpActionResult SendRegistration2FactorCode(int WebClientIDNumber, string phone)
        {
            NonIndividualInvestorService.SendRegistration2FactorCode(WebClientIDNumber, phone);
            return Ok(new { Status = true, Message = "The two factor authentication code has been sent successfully." });
        }

        [Route("complete-registration"), HttpPost]
        public IHttpActionResult CompleteRegistration(int WebClientIDNumber, string password, string twoFactorCode,string ContinueRegistrationToken)
        {
            return Ok(InvestorService.CompleteInvestorRegistration(WebClientIDNumber, password, twoFactorCode, NotificationTokenType.NonIndividualRegistrationEmailVerification.Id, ContinueRegistrationToken));
        }

        [Route("get-registration"), HttpPost]
        public IHttpActionResult GetRegistration(int WebClientIdNumber)
        {
            return Ok(NonIndividualInvestorService.GetRegistration(WebClientIdNumber));
        }

        [Route("send-verification-email"), HttpPost]
        public IHttpActionResult GetRegistration(RegistrationNonIndividualBO investor)
        {
            NonIndividualInvestorService.SendVerificationEmail(investor);
            return Ok(new { Status = true, Message = "The verification email has been sent successfully." });
        }

        [Route("get-investor-type"), HttpPost]
        public IHttpActionResult GetInvestorType()
        {
            //IList<InvestorTypeBO> types = InvestorTypeService.GetByClientType(InvestorRegistrationType.NonIndividual.ClientType);
            ////if (NonIndividualRegistration != null && NonIndividualRegistration.InvestorTypeGroup != null && NonIndividualRegistration.InvestorTypeGroup.Id > 0) types = types.Where(it => it.ClientSubTypeGroup != null && NonIndividualRegistration.InvestorTypeGroup.Id == it.ClientSubTypeGroup.Id).ToList();
            ////else 
            //types = types.Where(it => it.ClientSubTypeGroup != null && NonIndividualRegistrationTypeGroup.CorporateInvestor.Id == it.ClientSubTypeGroup.Id).ToList();
            return Ok(InvestorTypeService.GetByClientType(InvestorRegistrationType.NonIndividual.ClientType));
        }

        [Route("get-registration-declarations"), HttpPost]
        public IHttpActionResult GetRegistrationDeclarations(int WebClientIDNumber,string InvestorSubTypeEntered)
        {
            return Ok(InvestorService.GetRegistrationDeclarations(WebClientIDNumber, InvestorRegistrationType.NonIndividual.Id, InvestorSubTypeEntered));
        }

        [Route("add-registration-declarations"), HttpPost]
        public IHttpActionResult AddRegistrationDeclarations(List<RegistrationDeclarationBO> registrationDeclarations)
        {
            registrationDeclarations.ForEach(d => d.WebClientIDNumber = registrationDeclarations[0].WebClientIDNumber);
            return Ok(InvestorService.AddRegistrationDeclarations(registrationDeclarations));
        }

        [Route("get-registration-documents"), HttpPost]
        public IHttpActionResult GetRegistrationDocuments(int WebClientIDNumber, string InvestorSubTypeEntered)
        {
            return Ok(InvestorService.GetRegistrationDocuments(WebClientIDNumber, InvestorSubTypeEntered));
        }

        [Route("add-registration-documents"), HttpPost]
        public IHttpActionResult AddRegistrationDocuments(List<RegistrationDocumentBO> registrationDocuments)
        {
            registrationDocuments.ForEach(d => d.WebClientIDNumber = registrationDocuments[0].WebClientIDNumber);
            return Ok(InvestorService.AddRegistrationDocuments(registrationDocuments));
        }

    }
}
