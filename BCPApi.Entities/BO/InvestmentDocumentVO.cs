﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Script.Serialization;

namespace BCPApi.Entities.BO
{
    public class InvestmentDocumentBO
    {
        public int WIDocID { get; set; }
        public int WInvestmentID { get; set; }
        public string Document_Description { get; set; }
        public string Document_Link { get; set; }
        public bool Viewed { get; set; }
        public DateTime? Viewed_Date { get; set; }
        public bool Receipt_Confirmed { get; set; }
        public int OnS3 { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public string DocumentUrl
        {
            get
            {
                return (Document_Link ?? string.Empty).Split(new string[] { "#" }, StringSplitOptions.RemoveEmptyEntries).DefaultIfEmpty(string.Empty).LastOrDefault();
            }
        }

        [ScriptIgnore(ApplyToOverrides = true)]
        public string FileName
        {
            get
            {
                string link = DocumentUrl;
                int beginFileName = link.LastIndexOf("/");
                if (link.Length > beginFileName)
                    beginFileName++;
                return link.Substring(beginFileName);
            }
        }
    }
}
