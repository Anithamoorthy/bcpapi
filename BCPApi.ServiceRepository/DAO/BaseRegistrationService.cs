﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BCPApi.ServiceRepository.DAO
{
    public class BaseRegistrationService:CommonService
    {
        protected string IndividualRegistrationConfirmationLink(string code)
        {
            return RegistrationConfirmationLink("individual", code);
        }
        protected string JointRegistrationConfirmationLink(string code)
        {
            return RegistrationConfirmationLink("joint", code);
        }
        protected string NonIndividualRegistrationConfirmationLink(string code)
        {
            return RegistrationConfirmationLink("non-individual", code);
        }
        protected string OnlineRegistrationConfirmationLink(string code)
        {
            return RegistrationConfirmationLink("online", code);
        }

        private string RegistrationConfirmationLink(string registrationType, string code)
        {
            Uri url = HttpContext.Current.Request.Url;
            string host = url.AbsoluteUri.Substring(0, url.AbsoluteUri.IndexOf(url.AbsolutePath));
            if ("online".Equals(registrationType, StringComparison.OrdinalIgnoreCase))
                return string.Join("/", new[] { host, "activate", registrationType, "confirm-email", code });
            else
                return string.Join("/", new[] { host, "register", registrationType, "confirm-email", code });
        }

        protected string RegistrationConfirmationEmailContent(string name, string confirmationLink)
        {
            StringBuilder content = new StringBuilder();

            content.AppendLine(string.Format("Hello {0},<br><br>", name));
            content.AppendLine("Thank you for choosing to register for BCP Vespro. Please click on the link below to validate your e-mail address:<br>");
            content.AppendLine(string.Format("<a href='{0}'>{0}</a><br><br>", confirmationLink));
            content.AppendLine("If this link doesn't open for you, please copy and paste it into your browser or try an alternative internet browser (Google Chrome, Mozilla Firefox).<br>");
            content.AppendLine("Please note: This link will expire in 1 hour.< br><br>");
            content.AppendLine("Need to contact us? Send us an e-mail @ <a href='mailto:salessupport@bcp.ie'>salessupport@bcp.ie</a> or call us on <a href='tel:016684688'>01 6684688</a>.<br><br>");
            content.AppendLine("We look forward to hearing from you!<br>");
            //content.AppendLine("BCP Asset Management<br>");
            content.AppendLine("<strong>PS: Please don't respond to this automatically generated email.</strong>");

            return content.ToString();
        }
    }
}
