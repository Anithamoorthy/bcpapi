﻿using BCPApi.ServiceRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BCPApi.Investments.Controllers
{
    [RoutePrefix("investments")]
    public class AssetController : ApiController
    {
        public IAssetRepository assetService { get; set; }

        public AssetController(IAssetRepository asset)
        {
            assetService = asset;
        }

        [Route("asset"), HttpPost]
        public IHttpActionResult GetAssetAddFunds()
        {
            return Ok(assetService.GetAssetAddFunds());
        }
    }
}
